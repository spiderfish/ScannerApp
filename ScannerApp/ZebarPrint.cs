﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer;
using Zebra.Sdk.Printer.Discovery;

namespace ScannerApp.Common
{
    public class ZebarPrint
    {
        public string GetZebarPrintStatus(){
            string s = "";
            foreach (DiscoveredUsbPrinter usbPrinter in UsbDiscoverer.GetZebraUsbPrinters(new ZebraPrinterFilter()))
            {
                Connection c = usbPrinter.GetConnection();
                c.Open();
                if (c.Connected)
                {
                       s=  PrintStatus(c);
                   
                }
                c.Close();
            }
            return s;
        }
        
    private string PrintStatus(Connection connection)
        {
            string status = "";
            try
            {

                connection.Open();
                ZebraPrinter printer = ZebraPrinterFactory.GetInstance(connection);

                PrinterStatus printerStatus = printer.GetCurrentStatus();
                if (printerStatus.isReadyToPrint)
                {
                    status += "";//+ "Ready To Print"
                    Console.WriteLine("Ready To Print");
                }
                if (printerStatus.isPaused)
                {
                    status += "暂停";
                    Console.WriteLine("Cannot Print because the printer is paused.");
                }
                if (printerStatus.isHeadOpen)
                {
                    status += "盖子打开";
                    Console.WriteLine("Cannot Print because the printer head is open.");
                }
                if (printerStatus.isPaperOut)
                {
                    status += "缺纸";
                    Console.WriteLine("Cannot Print because the paper is out.");
                }

                {
                    //  label1.Text = status + printerStatus;
                    Console.WriteLine("Cannot Print.");
                }
            }
            catch (ConnectionException ex)
            {
                status = ex.Message;
                Console.WriteLine(ex.ToString());
            }
            //catch (ZebraPrinterLanguageUnknownException ex)
            //{
            //    status = ex.Message;
            //    Console.WriteLine(ex.ToString());
            //}
            finally
            {
                connection.Close();
            }
            return status;
        }

    }
}
