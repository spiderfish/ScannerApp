﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoUpdate.Log
{
    public enum LogType
    {
        All,
        Information,
        Debug,
        Success,
        Failure,
        Warning,
        Error

    }
}
