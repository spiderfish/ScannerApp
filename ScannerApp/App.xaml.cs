﻿using ScannerApp.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace ScannerApp
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
      
        [STAThread]
        static void Main()
        {
            // 方式1
            Application app = new Application();
            AutoUpdate.Updater.CheckUpdateStatus();

           
            IniFiles ini = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\ScanAppConfig.INI");
            string IsInitSet = "N";
            // 方式3
            if (ini.ExistINIFile())
            {
                try
                {
                      IsInitSet = ini.IniReadValue("是否初始化", "IsInitSet");

                }
                catch { }
             }

            if (IsInitSet == "N")
            {
                app.StartupUri = new Uri("MainWindow.xaml", UriKind.Relative);
            }
            else { app.StartupUri = new Uri("navigtion.xaml", UriKind.Relative); }
            app.Run();
        }


    }
}
