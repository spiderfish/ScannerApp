﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Authorize
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void AduFlatButton_Click(object sender, RoutedEventArgs e)
        {
            if(AuDate.SelectedDate==null)
            {
                MessageBox.Show("请选择授权时间!"); return;
            }
            if (AuDate.SelectedDate < DateTime.Now) {
                MessageBox.Show("授权时间不能小于今天!");return;
            }
            object o = new
            {
                Company = AuCompany.Text.Trim(),
                AuDate = AuDate.SelectedDate,
                IsFirst = "Y",//首次授权
                LicDate = 0,//有效期
                ErrorTime = 0//错误次数
            }; 
            string str = cryptHelper.Encrypt(JsonHelper.ToJson(o));
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "保存授权文件";
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.AddExtension = true;
            saveFileDialog.DefaultExt = "lic";
            saveFileDialog.Filter = "授权文件|*.lic";
            saveFileDialog.FileName = "WinProLic.lic";
            if (!(bool)saveFileDialog.ShowDialog())
            {
                return; //被点了取消
            }
            else {
                System.IO.File.WriteAllText(saveFileDialog.FileName, str, Encoding.UTF8);
            }
          //  var filename = saveFileDialog.FileName; //得到保存路径及文件名

        }
    }
}
