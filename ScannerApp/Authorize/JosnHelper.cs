﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
namespace Authorize
{
    public class JsonHelper
    {
        #region Json
     

        /// <summary>
        /// JsonConvert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJson<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        /// <summary>
        /// JsonConvert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        public static T ToObject<T>(string content)
        {
            return JsonConvert.DeserializeObject<T>(content);
        }

        public static string ReadJson(string path)
        { 
            StreamReader sr1 = new StreamReader(path, Encoding.UTF8);
            string jsons1 = sr1.ReadToEnd(); 
            return jsons1;
        }

        #endregion Json




    }



    /// <summary>
    /// 实体类、字符串互相转换
    /// </summary>
    public class PackReflectionEntity<T>
    {
        /// <summary>
        /// 将实体类通过反射组装成字符串
        /// </summary>
        /// <param name="t">实体类</param>
        /// <returns>组装的字符串</returns>
        public static string GetEntityToString(T t)
        {
            System.Text.StringBuilder sb = new StringBuilder();
            Type type = t.GetType();
            System.Reflection.PropertyInfo[] propertyInfos = type.GetProperties();
            for (int i = 0; i < propertyInfos.Length; i++)
            {
                sb.Append(propertyInfos[i].Name + ":" + propertyInfos[i].GetValue(t, null) + ",");
            }
            return sb.ToString().TrimEnd(new char[] { ',' });
        }
        public static string GetEntityToStringValue(T t)
        {
            System.Text.StringBuilder sb = new StringBuilder();
            Type type = t.GetType();
            System.Reflection.PropertyInfo[] propertyInfos = type.GetProperties();
            for (int i = 0; i < propertyInfos.Length; i++)
            {
                sb.Append(propertyInfos[i].GetValue(t, null) + ",");
            }
            return sb.ToString().TrimEnd(new char[] { ',' });
        }
        public static string GetEntityToStringValueBeginToEnd(T t,int B,int E)
        {
            System.Text.StringBuilder sb = new StringBuilder();
            Type type = t.GetType();
            System.Reflection.PropertyInfo[] propertyInfos = type.GetProperties();
            if (E > propertyInfos.Length) E = propertyInfos.Length;
            for (int i = B; i < E; i++)
            {
                sb.Append("\""+propertyInfos[i].GetValue(t, null) + "\",");
            }
            return sb.ToString().TrimEnd(new char[] { ',' });
        }
        /// <summary>
        /// /
        /// </summary>
        /// <param name="t">数据En'ti't'y</param>
        /// <param name="B">开始行</param>
        /// <param name="E">结束行</param>
        /// <returns></returns>
        public static List<object> GetEntityToStringValueBE(T t, int B, int E)
        {
            System.Text.StringBuilder sb = new StringBuilder();
            Type type = t.GetType();
            List<object> objs = new List<object>();
            System.Reflection.PropertyInfo[] propertyInfos = type.GetProperties();
            if (E > propertyInfos.Length) E = propertyInfos.Length;
            for (int i = B; i < E; i++)
            {
               object obj = propertyInfos[i].GetValue(t, null);
               objs.Add(obj);
            }
             
            return objs;
        }
        public static List<List<object>> GetEntityToStringValueBE_Cell(T t, int B, int E)
        {
            List<List<object>> ll = new List<List<object>>();
            List<object> l = GetEntityToStringValueBE(t,B,E);
            ll.Add(l);
            return ll;
        }


            /// <summary>
            /// 将反射得到字符串转换为对象
            /// </summary>
            /// <param name="str">反射得到的字符串</param>
            /// <returns>实体类</returns>
            public static T GetEntityStringToEntity(string str)
        {
            string[] array = str.Split(',');
            string[] temp = null;
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (string s in array)
            {
                temp = s.Split(':');
                dictionary.Add(temp[0], temp[1]);
            }
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetAssembly(typeof(T));
            T entry = (T)assembly.CreateInstance(typeof(T).FullName);
            System.Text.StringBuilder sb = new StringBuilder();
            Type type = entry.GetType();
            System.Reflection.PropertyInfo[] propertyInfos = type.GetProperties();
            for (int i = 0; i < propertyInfos.Length; i++)
            {
                foreach (string key in dictionary.Keys)
                {
                    if (propertyInfos[i].Name == key.ToString())
                    {
                        propertyInfos[i].SetValue(entry, GetObject(propertyInfos[i], dictionary[key]), null);
                        break;
                    }
                }
            }
            return entry;
        }
        /// <summary>
        /// 转换值的类型
        /// </summary>
        /// <param name="p"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        static object GetObject(System.Reflection.PropertyInfo p, string value)
        {
            switch (p.PropertyType.Name.ToString().ToLower())
            {
                case "int16":
                    return Convert.ToInt16(value);
                case "int32":
                    return Convert.ToInt32(value);
                case "int64":
                    return Convert.ToInt64(value);
                case "string":
                    return Convert.ToString(value);
                case "datetime":
                    return Convert.ToDateTime(value);
                case "boolean":
                    return Convert.ToBoolean(value);
                case "char":
                    return Convert.ToChar(value);
                case "double":
                    return Convert.ToDouble(value);
                default:
                    return value;
            }
        }
    }
}
