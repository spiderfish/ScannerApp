﻿using ScannerApp.Common;
using ScannerApp.Common.Print;
using ScannerApp.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static ScannerApp.Common.PopMessageHelper;
using static ScannerApp.Model.OperationType;
using static ScannerApp.Model.WorkType;

namespace ScannerApp
{
    /// <summary>
    /// navigtion.xaml 的交互逻辑
    /// </summary>
    public partial class navigtion : Window
    {
        ScanApp.ScanAppSoapClient sa = new ScanApp.ScanAppSoapClient();
        int i = 0, j = 0;
        IniFiles ini = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\ScanAppConfig.INI");
        
        string SelectLX = "";
        public navigtion()
        {
            InitializeComponent();
        }

        private void btnlan_Click(object sender, RoutedEventArgs e)
        {
            var window = new Window();//Windows窗体 
            Business.SelectLanguage s = new Business.SelectLanguage();
            window.Content = s;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.ShowDialog();//模式，弹出！
        }
        private void btnsingle_Click(object sender, RoutedEventArgs e)
        {
            ini.IniWriteValue("公司信息", "SingleOrNet", "Single"); 
            System.Windows.Forms.Application.Restart();
            Application.Current.Shutdown();
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            ++i;
            j = i;
            if (i == myMenu.Items.Count - 1)
            {
                btnNext.IsEnabled = false;
                btnFinsh.IsEnabled = true;
            }
            if (i >= 1) btnPer.IsEnabled = true;
            LoadData(i);
            MenuItem item = myMenu.ItemContainerGenerator.ContainerFromIndex(i) as MenuItem;
            gb.Header = item.Header;
            if (item != null)
            {
                item.Background = Brushes.DarkGoldenrod;
                if (j >= 1)
                {
                    j = j - 1;
                    MenuItem prevItem = myMenu.ItemContainerGenerator.ContainerFromIndex(j) as MenuItem;
                    if (prevItem != null)
                    {
                        prevItem.Background = null;
                    }
                }
            }

            DisplayThree();


        }
        private void btnPer_Click(object sender, RoutedEventArgs e)
        {
            --i;
            j = i;
            if (i <= 0)
            {
                btnPer.IsEnabled = false;
                btnNext.IsEnabled = true;
            }
            btnFinsh.IsEnabled = false;
            LoadData(i);
            MenuItem item = myMenu.ItemContainerGenerator.ContainerFromIndex(i) as MenuItem;

            gb.Header = item.Header;
            if (item != null)
            {
                item.Background = Brushes.DarkGoldenrod;
                if (j >= 0)
                {
                    j = j + 1;
                    MenuItem prevItem = myMenu.ItemContainerGenerator.ContainerFromIndex(j) as MenuItem;
                    if (prevItem != null)
                    {
                        prevItem.Background = null;
                    }
                }
            }
            DisplayThree();
        }

        private void DisplayThree()
        {
            string SelectLX = ini.IniReadValue("登录类型", "LoginStyle");
            if (SelectLX == "xs" )
            {
                Socket.Visibility = Visibility.Visible;
                comsz.Visibility = Visibility.Visible;
            }
            else if (SelectLX == "cz")//称重用的是com口
            {
                Socket.Visibility = Visibility.Collapsed;
                comsz.Visibility = Visibility.Visible;
            }
            else if ( SelectLX == "db")//打包
            {
                Socket.Visibility = Visibility.Visible;
                comsz.Visibility = Visibility.Visible;
            }
            else {
                //Socket.Visibility = Visibility.Collapsed;
                comsz.Visibility = Visibility.Collapsed;
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Maximized;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            if (myMenu.Items.Count >= 1)//初始化，默认有数据，则显示第一个数据
            {
                MenuItem item = myMenu.ItemContainerGenerator.ContainerFromIndex(0) as MenuItem;
                loadszjk();//默认设置接口
                item.Background = Brushes.DarkGoldenrod;
                gb.Header = item.Header;
                btnPer.IsEnabled = false;
                btnFinsh.IsEnabled = false;
            }
        }

        //设置完成
        private void btnFinsh_Click(object sender, RoutedEventArgs e)
        {
            ini.IniWriteValue("是否初始化", "IsInitSet", "Y");
            this.Hide();
            MainWindow mw = null;
            foreach (Window w in App.Current.Windows)
            {
                if (w is MainWindow)
                {
                    mw = w as MainWindow;
                }
            }
            if (mw == null)
            {
                mw = new MainWindow();
                mw.Show();
            }
            else
            {
                // Restart current process Method 1
                System.Windows.Forms.Application.Restart();
                Application.Current.Shutdown();
                // mw.Visibility = System.Windows.Visibility.Visible;
            }
            // mw.Show();
        }

        //打印设置保存
        private void btnSetPrinter_Click(object sender, RoutedEventArgs e)
        {
            if (Zebra.IsChecked == true)
            {
                ini.IniWriteValue("打印类型", "PrintStyle", "Zebra");
            }
            else if (Gainscha.IsChecked == true)
            {
                ini.IniWriteValue("打印类型", "PrintStyle", "Gainscha");
            }
            else if (ZebraZD420.IsChecked == true)
            {
                ini.IniWriteValue("打印类型", "PrintStyle", "ZebraZD420");
            }
            UIAction(() =>
            {
                tips.SetResourceReference(TextBlock.TextProperty, "InfoSave");
                tips2.SetResourceReference(TextBlock.TextProperty, "InfoTitle");
                PopMessageHelper.ShowMessage(tips.Text, tips2.Text, 3000, MsgStyle.RedCritical_OK);

            });
        }
        //测试
        private void btnPrinttest_Click(object sender, RoutedEventArgs e)
        {

            if (Zebra.IsChecked == true||ZebraZD420.IsChecked==true)
            {
                if (ZebarPrint.GetZebarPrintCount() > 1)
                    lbprinttest.Content = "打印机超过2个，请删除一个无效的！";
                else         
                    lbprinttest.Content = ZebarPrint.GetZebarPrintStatus();
               
            }
            else if (Gainscha.IsChecked == true)
                lbprinttest.Content = GetPrintStatus();
        }
        string GetPrintStatus()
        { //打印机参数
            string a = "00";
            if (ini.ExistINIFile())
            {
              string  Com = ini.IniReadValue("串口设置", "COM"); //Common.ConfigHelper.GetIni("COM", "");
                string BaudRate = ini.IniReadValue("串口设置", "BaudRate"); //Common.ConfigHelper.GetIni("BaudRate", "");
                string DataBits = ini.IniReadValue("串口设置", "DataBits");//Common.ConfigHelper.GetIni("DataBits", "");
                string StopRate = ini.IniReadValue("串口设置", "StopRate");//Common.ConfigHelper.GetIni("StopRate", "");
                string ParityRate = ini.IniReadValue("串口设置", "ParityRate"); //Common.ConfigHelper.GetIni("ParityRate", "");

             
                try
                {

                    int BR = stringtoint(BaudRate);//获取或设置串行 波特率bit/s  默认值9600
                    int DB = stringtoint(DataBits); ;//获取或设置每个字节的标准数据位长度 默认值8
                    int stoprate = stringtoint(StopRate);
                    int ParityBits = 0;//获取或设置奇偶校验检查协议    默认值None
                    ParityBits = ParityRate == "None" ? 0 : 1;//目前就2个

                    ComStatusSet.SetSerial(Com, BR, stoprate, ParityBits, DB);
                    a = ComStatusSet.GetStaus();
                }
                catch (Exception ex)
                {
                    a = ex.Message;
                }
            }
            return a;
        }
        void LoadData(int i)
        {

            switch (i)
            {
                case 0:
                    loadszjk();
                    szjk.Visibility = Visibility.Visible;
                    szzd.Visibility = Visibility.Collapsed;

                    break;
                case 1:
                    loadszzd();//默认设置站点
                    szjk.Visibility = Visibility.Collapsed;
                    szzd.Visibility = Visibility.Visible;
                    szck.Visibility = Visibility.Collapsed;
                    break;
                case 2:
                    loadszck();//默认设置串口
                    szzd.Visibility = Visibility.Collapsed;
                    szck.Visibility = Visibility.Visible;
                    bz3.Visibility = Visibility.Collapsed;
                    break;
                case 3:
                    loadPWD();
                    szck.Visibility = Visibility.Collapsed;
                    bz3.Visibility = Visibility.Visible;
                    bz4.Visibility = Visibility.Collapsed;
                    break;

                default:

                    break;
            }

        }

        void loadPWD()
        {
            szpwd.Text= ini.IniReadValue("密码设置", "setting");
            plcpwd.Text=  ini.IniReadValue("密码设置", "PLC");
        }

        #region 设置接口
       void loadszjk()
        {
            string checklogin = ini.IniReadValue("登录类型", "LoginStyle");
            if (checklogin != "")
            {
                if (checklogin == OperationTypes.test.ToString() ) test.IsChecked = true;
                if (checklogin == OperationTypes.mdfx.ToString()  ) mdfx.IsChecked = true;
                if (checklogin == OperationTypes.hlfx.ToString() ) hlfx.IsChecked = true;
                if (checklogin == OperationTypes.jycj.ToString()) { jycj.IsChecked = true; workmd.Visibility = Visibility.Visible; }
                if (checklogin == OperationTypes.xs.ToString()) xs.IsChecked = true;
                if (checklogin == OperationTypes.db.ToString()) { db.IsChecked = true; pk_print_PLC.Visibility = Visibility.Visible; printToPLCtime.Text= ini.IniReadValue("其他参数", "PrintToPLC"); }
                if (checklogin == OperationTypes.cz.ToString()) cz.IsChecked = true;
            }
            string WorkStyle = ini.IniReadValue("作业类型", "WorkStyle");
            //if (WorkStyle == WorkTypes.WorkAsOrder.ToString()) adzy.IsChecked = true;
            //if (WorkStyle == WorkTypes.DirectWork.ToString()) zjzy.IsChecked = true;
            if (WorkStyle == WorkTypes.MixWork.ToString()) hhzy.IsChecked = true;

            string printsytle = ini.IniReadValue("打印类型", "PrintStyle");
            if (printsytle != "") {
                if (printsytle == PrintTypes.Zebra.ToString()) Zebra.IsChecked = true;
                 else   if (printsytle == PrintTypes.ZebraZD420.ToString()) ZebraZD420.IsChecked = true;
                else if (printsytle == PrintTypes.Gainscha.ToString()) Gainscha.IsChecked = true;
            }


            string a = Common.ConfigHelper.GetEndpointClientAddress("ScanAppSoap");
            txtsjjk.Text = a;

        }

        private void btn_sjjk_Click(object sender, RoutedEventArgs e)
        {
            if (Common.ConfigHelper.SetEndpointClientAddress("ScanAppSoap", txtsjjk.Text))
            {
                lbsjjk.SetResourceReference(ContentProperty, "InfoSave");
               // lbsjjk.Content = "修改成功！";
                lbsjjk.Foreground = Brushes.Green;
            }
            else {
                lbsjjk.SetResourceReference(ContentProperty, "ErrorSave");
               // lbsjjk.Content = "修改失败！";
                lbsjjk.Foreground = Brushes.Red;
            }
         
        }
        private void view(object sender, RoutedEventArgs e)
        {
            if (jycj.IsChecked == true)
            { 
                workmd.Visibility = Visibility.Visible;

            }else workmd.Visibility = Visibility.Collapsed;

            if (db.IsChecked == true)
            {
                pk_print_PLC.Visibility = Visibility.Visible;
                pk_print_Print.Visibility = Visibility.Visible;

            }
            else
            {
                pk_print_PLC.Visibility = Visibility.Collapsed;
                pk_print_Print.Visibility = Visibility.Collapsed;
            }
            
        }
        private void btnlx_Click(object sender, RoutedEventArgs e)
        {
          
            if (cz.IsChecked == true)
            {
                ini.IniWriteValue("登录类型", "LoginStyle", "cz" );
            
            }
            if (test.IsChecked == true)
            {
                ini.IniWriteValue("登录类型", "LoginStyle", "test");
              
            }
            if (mdfx.IsChecked == true)
            {
                ini.IniWriteValue("登录类型", "LoginStyle", "mdfx");
              
            }
            if (hlfx.IsChecked == true)
            {
                ini.IniWriteValue("登录类型", "LoginStyle", "hlfx");
                
            }
            if (jycj.IsChecked == true)
            {
                ini.IniWriteValue("登录类型", "LoginStyle", "jycj");
                workmd.Visibility = Visibility.Visible;
               
            }
            if (xs.IsChecked == true)
            {
                ini.IniWriteValue("登录类型", "LoginStyle", "xs");
              
            }
            if (db.IsChecked == true)
            {
                ini.IniWriteValue("登录类型", "LoginStyle", "db");
               
            }
            UIAction(() =>
            {
                tips.SetResourceReference(TextBlock.TextProperty, "InfoSave");
                tips2.SetResourceReference(TextBlock.TextProperty, "InfoTitle");
                PopMessageHelper.ShowMessage(tips.Text, tips2.Text, 3000, MsgStyle.RedCritical_OK);

            });
        }

        private void btnwork_Click(object sender, RoutedEventArgs e)
        {

            if (adzy.IsChecked == true)
            {
                ini.IniWriteValue("作业类型", "WorkStyle", "WorkAsOrder");

            }
            //if (zjzy.IsChecked == true)
            //{
            //    ini.IniWriteValue("作业类型", "WorkStyle", "DirectWork");

            //}
            if (hhzy.IsChecked == true)
            {
                ini.IniWriteValue("作业类型", "WorkStyle", "MixWork");

            }
            UIAction(() =>
            {
                tips.SetResourceReference(TextBlock.TextProperty, "InfoSave");
                tips2.SetResourceReference(TextBlock.TextProperty, "InfoTitle");
                PopMessageHelper.ShowMessage(tips.Text, tips2.Text, 3000, MsgStyle.RedCritical_OK);

            });
        }
        private void btnPrintToPLC_Click(object sender, RoutedEventArgs e)
        {

           
            if (printToPLCtime.Text.Trim() != "")
            {
                ini.IniWriteValue("其他参数", "PrintToPLC", printToPLCtime.Text );

            }
            UIAction(() =>
            {
                tips.SetResourceReference(TextBlock.TextProperty, "InfoSave");
                tips2.SetResourceReference(TextBlock.TextProperty, "InfoTitle");
                PopMessageHelper.ShowMessage(tips.Text, tips2.Text, 3000, MsgStyle.RedCritical_OK);

            });
        }
       

        #endregion

        #region 设置站点
        string DeviceNo = "";
        private void btnok_Click(object sender, RoutedEventArgs e)
        {
            string mac = Common.MacAddress.GetMacAddress();
            string DevName = DeviceNo;
            if (mac == "") PopMessageHelper.ShowMessage("MAC地址获取失败！检测网卡是否正常？\r\n窗口3秒后关闭...", "WinPro系统设置提示", 3000, MsgStyle.RedCritical_OK);
            if (DevName == "") PopMessageHelper.ShowMessage("请选择一个有效机器名称！\r\n窗口3秒后关闭...", "WinPro系统设置提示", 3000, MsgStyle.RedCritical_OK);

            string para = "{\"DeviceName\":\"" + DevName + "\",\"MacAddress\":\"" + mac + "\"}";
            string msg = sa.PC_DeviceName("ADD", para);
            if (msg == "")
            {
                PopMessageHelper.ShowMessage("绑定成功!!\r\n窗口1秒后关闭...", "WinPro系统设置提示", 1000, MsgStyle.BlueInfo_OK);
                this.Close();
            }
            else
                PopMessageHelper.ShowMessage("绑定失败" + msg + "\r\n窗口3秒后关闭...", "WinPro系统设置提示", 3000, MsgStyle.RedCritical_OK);
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView dr = dataGrid.SelectedItem as DataRowView; if (dr != null)
            {
                tb_No.Text = dr["product_name"].ToString();
                DeviceNo = dr["id"].ToString();
            }
        }

        private void loadszzd()
        {
            string mac = Common.MacAddress.GetMacAddress();
            if (mac == "") {
                UIAction(() =>
                {
                    tips.SetResourceReference(TextBlock.TextProperty, "ErrorMac");
                    tips2.SetResourceReference(TextBlock.TextProperty, "InfoTitle");
                    PopMessageHelper.ShowMessage(tips.Text, tips2.Text, 3000, MsgStyle.RedCritical_OK);

                });
            } //PopMessageHelper.ShowMessage("MAC地址获取失败！检测网卡是否正常？\r\n窗口3秒后关闭...", "WinPro系统设置提示", 3000, MsgStyle.RedCritical_OK);

            string para = "{\"DeviceName\":\"\",\"MacAddress\":\"" + mac + "\"}";

            string msg = sa.PC_DeviceName("IsExist", para);
            if (msg == "") { }
            else tb_No.Text = msg;

            DataTable lsdt = sa.GetList_Basic_Device();
            if (lsdt.Rows.Count <= 0) {
                UIAction(() =>
                {
                    tips.SetResourceReference(TextBlock.TextProperty, "Errorlist");
                    tips2.SetResourceReference(TextBlock.TextProperty, "InfoTitle");
                    PopMessageHelper.ShowMessage(tips.Text, tips2.Text, 3000, MsgStyle.RedCritical_OK);

                });
            }

               // PopMessageHelper.ShowMessage("请联系相关人员，维护机器列表！\r\n窗口3秒后关闭...", "WinPro系统设置提示", 3000, MsgStyle.YellowAlert_OK);

            dataGrid.ItemsSource = lsdt.DefaultView;
        }

        #endregion

        #region 设置串口
     

        private List<int> BaudRateList = new List<int>() { 100, 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 56000, 57600, 115200, 128000, 256000, 512000 };
        private List<int> DataBitsList = new List<int>() { 6, 7, 8 };
        private List<double> StopBitsList = new List<double>() { 1, 1.5, 2 };
        private List<string> ParityList = new List<string>() { "None", "EVEN", "Odd" };

        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            string SelectLX = ini.IniReadValue("登录类型", "LoginStyle");
              if (SelectLX == "cz" )//称重用的是com口
            {
                if (cbPortName.SelectedValue.ToString() == "" ||
                   btl.SelectedValue.ToString() == "" ||
                   sjw.SelectedValue.ToString() == "" ||
                   tzw.SelectedValue.ToString() == "" ||
                   parity.SelectedValue.ToString() == "")
                {
                    UIAction(() =>
                    {
                        tips.SetResourceReference(TextBlock.TextProperty, "ErrorPara");
                        tips2.SetResourceReference(TextBlock.TextProperty, "InfoTitle");
                        PopMessageHelper.ShowMessage(tips.Text, tips2.Text, 3000, MsgStyle.RedCritical_OK);

                    });
                   // PopMessageHelper.ShowMessage("参数选择不能为空！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                }
                ini.IniWriteValue("串口设置", "COM", cbPortName.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "BaudRate", btl.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "DataBits", sjw.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "StopRate", tzw.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "ParityRate", parity.SelectedValue.ToString());
            }
            else if (SelectLX == "db"|| SelectLX == "xs")//打包
            {
                if (cbPortName.SelectedValue.ToString() == "" ||
                                  btl.SelectedValue.ToString() == "" ||
                                  sjw.SelectedValue.ToString() == "" ||
                                  tzw.SelectedValue.ToString() == "" ||
                                  parity.SelectedValue.ToString() == "")
                {
                    UIAction(() =>
                    {
                        tips.SetResourceReference(TextBlock.TextProperty, "ErrorPara");
                        tips2.SetResourceReference(TextBlock.TextProperty, "InfoTitle");
                        PopMessageHelper.ShowMessage(tips.Text, tips2.Text, 3000, MsgStyle.RedCritical_OK);

                    });
                }
                ini.IniWriteValue("串口设置", "COM", cbPortName.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "BaudRate", btl.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "DataBits", sjw.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "StopRate", tzw.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "ParityRate", parity.SelectedValue.ToString());
                ini.IniWriteValue("TCPIP", "PLCIP", ip.Text);
                ini.IniWriteValue("TCPIP", "PLCPort", port.Text);
                ini.IniWriteValue("TCPIP", "PrintIP", printip.Text);
            }
            else { }//其他 采集 活力分析（tcp,但是特殊）都不需要设置
            UIAction(() =>
            {
                tips.SetResourceReference(TextBlock.TextProperty, "InfoSave");
                tips2.SetResourceReference(TextBlock.TextProperty, "InfoTitle");
                PopMessageHelper.ShowMessage(tips.Text, tips2.Text, 3000, MsgStyle.OK);

            });
          //  PopMessageHelper.ShowMessage("保存成功！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.OK);

        }
        //密码保存
        private void btnsave1_Click(object sender, RoutedEventArgs e)
        {
            ini.IniWriteValue("密码设置", "setting", szpwd.Text.Trim());
            ini.IniWriteValue("密码设置", "PLC", plcpwd.Text.Trim());
            UIAction(() =>
            {
                tips.SetResourceReference(TextBlock.TextProperty, "InfoSave");
                tips2.SetResourceReference(TextBlock.TextProperty, "InfoTitle");
                PopMessageHelper.ShowMessage(tips.Text,tips2.Text, 3000, MsgStyle.OK);

            });
        }

        private void loadszck()
        {
            //获取串口
            string[] s = SerialPort.GetPortNames();
            //  cbPortName.Items.AddRange(SerialPort.GetPortNames());
            cbPortName.ItemsSource = s;
            if (cbPortName.Items.Count > 0)
            {
                cbPortName.SelectedIndex = 0;
            }
            btl.ItemsSource = BaudRateList;
            sjw.ItemsSource = DataBitsList;
            tzw.ItemsSource = StopBitsList;
            parity.ItemsSource = ParityList;

            btl.SelectedIndex = 6;
            sjw.SelectedIndex = 2;
            tzw.SelectedIndex = 0;
            parity.SelectedIndex = 0;

            if (ini.ExistINIFile())
            {
                try
                {
                    string Com = ini.IniReadValue("串口设置", "COM"); //Common.ConfigHelper.GetIni("COM", "");
                    string BaudRate = ini.IniReadValue("串口设置", "BaudRate"); //Common.ConfigHelper.GetIni("BaudRate", "");
                    string DataBits = ini.IniReadValue("串口设置", "DataBits");//Common.ConfigHelper.GetIni("DataBits", "");
                    string StopRate = ini.IniReadValue("串口设置", "StopRate");//Common.ConfigHelper.GetIni("StopRate", "");
                    string ParityRate = ini.IniReadValue("串口设置", "ParityRate"); //Common.ConfigHelper.GetIni("ParityRate", "");


                    ip.Text = ini.IniReadValue("TCPIP", "PLCIP");
                    port.Text = ini.IniReadValue("TCPIP", "PLCPort");
                    printip.Text = ini.IniReadValue("TCPIP", "PrintIP");

                    cbPortName.SelectedValue = Com;
                    btl.SelectedValue = int.Parse(BaudRate);
                    sjw.SelectedValue = int.Parse(DataBits);
                    tzw.SelectedValue = double.Parse(StopRate);
                    parity.SelectedValue = ParityRate;

                  
                }
                catch { }
            }
        }
        void UIAction(Action action)//在主线程外激活线程方法
        {
            System.Threading.SynchronizationContext.SetSynchronizationContext(new System.Windows.Threading.DispatcherSynchronizationContext(App.Current.Dispatcher));
            System.Threading.SynchronizationContext.Current.Post(_ => action(), null);
        }
        int stringtoint(string a)
        {
            int i = 0;
            try { i = int.Parse(a); } catch { }
            return i;
        }
        #endregion




    }
}
