﻿using ScannerApp.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static ScannerApp.Common.PopMessageHelper;

namespace ScannerApp
{
    /// <summary>
    /// SingleSetting.xaml 的交互逻辑
    /// </summary>
    public partial class SingleSetting : Window
    {
        IniFiles ini = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\ScanAppConfig.INI");
      
        public SingleSetting()
        {
            InitializeComponent();
        }

        private void window_loaded(object sender, RoutedEventArgs e) 
        {
            ip.Text=    ini.IniReadValue("TCPIP", "PLCIP");
              port.Text=  ini.IniReadValue("TCPIP", "PLCPort" );
              printip.Text=  ini.IniReadValue("TCPIP", "PrintIP" );

               szpwd.Text= ini.IniReadValue("密码设置", "setting" );
               plcpwd.Text=  ini.IniReadValue("密码设置", "PLC" );
            string s= ini.IniReadValue("SingleAppSetting", "LoginStyle" );
            if (s == "xs") xs.IsChecked = true;
            if (s == "db") db.IsChecked = true;
            loadCOM();
        }

        private List<int> BaudRateList = new List<int>() { 100, 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 56000, 57600, 115200, 128000, 256000, 512000 };
        private List<int> DataBitsList = new List<int>() { 6, 7, 8 };
        private List<double> StopBitsList = new List<double>() { 1, 1.5, 2 };
        private List<string> ParityList = new List<string>() { "None", "EVEN", "Odd" };

        void loadCOM()
        {
            //获取串口
            string[] s = SerialPort.GetPortNames();
            //  cbPortName.Items.AddRange(SerialPort.GetPortNames());
            cbPortName.ItemsSource = s;
            if (cbPortName.Items.Count > 0)
            {
                cbPortName.SelectedIndex = 0;
            }
            btl.ItemsSource = BaudRateList;
            sjw.ItemsSource = DataBitsList;
            tzw.ItemsSource = StopBitsList;
            parity.ItemsSource = ParityList;

            btl.SelectedIndex = 6;
            sjw.SelectedIndex = 2;
            tzw.SelectedIndex = 0;
            parity.SelectedIndex = 0;
            try
            {
                string Com = ini.IniReadValue("串口设置", "COM"); //Common.ConfigHelper.GetIni("COM", "");
                string BaudRate = ini.IniReadValue("串口设置", "BaudRate"); //Common.ConfigHelper.GetIni("BaudRate", "");
                string DataBits = ini.IniReadValue("串口设置", "DataBits");//Common.ConfigHelper.GetIni("DataBits", "");
                string StopRate = ini.IniReadValue("串口设置", "StopRate");//Common.ConfigHelper.GetIni("StopRate", "");
                string ParityRate = ini.IniReadValue("串口设置", "ParityRate"); //Common.ConfigHelper.GetIni("ParityRate", "");

                cbPortName.SelectedValue = Com;
                btl.SelectedValue = int.Parse(BaudRate);
                sjw.SelectedValue = int.Parse(DataBits);
                tzw.SelectedValue = double.Parse(StopRate);
                parity.SelectedValue = ParityRate;
            }
            catch  { }

        }
        //IP,PLC 打印机
        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            //if (SelectLX == "xs")//稀释/打包，用的是TCP
            //{
         
                ini.IniWriteValue("TCPIP", "PLCIP", ip.Text);
                ini.IniWriteValue("TCPIP", "PLCPort", port.Text);
                ini.IniWriteValue("TCPIP", "PrintIP", printip.Text);
            try
            {
                ini.IniWriteValue("串口设置", "COM", cbPortName.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "BaudRate", btl.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "DataBits", sjw.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "StopRate", tzw.SelectedValue.ToString());
                ini.IniWriteValue("串口设置", "ParityRate", parity.SelectedValue.ToString());

            }
            catch { }
                ini.IniWriteValue("密码设置", "setting", szpwd.Text.Trim());
                ini.IniWriteValue("密码设置", "PLC", plcpwd.Text.Trim());

            if (xs.IsChecked == true)
            {
                //MessageBox.Show("No, please wait...");

                ini.IniWriteValue("SingleAppSetting", "LoginStyle", "xs");
               // ini.IniWriteValue("SingleAppSetting", "LoginStyle", "xs");
                 
            }
            if (db.IsChecked == true)
            {
                ini.IniWriteValue("SingleAppSetting", "LoginStyle", "db");

            }
            UIAction(() =>
            {
                tips.SetResourceReference(TextBlock.TextProperty, "InfoSave");
                tips2.SetResourceReference(TextBlock.TextProperty, "InfoTitle");
                PopMessageHelper.ShowMessage(tips.Text, tips2.Text, 3000, MsgStyle.OK);

            });
            if (ini.ExistINIFile())
            {
                string SelectLX = ini.IniReadValue("SingleAppSetting", "LoginStyle");
            }
            System.Windows.Forms.Application.Restart();
            Application.Current.Shutdown();
        }
        private void btnweb_Click(object sender, RoutedEventArgs e)
        {
            ini.IniWriteValue("公司信息", "SingleOrNet", "WebNet"); 
            System.Windows.Forms.Application.Restart();
            Application.Current.Shutdown();
        }

        void UIAction(Action action)//在主线程外激活线程方法
        {
            System.Threading.SynchronizationContext.SetSynchronizationContext(new System.Windows.Threading.DispatcherSynchronizationContext(App.Current.Dispatcher));
            System.Threading.SynchronizationContext.Current.Post(_ => action(), null);
        }
        
        private void close(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = System.Windows.MessageBox.Show("Are you Close System？", "Tips", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                e.Cancel = false;
                System.Windows.Application.Current.Shutdown();
                Environment.Exit(0);
            }
            else
            {
                e.Cancel = true;

            }
        }

    }
}
