﻿using ScannerApp.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ScannerApp
{
    public partial class PrintBarCode : Form
    {
        private BarCodeClass bcc = new BarCodeClass();
        private DocementBase _docement;
        string testSerial = System.DateTime.Now.ToString("yyyyMMddhhmmss").Substring(0, 12);
        int TotalWidth = 300, TotalHight = 100, BarWidth = 100, BarHight = 20;
        IniFiles ini = new IniFiles(Application.StartupPath + @"\ScanAppConfig.INI");

        public PrintBarCode()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
         
            PrintDocument document = new PrintDocument();
            document.DefaultPageSettings.PaperSize = new PaperSize("Custom", 500, 300);
            Margins margins = new Margins(0x87, 20, 5, 20);
            document.DefaultPageSettings.Margins = margins;
            PrintPreviewDialog dialog = new PrintPreviewDialog();
            document.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            //将写好的格式给打印预览控件以便预览 
            dialog.Document = document;
            dialog.ShowDialog();
            //try
            //{
            //    //if (this.CurrentPrintQRCode != null && this.CurrentPrintQRCode.Count() > 0)
            //    {
            //       // document.Print();
            //        Thread.Sleep(1000);
            //    }
            //}
            //catch (Exception exception)
            //{
            //    PopMessageHelper.ShowMessage("错误:此单号无效！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

            //    document.PrintController.OnEndPrint(document, new PrintEventArgs());
            //}

        }
        private void pd_PrintPage(object sender, PrintPageEventArgs e) //触发打印事件
        {
            AssetEntity a = new AssetEntity();
            a.Serial = testSerial;
            a.Category = "testet";
            Bitmap bmp2 = new Bitmap(this.pictureBox1.Width, this.pictureBox1.Height);
            BarcodeHelper.GetPrintPicture(bmp2, a, e);
        }

        private void PrintBarCode_Load(object sender, EventArgs e)
        {
             if(ini.ExistINIFile())
            {
                try
                {
                    TotalWidth = int.Parse(ini.IniReadValue("条码打印", "总长度"));
                    TotalHight = int.Parse(ini.IniReadValue("条码打印", "总宽度"));
                    BarWidth = int.Parse(ini.IniReadValue("条码打印", "码长度"));
                    BarHight = int.Parse(ini.IniReadValue("条码打印", "码宽度"));
                }
                catch { }
            }
            //AssetEntity a = new AssetEntity();
            //a.Serial = testSerial;
            //a.Category = "testet";
            //Bitmap bmp2 = new Bitmap(this.pictureBox1.Width, this.pictureBox1.Height);
            //Image image = BarcodeHelper.GetPrintPicture(bmp2, a, this.pictureBox1.Width, this.pictureBox1.Height);
            //pictureBox1.Image = image;//Image.FromHbitmap(image.GetHbitmap());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
            {
                MessageBox.Show("You Must Load an Image first!");
                return;
            }
            else
            {
                _docement = new imageDocument(pictureBox1.Image);
            }
            _docement.showPrintPreviewDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // bcc.CreateBarCode(pictureBox1, );
            AssetEntity a = new AssetEntity();
            a.Serial = testSerial;
            a.Category = "testet";
            Bitmap bmp2 = new Bitmap(this.pictureBox1.Width, this.pictureBox1.Height);
            Image image= BarcodeHelper.GetPrintPicture(bmp2, a, this.pictureBox1.Width, this.pictureBox1.Height);
            pictureBox1.Image = image;//Image.FromHbitmap(image.GetHbitmap());

        }

        private void button4_Click(object sender, EventArgs e)
        {
              ini.IniWriteValue("条码打印", "总长度",txtzcd.Text);
                ini.IniWriteValue("条码打印", "总宽度",txtzkd.Text);
                ini.IniWriteValue("条码打印", "码长度",txtmcd.Text);
             ini.IniWriteValue("条码打印", "码宽度",txtmkd.Text);

            MessageBox.Show("设置保存成功！");
        }
    }
}
