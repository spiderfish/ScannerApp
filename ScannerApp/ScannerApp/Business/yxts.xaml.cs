﻿using ScannerApp.Common;
using ScannerApp.Common.PLC;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static ScannerApp.Common.PopMessageHelper;

namespace ScannerApp.Business
{
    /// <summary>
    /// yxts.xaml 的交互逻辑
    /// </summary>
    public partial class yxts : UserControl
    {
        IniFiles ini = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\ScanAppConfig.INI");
        System.Windows.Threading.DispatcherTimer timer_heartbeat = new System.Windows.Threading.DispatcherTimer();

        public yxts()
        {
            InitializeComponent();
        }
        private void MouseLeftDown(object sender, MouseButtonEventArgs e)
        {
            loading();
            StackPanel sp= (StackPanel)sender;
            ClickTag(sp.Tag.ToString());
            loadingEnd();

        }

        void ClickTag(string tag)
        {
            switch (tag)
            {
                case "11"://穿袋
                    string M102 = Fun_MXX(102);
                    if (M102 == "1")//txt11.Text == "OFF"||
                    {
                        stack11.Background = Brushes.Green;
                        txt11.Text = "ON";
                    }
                    else if (M102 == "0")
                    {
                        stack11.Background = Brushes.Navy;
                        txt11.Text = "OFF";
                    }
                    else {
                        PopMessageHelper.ShowMessage(M102 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "12"://移袋
                    string M138_ = Fun_ReadMxx(138);
                    if (M138_ == "1") return;
                    string M103 = Fun_MXX(103);
                    if (M103 == "1")//txt12.Text == "OFF"||
                    {
                        stack12.Background = Brushes.Green;
                        txt12.Text = "ON";
                    }
                    else if (M103 == "0")
                    {
                        stack12.Background = Brushes.Navy;
                        txt12.Text = "OFF";
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M103 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "13"://压袋
                    string M109 = Fun_MXX(109);
                    if (M109 == "1")//txt13.Text == "OFF"||
                    {
                        stack13.Background = Brushes.Green;
                        txt13.Text = "ON";
                    }
                    else if (M109 == "0")
                    {
                        stack13.Background = Brushes.Navy;
                        txt13.Text = "OFF";
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M109 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "14"://切袋
                    string M105 = Fun_MXX(105);
                    if (M105 == "1")//txt14.Text == "OFF"||
                    {
                        stack14.Background = Brushes.Green;
                        txt14.Text = "ON";
                    }
                    else if (M105 == "0")
                    {
                        stack14.Background = Brushes.Navy;
                        txt14.Text = "OFF";
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M105 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "21"://分离标签
                    string M108 = Fun_MXX(108);
                    if (M108 == "1")//txt21.Text == "OFF"||
                    {
                        stack21.Background = Brushes.Green;
                        txt21.Text = "ON";
                    }
                    else if (M108 == "0")
                    {
                        stack21.Background = Brushes.Navy;
                        txt21.Text = "OFF";
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M108 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "22"://吸取标签
                    string M111 = Fun_MXX(111);
                    if (M111 == "1")//txt22.Text == "OFF"||
                    {
                        stack22.Background = Brushes.Green;
                        txt22.Text = "ON";
                    }
                    else if (M111 == "0")
                    {
                        stack22.Background = Brushes.Navy;
                        txt22.Text = "OFF";
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M111 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "23"://吸标旋转
                    string   M107_ = Fun_ReadMxx(107);
                    if (M107_ == "1") return;
                    string M106 = Fun_MXX(106);
                    if (M106 == "1")//txt23.Text == "OFF"||
                    {
                        stack23.Background = Brushes.Green;
                        txt23.Text = "ON";
                    }
                    else if (M106 == "0")
                    {
                        stack23.Background = Brushes.Navy;
                        txt23.Text = "OFF";
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M106 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "24"://贴标旋转
                    string M107 = Fun_MXX(107);
                    if (M107 == "1")//txt24.Text == "OFF"||
                    {
                        stack24.Background = Brushes.Green;
                        txt24.Text = "ON";
                    }
                    else if (M107 == "0")
                    {
                        stack24.Background = Brushes.Navy;
                        txt24.Text = "OFF";
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M107 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "31"://罐针插入
                    string M104 = Fun_MXX(104);
                    if (M104 == "1")//txt31.Text == "OFF"||
                    {
                        stack31.Background = Brushes.Green;
                        txt31.Text = "ON";
                    }
                    else if (M104 == "0")
                    {
                        stack31.Background = Brushes.Navy;
                        txt31.Text = "OFF";
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M104 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "32"://泵入
                    string M104_ = Fun_ReadMxx(1);
                    if (M104_ == "1") return;
                    string M0 = Fun_MXX(0);
                    if (M0 == "1")//txt32.Text == "OFF"||
                    {
                        stack32.Background = Brushes.Green;
                        txt32.Text = "ON";
                    }
                    else if (M0 == "0")
                    {
                        stack32.Background = Brushes.Navy;
                        txt32.Text = "OFF";
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M0 + "或M操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "33"://泵入
                    string M104__ = Fun_ReadMxx(0);
                    if (M104__ == "1") return;
                    string M1 = Fun_MXX(1);
                    if (M1 == "1")//txt32.Text == "OFF"||
                    {
                        stack33.Background = Brushes.Green;
                        txt33.Text = "ON";
                    }
                    else if (M1 == "0")
                    {
                        stack33.Background = Brushes.Navy;
                        txt33.Text = "OFF";
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M1 + "或M操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
            }
        }

        /// <summary>
        /// 穿袋:M102  指示灯:X1
        /// 移袋:M103 指示灯:X3
        //压袋:M109 指示灯:X21
        //切袋:M105 指示灯:X7
        //分离标签:M108
        //吸取标签:M111 指示灯:X27
        //吸标选装:M106 指示灯; X23
        // 贴标:M107 指示灯:X25
        // 灌针插入:M104 指示灯:X5
        // 泵入:M0
        /// </summary>
        void LoadData()
        {
            string M102 = Fun_ReadMxx(102);
            stack11.Background= M102 == "1"? Brushes.Green: Brushes.Navy;
            txt11.Text = M102 == "1" ? "ON" : "OFF";

            string M103 = Fun_ReadMxx(103);
            stack12.Background = M103 == "1" ? Brushes.Green : Brushes.Navy;
            txt12.Text = M103 == "1" ? "ON" : "OFF";


            string M109 = Fun_ReadMxx(109);
            stack13.Background = M109 == "1" ? Brushes.Green : Brushes.Navy;
            txt13.Text = M109 == "1" ? "ON" : "OFF";

            string M105 = Fun_ReadMxx(105);
            stack14.Background = M105 == "1" ? Brushes.Green : Brushes.Navy;
            txt14.Text = M105 == "1" ? "ON" : "OFF";

            string M108 = Fun_ReadMxx(108);
            stack21.Background = M108 == "1" ? Brushes.Green : Brushes.Navy;
            txt21.Text = M108 == "1" ? "ON" : "OFF";

            string M111 = Fun_ReadMxx(111);
            stack22.Background = M111 == "1" ? Brushes.Green : Brushes.Navy;
            txt22.Text = M111 == "1" ? "ON" : "OFF";

            string M23 = Fun_ReadMxx(106);
            stack23.Background = M23 == "1" ? Brushes.Green : Brushes.Navy;
            txt23.Text = M23 == "1" ? "ON" : "OFF";


            string M107 = Fun_ReadMxx(107);
            stack24.Background = M107 == "1" ? Brushes.Green : Brushes.Navy;
            txt24.Text = M107 == "1" ? "ON" : "OFF";

            string M104= Fun_ReadMxx(104);
            stack31.Background = M104 == "1" ? Brushes.Green : Brushes.Navy;
            txt31.Text = M104 == "1" ? "ON" : "OFF";

            string M0 = Fun_ReadMxx(0);
            stack32.Background = M0 == "1" ? Brushes.Green : Brushes.Navy;
            txt32.Text = M0 == "1" ? "ON" : "OFF";

            string M1 = Fun_ReadMxx(1);
            stack33.Background = M1 == "1" ? Brushes.Green : Brushes.Navy;
            txt33.Text = M1 == "1" ? "ON" : "OFF";



        }
        string Fun_ReadMxx(int M)
        {
            bool b_result = false;
            int MXX = DMTDeltaPLC.ReadMReg(M, out b_result) == 1 ? 1 : 0; //1为启动，0为关闭
            if (MXX == 1)
            {
                if (b_result == true)
                    return "1";
                else return "0";
                
            }
            else
            {
                return "";
            }
        }

        string Fun_MXX(int M)
        {
            bool b_result = false;
            int MXX = DMTDeltaPLC.ReadMReg(M, out b_result) == 1 ? 1 : 0; //1为启动，0为关闭
            if (MXX==1)
            {
                bool istrue = DMTDeltaPLC.SetMReg(M, !b_result) == 1 ? true : false;
                if (istrue)//操作成功
                {
                    if (!b_result == true)
                        return "1";
                    else return "0";
                }
                else
                {
                    return "";
                }
            }
            else return "读取M"+M+"失败！";

        }

        void UIAction(Action action)//在主线程外激活线程方法
        {
            System.Threading.SynchronizationContext.SetSynchronizationContext(new System.Windows.Threading.DispatcherSynchronizationContext(App.Current.Dispatcher));
            System.Threading.SynchronizationContext.Current.Post(_ => action(), null);
        }


        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            loading();
            if (!DMTDeltaPLC.IsConntect)
            {

            }
            else
            {
                LoadData();
                timer_heartbeat.Interval = TimeSpan.FromSeconds(1);
                timer_heartbeat.Tick += heartbeat;
                timer_heartbeat.Start();

            }

            loadingEnd();
        }
        private void heartbeat(object source, EventArgs e)
        {

            try
            {
                UIAction(() =>
                {
                    LoadData();
                });
              
            }
            catch (Exception ex)
            { 

            }

        }

        private void loading()
        {
            this._loading.Visibility = Visibility.Visible;
        }
        private void loadingEnd()
        {
            this._loading.Visibility = Visibility.Collapsed;
        }


    }
}
