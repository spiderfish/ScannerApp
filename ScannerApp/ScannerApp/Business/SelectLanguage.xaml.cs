﻿using ScannerApp.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScannerApp.Business
{
    /// <summary>
    /// SelectLanguage.xaml 的交互逻辑
    /// </summary>
    public partial class SelectLanguage : UserControl
    {
        IniFiles ini = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\ScanAppConfig.INI");

        public SelectLanguage()
        {
            InitializeComponent();
        }

        private void stack8_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectgq(8);
        }

        private void stack1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectgq(1);
        }
        private void stack2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectgq(2);
        }
        private void stack3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectgq(3);
        }
        private void stack4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectgq(4);
        }
        private void stack5_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectgq(5);
        }
        private void stack6_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectgq(6);
        }
        private void stack7_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectgq(7);
        }

        void selectgq(int i)
        {
            string langName = "zh-CN";
            switch (i)
            {
                case 1:
                    langName = "zh-CN";
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/china.png")); 
                    break;
                case 2:
                    langName = "en-US";
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/england.png"));
                    break;
                case 3:
                    langName = "sp-sp";//西班牙
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/spansh.png"));
                    break;
                case 4:
                    langName = "ko-ko";//韩国
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/gq4.png"));
                    break;
                case 5://德国
                    langName = "ge-ge";//西班牙
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/gq5.png"));
                    break;
                case 6://俄罗斯
                    langName = "ru-ru";
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/gq6.png"));
                    break;
                case 7:
                    langName = "po-po";
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/pty.png"));
                    break;
                case 8:
                    langName = "it-it";
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/gq8.png"));
                   
                    break;
            }
            ini.IniWriteValue("公司信息", "SelectLanguages", langName);
            ResourceDictionary langRd = null;
            try
            {
                //根据名字载入语言文件
                langRd = Application.LoadComponent(new Uri(@"Languages\" + langName + ".xaml", UriKind.Relative)) as ResourceDictionary;
            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.Message);
            }
            if (langRd != null)
            {
                //if (this.Resources.MergedDictionaries.Count > 0)
                //{
                //    this.Resources.MergedDictionaries.Clear();
                //}
                //this.Resources.MergedDictionaries.Add(langRd);
               Application.Current.Resources.MergedDictionaries[0] = langRd;
            }

            }

        private void select_Loaded(object sender, RoutedEventArgs e)
        {
            string langName = "zh-CN";
            try
            {
                langName = ini.IniReadValue("公司信息", "SelectLanguages"); //Common.ConfigHelper.GetIni("COM", "");
            }
            catch { }
            switch (langName)
            {
                case "zh-CN": 
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/china.png"));
                    break;
                case "en-US": 
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/england.png"));
                    break;
                case "sp":
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/spansh.png"));
                    break;
                case "gq4":
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/gq4.png"));
                    break;
                case "gq5":
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/gq5.png"));
                    break;
                case "gq6":
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/gq6.png"));
                    break;
                case "pty":
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/pty.png"));
                    break;
                case "gq8":
                    stackimg.Source = new BitmapImage(new Uri("pack://application:,,,/icon/flag/gq8.png"));

                    break;
            }
        }
    }
}
