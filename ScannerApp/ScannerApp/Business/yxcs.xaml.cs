﻿using ScannerApp.Common;
using ScannerApp.Common.PLC;
using ScannerApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static ScannerApp.Common.PopMessageHelper;

namespace ScannerApp.Business
{
    /// <summary>
    /// yxcs.xaml 的交互逻辑
    /// </summary>
    public partial class yxcs : UserControl
    {
        IniFiles ini = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\ScanAppConfig.INI");
        System.Windows.Threading.DispatcherTimer timer_heartbeat = new System.Windows.Threading.DispatcherTimer();
      
        public yxcs()
        {
            InitializeComponent();
        }
        private void btn_jz(object sender, RoutedEventArgs e)
        {
            loading();
            if (sjgzl.Text == "")
            {
                PopMessageHelper.ShowMessage("请在[当前实际罐装量]中填写当前实际数量，再确认！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);
                return;
            }
            int m91 = DMTDeltaPLC.SetMReg(91, true);
            if (m91 == 1)//操作成功
            {
                btnjz.Background = System.Windows.Media.Brushes.Green;
                
            }
            loadingEnd();
        }
        private void btn_cz(object sender, RoutedEventArgs e)
        {
            loading();
            int m91 = DMTDeltaPLC.SetMReg(92, true);
            if (m91 == 1)//操作成功
            {
                btncz.Background = System.Windows.Media.Brushes.Green;
               
            }
            loadingEnd();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            loading();

            if (!DMTDeltaPLC.IsConntect)
            {
               
            }
            else
            {   //心跳，包括重连
                timer_heartbeat.Interval = TimeSpan.FromMilliseconds(100);
                timer_heartbeat.Tick += heartbeat;
                timer_heartbeat.Start();
                LoadData();
            }
              

            loadingEnd();
        }
        private void heartbeat(object source, EventArgs e)
        {
            try
            {

                UIAction(() =>
                {
                    LoadData();
                });
      

            }
            catch (Exception ex)
            {
               

            }

        }
        void UIAction(Action action)//在主线程外激活线程方法
        {
            System.Threading.SynchronizationContext.SetSynchronizationContext(new System.Windows.Threading.DispatcherSynchronizationContext(App.Current.Dispatcher));
            System.Threading.SynchronizationContext.Current.Post(_ => action(), null);
        }

        void LoadData()
        {


            double result = 0;
            string D408_ = DMTDeltaPLC.ReadDReg(408, out result) == 0 ? result.ToString() : "0";
            D408.Text = (float.Parse(D408_)).ToString("F1");
            //  D480.Text = DMTDeltaPLC.ReadDReg(480, out result) == 0 ? result.ToString() : "";
            D600.Text = DMTDeltaPLC.ReadDReg(600, out result) == 0 ? result.ToString() : "0";
            string D602_ = DMTDeltaPLC.ReadDReg(602, out result) == 0 ? result.ToString() : "0";
            D602.Text = (float.Parse(D602_)/1000).ToString();
            string D420_ = DMTDeltaPLC.ReadDReg(420, out result) == 0 ? result.ToString() : "0";
            D420.Text = (float.Parse(D420_) / 10).ToString();
            string D422_ = DMTDeltaPLC.ReadDReg(422, out result) == 0 ? result.ToString() : "0";
            D422.Text = (float.Parse(D422_) / 10).ToString();
            string D440_ = DMTDeltaPLC.ReadDReg(440, out result) == 0 ? result.ToString() : "0";
            D440.Text = (float.Parse(D440_) / 10).ToString();
            //同D436
            sjgzl.Text = DMTDeltaPLC.ReadDReg(606, out result) == 0 ? result.ToString() : "";
        }

        string Fun_M91()
        {
            bool b_result = false;
            int MXX = DMTDeltaPLC.ReadMReg(91, out b_result) == 1 ? 1 : 0; //1为启动，0为关闭
            if (b_result)
            {
                //点击后m92置1，松开m91置0
            }
            if (MXX == 1)
            {
                bool istrue = DMTDeltaPLC.SetMReg(91, !b_result) == 1 ? true : false;
                if (istrue)//操作成功
                {
                    if (!b_result == true)
                        return "1";
                    else return "0";
                }
                else
                {
                    return "";
                }
            }
            else return "读取M" + MXX + "失败！";

        }

        private void loading()
        {
            this._loading.Visibility = Visibility.Visible;
        }
        private void loadingEnd()
        {
            this._loading.Visibility = Visibility.Collapsed;
        }

        private void btn_tj(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DAdr == 0) return;
                float value = float.Parse(publicclass.PopNum);
                if (DAdr == 422 || DAdr == 440) value = (value * 10);
                if (DAdr == 440 && (int)value > 100)
                {
                    PopMessageHelper.ShowMessage("范围1-100！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);
                    return;
                }
                int sfqd = DMTDeltaPLC.SetDReg(DAdr, (int)value) == 1 ? 1 : 0; //1为启动，0为关闭
                if (sfqd == 1)
                {
                    LoadData();
                    aaa.IsOpen = false;
                    //ShowModalDialog_value(false, 0, "", "");
                }
                else
                {
                    PopMessageHelper.ShowMessage(DAdr + "设定失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                }
                if (DAdr == 408)//D408每次手动设置完成后把D1030清零
                {
                    int D1030 = DMTDeltaPLC.SetDReg(1030, 0) == 1 ? 1 : 0;
                    if (sfqd == 0)
                    {
                        PopMessageHelper.ShowMessage(DAdr + "设定失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);
                    }
                }
            }
            catch (Exception ex)
            {
                PopMessageHelper.ShowMessage(ex.Message, "提示", 3000, MsgStyle.RedCritical_OK);
            }

        }
        private void btn_numclose(object sender, RoutedEventArgs e)
        {
            publicclass.PopNum = "0";
            aaa.IsOpen = false;
        }
        private void _mld(object sender, MouseButtonEventArgs e)
        {
            TextBlock sp = (TextBlock)sender;
            int a = int.Parse(sp.Tag.ToString());
            string tips = "", title = "";
            switch (a)
            {
                case 408:
                    DAdr = 408;
                    tips = "罐装预设量";title = "罐装预设量";
                    break;
                case 600:
                    DAdr = 600;
                    tips = "罐装速度"; title = "罐装速度";
                    break;
                case 422:
                    DAdr = 422;
                    tips = "热合温度"; title = "热合温度";
                    break;
                case 606:
                    DAdr = 606;
                    tips = "当前实际罐装量"; title = "当前实际罐装量";
                    break;
                case 440:
                    DAdr = 440;
                    tips = "热封时间"; title = "热封时间";
                    break;
            }
            aaa.IsOpen = true;
           // ShowModalDialog_value(true,a,tips,title);
        }

        int DAdr = 0;
        private void Dlg_OK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DAdr == 0) return;
                float value = float.Parse(txtvalue.Text);
                if (DAdr == 422 || DAdr == 440) value = (value * 10);
                if (DAdr == 440 && (int)value > 100)
                {
                    PopMessageHelper.ShowMessage("范围1-100！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);
                    return;
                }
                int sfqd = DMTDeltaPLC.SetDReg(DAdr, (int)value) == 1 ? 1 : 0; //1为启动，0为关闭
                if (sfqd == 1)
                {
                    LoadData();
                    ShowModalDialog_value(false, 0, "", "");
                }
                else
                {
                    PopMessageHelper.ShowMessage(DAdr+  "设定失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);
                     
                }
                if (DAdr == 408)//D408每次手动设置完成后把D1030清零
                {
                    int D1030 = DMTDeltaPLC.SetDReg(1030, 0) == 1 ? 1 : 0;
                    if (sfqd == 0)
                    {
                        PopMessageHelper.ShowMessage(DAdr + "设定失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK); 
                    }
                }
            }
            catch (Exception ex)
            {
                PopMessageHelper.ShowMessage(ex.Message, "提示", 3000, MsgStyle.RedCritical_OK);
            }
        }
        private void Dlg_Close_Click(object sender, RoutedEventArgs e)
        {
            ShowModalDialog_value(false, 0, "", "");
        }
        private void ShowModalDialog_value(bool bShow, int ad, string tips, string title)
        {
            this.ModalDialog1.IsOpen = bShow;
            if (bShow)
            {
                DAdr = ad;
                txtsd.Text = tips;
                lbsd.Content = title;
            }
            // this.MainPanel.IsEnabled = !bShow;
        }
        private void relash(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

    }
}
