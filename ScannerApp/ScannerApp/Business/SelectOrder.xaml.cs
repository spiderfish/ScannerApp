﻿using ScannerApp.Common;
using ScannerApp.ScanApp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScannerApp.Business
{
    /// <summary>
    /// SelectOrder.xaml 的交互逻辑
    /// </summary>
    public partial class SelectOrder : Window
    {
        public delegate void PassValuesHandler(object sender, PassValuesEventArgs e);

        public event PassValuesHandler PassValuesEvent;
        private static BasicHttpBinding binding;
        private static EndpointAddress endpoint;
        //ScanApp.ScanAppSoapClient sa = new ScanApp.ScanAppSoapClient();
        //父窗口传值状态
        public string GetIsStatus { get; set; }
        public SelectOrder()
        {
            InitializeComponent();
            binding = new BasicHttpBinding();
            endpoint = new EndpointAddress("http://winpro.gerp.top/WebService/ScanApp.asmx");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (GetIsStatus == "" || GetIsStatus == null)
                GetIsStatus = "-10-";
            // string url = ConfigHelper.GetIni("httpUrl", "");
             
            //var httpClient = Common.HttpHelper.HttpClientService.GetInstance();
            //var data = new Dictionary<string, string>
            //{
            //    { "IsStatus", GetIsStatus } 
            //};
            //var x = httpClient.HttpPost_urlencoded(url+ "/GetCollection_ByConditon", data);
            ////top.gerp.winpro.ScanApp w = new top.gerp.winpro.ScanApp();
            // DataSet lsdt = XmlDatasetConvert.ConvertXMLToDataSet(x); 
             //DataSet lsdt = sa.GetCollection_ByConditon(GetIsStatus);
            using (var client = new ScanAppSoapClient(binding, endpoint))
            {
                // 设置超时
               // client.Endpoint.Behaviors.Add(new WebHttpBehavior { ReceiveTimeout = TimeSpan.FromMinutes(5), SendTimeout = TimeSpan.FromMinutes(5) });

                // 调用服务方法
                var lsdt = client.GetCollection_ByConditon(GetIsStatus);
                // 处理结果
                //打包不跳过
                if (GetIsStatus == "-95-_-96-") btnCancel.Visibility = Visibility.Collapsed;
                datagrid2.ItemsSource = lsdt.Tables[0].DefaultView;

            }
        }
        /// <summary>
        /// 返回给子窗口选择值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
          private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            var b = (DataRowView)this.datagrid2.SelectedItem;
            if (b == null) {
                tips.Content = "Please Select a Vaild Data!";
                return;
            }
             PassValuesEventArgs args = new PassValuesEventArgs("OK", b["id"].ToString(), b["shortcode"].ToString(),b);
             PassValuesEvent(this, args);

            this.Close();
        }
        //跳过
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            var b = (DataRowView)this.datagrid2.SelectedItem;
            if (b == null)
            {
                tips.Content = "Please Select a Vaild Data!";
                return;
            }
            PassValuesEventArgs args = new PassValuesEventArgs("Cancel",b["id"].ToString(), b["shortcode"].ToString(), b);
            PassValuesEvent(this, args);

            this.Close();
        }

        private void Datagrid2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
    public class PassValuesEventArgs : EventArgs
    {
        public string OrderID { get; internal set; }
        public string ShortCode { get; internal set; }
        public string Type { get; internal set; }
        public object RowData { get; internal set; }
        public PassValuesEventArgs(string Type,string OrderID, string ShortCode,object RowData)
        {
            this.Type = Type;
            this.OrderID = OrderID;
            this.ShortCode = ShortCode;
            this.RowData = RowData;
        }
    }
}
