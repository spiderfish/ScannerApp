﻿ 
   using System;
  using System.Collections.Generic;
  using System.ComponentModel;
  using System.Data;
  using System.Drawing;
  using System.Text;
  using System.Windows.Forms;
  //先添加如下引用:
  using System.Diagnostics;
 using System.Runtime.InteropServices;
 namespace ScannerApp.Business
 {
   public partial class VIT_Form : Form
{
    public VIT_Form()
    {
        InitializeComponent();
    }
          
    #region WIN API---引入系统API,用于把程序打开的窗口整合到SHOW页面中 
    #region  宏定义
       private const int SWP_NOOWNERZORDER = 0x200;
    private const int SWP_NOREDRAW = 0x8;
    private const int SWP_NOZORDER = 0x4;
    private const int SWP_SHOWWINDOW = 0x0040;
    private const int WS_EX_MDICHILD = 0x40;
    private const int SWP_FRAMECHANGED = 0x20;
    private const int SWP_NOACTIVATE = 0x10;
    private const int SWP_ASYNCWINDOWPOS = 0x4000;
    private const int SWP_NOMOVE = 0x2;
    private const int SWP_NOSIZE = 0x1;
    private const int GWL_STYLE = (-16);
    private const int WS_VISIBLE = 0x10000000;
    private const int WM_CLOSE = 0x10;
    private const int WS_CHILD = 0x40000000;
    #endregion
    [DllImport("user32.dll", EntryPoint = "GetWindowThreadProcessId", SetLastError = true,
         CharSet = CharSet.Unicode, ExactSpelling = true,
         CallingConvention = CallingConvention.StdCall)]
    private static extern long GetWindowThreadProcessId(long hWnd, long lpdwProcessId);
         [DllImport("user32.dll", SetLastError = true)]
    private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll", SetLastError = true)]
    private static extern long SetParent(IntPtr hWndChild, IntPtr hWndNewParent);
         [DllImport("user32.dll", EntryPoint = "GetWindowLongA", SetLastError = true)]
    private static extern long GetWindowLong(IntPtr hwnd, int nIndex);
         [DllImport("user32.dll", EntryPoint = "SetWindowLongA", SetLastError = true)]
    private static extern long SetWindowLong(IntPtr hwnd, int nIndex, long dwNewLong);
       //private static extern int SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong);
 
       [DllImport("user32.dll", SetLastError = true)]
    private static extern long SetWindowPos(IntPtr hwnd, long hWndInsertAfter, long x, long y, long cx, long cy, long wFlags);
        [DllImport("user32.dll", SetLastError = true)]
    private static extern bool MoveWindow(IntPtr hwnd, int x, int y, int cx, int cy, bool repaint);
         [DllImport("user32.dll", EntryPoint = "PostMessageA", SetLastError = true)]
    private static extern bool PostMessage(IntPtr hwnd, uint Msg, long wParam, long lParam);
 
       #endregion
         Process process = null;
    IntPtr appWin;
    private string exeName = "";
    //在控件改变大小的时候，调用:
    private void splitContainer1_Panel2_Resize(object sender, EventArgs e)
    {
        if (this.appWin != IntPtr.Zero)
        {
            MoveWindow(appWin, 0, 0, this.splitContainer1.Panel2.Width, this.splitContainer1.Panel2.Height, true);
        }
        base.OnResize(e);
    }
      
       //窗口退出时: 
        private void VIT_Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (process != null && !process.HasExited)
                process.Kill();

        }
        // 3 用Process执行其它程序，传入打开窗口命令,运行进程并放入C#窗口中

        private void VIT_Form_Load(object sender, EventArgs e)
        {
            exeName = @"C:\WINDOWS\system32\notepad.exe";
            try
            {
                // Start the process 
                process = System.Diagnostics.Process.Start(this.exeName);
                // Wait for process to be created and enter idle condition 
                process.WaitForInputIdle();
                // Get the main handle
                appWin = process.MainWindowHandle;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error");
            }
            // Put it into this form
            SetParent(appWin, this.splitContainer1.Panel2.Handle);
            // Remove border and whatnot
            // SetWindowLong(appWin, GWL_STYLE, WS_VISIBLE);
            // Move the window to overlay it on this window
            MoveWindow(appWin, 0, 0, this.Width, this.Height, true);
        }

      
    }
 }