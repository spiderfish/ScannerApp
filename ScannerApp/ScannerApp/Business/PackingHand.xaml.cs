﻿using ScannerApp.Common;
using ScannerApp.Common.PLC;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using static ScannerApp.Common.PopMessageHelper;

namespace ScannerApp.Business
{
    /// <summary>
    /// PackingHand.xaml 的交互逻辑
    /// </summary>
    public partial class PackingHand : Window
    {

        IniFiles ini = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\ScanAppConfig.INI");

        public PackingHand()
        {
            InitializeComponent();
        }
        private void btn_close(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btn_close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        //穿袋
        private void btn_cd(object sender, RoutedEventArgs e)
        { ClickTag("11"); }
        //移袋
        private void btn_yd(object sender, RoutedEventArgs e)
        { ClickTag("12"); }
        //灌针
        private void btn_gz(object sender, RoutedEventArgs e)
        { ClickTag("31"); }
        //泵入
        private void btn_br(object sender, RoutedEventArgs e)
        { ClickTag("32"); }
        //泵出
        private void btn_bc(object sender, RoutedEventArgs e)
        { ClickTag("33"); }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!DMTDeltaPLC.IsConntect)
            { 
            }
            else
                loadPLC();
            DispatcherTimer Timer = new DispatcherTimer();
            Timer.Tick += Timer_Tick;
            Timer.Interval = TimeSpan.FromSeconds(1);
            Timer.Start();


        }


        private void Timer_Tick(object sender, EventArgs e)
        {
            //todo
            //获取温度的数值

            //获取年月日
            string dt_ymd = DateTime.Now.ToLongDateString();
            this.nyr.Text = dt_ymd;
            //获取几时几分秒
            string dt_hm = DateTime.Now.ToLongTimeString();
            this.sj.Text = dt_hm;

            //获取今天是星期几
            //string[] Day = new string[] { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
            //string week = Day[Convert.ToInt32(DateTime.Now.DayOfWeek.ToString("d"))].ToString();
            //this.LbWeekday.Content = week;
        }
        private void loadPLC()
        {
            double result = 0;
            string d20_ = DMTDeltaPLC.ReadDReg(20, out result) == 0 ? result.ToString() : "0";
            string rl1 = (float.Parse(d20_)  ).ToString();
            string rl2 = DMTDeltaPLC.ReadDReg(408, out result) == 0 ? result.ToString() : "0";
            rl.Text = rl1 + "/" + rl2;
            string sl1 = DMTDeltaPLC.ReadDReg(220, out result) == 0 ? result.ToString() : "0";
            string sl2 = DMTDeltaPLC.ReadDReg(218, out result) == 0 ? result.ToString() : "0";
            sl.Text = sl1 + "/" + sl2;



            string M102 = Fun_ReadMxx(102);
            cd.Background = M102 == "1" ? Brushes.Navy : Brushes.Blue;
        
            string M103 = Fun_ReadMxx(103);
            yd.Background = M103 == "1" ? Brushes.Navy : Brushes.Blue;
                 
            string M104 = Fun_ReadMxx(104);
            gz.Background = M104 == "1" ? Brushes.Navy : Brushes.Blue;
       
            string M0 = Fun_ReadMxx(1);
            br.Background = M0 == "1" ? Brushes.Navy : Brushes.Blue;

            string M1 = Fun_ReadMxx(0);
            bc.Background = M1 == "1" ? Brushes.Navy : Brushes.Blue;

        }

        bool isbulue = false;
        void ClickTag(string tag)
        {
            switch (tag)
            {
                case "11"://穿袋
                    string M102 = Fun_MXX(102);
                    if (M102 == "1")//txt11.Text == "ON"||
                    {
                        cd.Background = Brushes.Navy; 
                    }
                    else if (M102 == "0")
                    {
                        cd.Background = Brushes.Blue;
                       
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M102 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "12"://移袋
                    string M103 = Fun_MXX(30);
                    if (!isbulue)//txt12.Text == "ON"||
                    {
                        isbulue = true;
                        yd.Background = Brushes.Navy;
                        
                    }
                    else if (isbulue)
                    {
                        isbulue = false;
                        yd.Background = Brushes.Blue; 
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M103 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break; 
                case "31"://罐针插入
                    string M104 = Fun_MXX(104);
                    if (M104 == "1")//txt31.Text == "ON"||
                    {
                        gz.Background = Brushes.Navy; 
                    }
                    else if (M104 == "0")
                    {
                        gz.Background = Brushes.Blue; 
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M104 + "或M91操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;

                case "33"://泵出
                    string M01 = Fun_MXX(1);
                    if (M01 == "1")//txt32.Text == "ON"||
                    {
                        bc.Background = Brushes.Navy;
                    }
                    else if (M01 == "0")
                    {
                        bc.Background = Brushes.Blue;
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M01 + "或M操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
                case "32"://泵入
                    string M0 = Fun_MXX(0);
                    if (M0 == "1")//txt32.Text == "ON"||
                    {
                        br.Background = Brushes.Navy; 
                    }
                    else if (M0 == "0")
                    {
                        br.Background = Brushes.Blue; 
                    }
                    else
                    {
                        PopMessageHelper.ShowMessage(M0 + "或M操作失败！！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

                    }
                    break;
            }
        }

        string Fun_ReadMxx(int M)
        {
            bool b_result = false;
            int MXX = DMTDeltaPLC.ReadMReg(M, out b_result) == 0 ? 1 : 0; //1为启动，0为关闭
            if (MXX == 1)
            {
                if (!b_result == true)
                    return "1";
                else return "0";

            }
            else
            {
                return "";
            }
        }

        string Fun_MXX(int M)
        {
            bool b_result = false;
            int MXX = DMTDeltaPLC.ReadMReg(M, out b_result) == 1 ? 1 : 0; //1为启动，0为关闭
            if (MXX == 1)
            {
                bool istrue = DMTDeltaPLC.SetMReg(M, !b_result) == 1 ? true : false;
                if (istrue)//操作成功
                {
                    if (!b_result == true)
                        return "1";
                    else return "0";
                }
                else
                {
                    return "";
                }
            }
            else return "读取M" + M + "失败！";

        }


      

    }
}
