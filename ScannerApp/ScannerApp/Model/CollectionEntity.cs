﻿namespace ScannerApp
{
    public class CollectionEntity
    {
        private string _cname;
        private string _color;
        private string _corralName;
        private string _shortcode;

        public string cname { get => _cname; set => _cname = value; }
        public string color { get => _color; set => _color = value; }
        public string corralName { get => _corralName; set => _corralName = value; }
        public string shortcode { get => _shortcode; set => _shortcode = value; }
    }
}