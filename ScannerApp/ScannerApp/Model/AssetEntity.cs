﻿namespace ScannerApp
{
    public class AssetEntity
    {
        private string serial;
        private string name;
        private string category;
        private string model;
        private string manufacturer;
        private float price;
        private string department;
        private string manager;
        private string storagePlace;

        public string Serial { get => serial; set => serial = value; }
        public string Name { get => name; set => name = value; }
        public string Category { get => category; set => category = value; }
        public string Model { get => model; set => model = value; }
        public string Manufacturer { get => manufacturer; set => manufacturer = value; }
        public float Price { get => price; set => price = value; }
        public string Department { get => department; set => department = value; }
        public string Manager { get => manager; set => manager = value; }
        public string StoragePlace { get => storagePlace; set => storagePlace = value; }
        public string Remark { get; internal set; }
        public string StartUseTime { get; internal set; }
    }
}