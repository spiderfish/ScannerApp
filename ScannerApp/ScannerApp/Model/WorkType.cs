﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Model
{
   public static class WorkType
    {
     public   enum WorkTypes
        {
            [Description("按单作业")]
            WorkAsOrder,//称重
            [Description("直接作业")]
            DirectWork,//精液采集
            [Description("混合作业")]
            MixWork
        }
    }
}
