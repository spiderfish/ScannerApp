﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.IO;
using System.IO.Ports;
using System.Data;
using System.Windows.Threading;
using System.Drawing;

namespace ScannerApp
{
    public class ViewModel : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }


        private ObservableCollection<TextBlock> listStatus = new ObservableCollection<TextBlock> { };
        public ObservableCollection<TextBlock> ListStatus
        {
            get { return listStatus; }
            set
            {
                listStatus = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ListStatus"));
            }
        }

        /// <summary>
        /// 增加状态信息到列表里
        /// </summary>
        /// <param name="isCommand">true</param>
        /// <param name="strStatus">内容</param>
        /// <param name="type">0green，1yellow，2red</param>
        public void AddItemsToStatus(string strStatus, int type)
        {
            try
            {
                TextBlock tb = new TextBlock();
                tb.Text = strStatus;
                if (type == 0)
                    tb.Foreground = System.Windows.Media.Brushes.Green;
                if (type == 1)
                    tb.Foreground = System.Windows.Media.Brushes.Yellow;
                if (type == 2)
                    tb.Foreground = System.Windows.Media.Brushes.Red;
                ListStatus.Add(tb);

                FocusLastItem();
            }
            catch { }
        }

        public void listStatusClear()
        {
            listStatus.Clear();
        }
        /// <summary>
        /// 委托定义，用于控制界面元素
        /// </summary>
        public delegate void ScrollToEnd();
        public ScrollToEnd FocusLastItem = null;

    }
}