﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Model
{
  public  class PrintWorkDocumentEntity
    {
        
            private string _WorkID;
        private string _SaleID;
        /// <summary>
        /// 作业单号
        /// </summary>
            public string WorkID { get => _WorkID; set => _WorkID = value; }
        public string SaleID { get => _SaleID; set => _SaleID = value; }
    }
 
}
