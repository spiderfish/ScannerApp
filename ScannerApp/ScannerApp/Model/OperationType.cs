﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Model
{
   public static class OperationType
    {
     public   enum OperationTypes
        {
           [Description("称重")]
            cz,//称重
            [Description("精液采集")]
            jycj,//精液采集
            [Description("密度分析")]
            mdfx,//密度分析
            [Description("活力分析")]
            hlfx,//活力分析
            [Description("稀释")]
            xs,//稀释
            [Description("打包")]
            db,//打包
            [Description("测试打印")]
            test//测试打印
        }

        public enum PrintTypes {
            [Description("斑马GX430")]
            Zebra, 
            [Description("佳博")]
            Gainscha,
             [Description("斑马ZD420")]
            ZebraZD420,
        }
    }
}
