﻿using ScannerApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScannerApp
{
    /// <summary>
    /// Window1.xaml 的交互逻辑
    /// </summary>
    public partial class Window1 : Window, INotifyPropertyChanged
    {
        public Window1()
        {
            InitializeComponent();
        }
 
            private int terminalNo;

            public int TerminalNo
            {
                get { return terminalNo; }
                set
                {
                    if (value != terminalNo)
                    {
                        terminalNo = value;
                        OnPropertyChanged("TerminalNo");
                    }
                }
            }


        

            public event PropertyChangedEventHandler PropertyChanged;

            public void OnPropertyChanged(string propertyName)
            {
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            private void btnShow_Click(object sender, RoutedEventArgs e)
            {
                MessageBox.Show(string.Format("终端编号的值为{0}", TerminalNo));
            }
        private void mmm(object sender, MouseButtonEventArgs e)
        {
            aaa.IsOpen = true;
            
        }
        private void close(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("终端编号的值为{0}", publicclass.PopNum));
        }
        private void tt(object sender, RoutedEventArgs e)
        {
            aaa.IsOpen = false;
            MessageBox.Show(string.Format("终端编号的值为{0}", publicclass.PopNum));
        }

        private void Button_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
        }
    }
}
