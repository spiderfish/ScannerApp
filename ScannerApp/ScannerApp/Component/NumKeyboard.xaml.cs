﻿using ScannerApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScannerApp.Component
{
    /// <summary>
    /// NumKeyboard.xaml 的交互逻辑
    /// </summary>
    public partial class NumKeyboard : UserControl
    {
        private static TextBlock _SpinEdit; 
        public NumKeyboard()
        {
            InitializeComponent();
        }
        private void TextBlock_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {


                TextBlock btn = e.OriginalSource as TextBlock;
                if (btn.Name == "Back")
                {
                    txtValue.Text = txtValue.Text.Substring(0, txtValue.Text.Length - 1);
                }
                else
                {
                    txtValue.Text += btn.Text;
                }
                publicclass.PopNum = txtValue.Text.Length == 0 ? "0" : txtValue.Text;
            }
            catch
            {
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            publicclass.PopNum = txtValue.Text.Length == 0 ? "0" : txtValue.Text; 

           // _SpinEdit.Text = (txtValue.Text.Length == 0 ? "0" : txtValue.Text);

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtValue.Text = "";
        }
    }
}
