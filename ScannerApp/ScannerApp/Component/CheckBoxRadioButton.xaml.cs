﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckBoxRadioButton
{
    /// <summary>
    /// SingleCheckBox.xaml 的交互逻辑
    /// </summary>
    public partial class SingleCheckBox : RadioButton
    {
        public SingleCheckBox()
        {
            InitializeComponent();
        }

        private bool hasCheck;
        public bool HasCheck
        {
            get { return hasCheck; }
            set { hasCheck = value; }
        }

        private void RadioButton_Click_1(object sender, RoutedEventArgs e)
        {
            if (this.HasCheck == false)
            {
                this.HasCheck = true;
                this.IsChecked = true;
            }
            else
            {
                this.HasCheck = false;
                this.IsChecked = false;
            }
        }

        private void RadioButton_Unchecked_1(object sender, RoutedEventArgs e)
        {
            this.HasCheck = false;
        }
    }
}