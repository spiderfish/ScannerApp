﻿using ScannerApp.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static ScannerApp.Common.PopMessageHelper;

namespace ScannerApp
{
    
    /// <summary>
    /// ComSet.xaml 的交互逻辑
    /// </summary>
    public partial class ComSet : Window
    {
        public delegate void UpdateMainwindowLabel(string labelContent);

        public event UpdateMainwindowLabel updateMainwindowLabel;

        private List<int> BaudRateList = new List<int>() { 100, 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 56000, 57600, 115200, 128000, 256000, 512000 };
        private List<int> DataBitsList = new List<int>() { 6, 7, 8 };
        private List<double> StopBitsList = new List<double>() { 1, 1.5, 2 };
        private List<string> ParityList = new List<string>() { "None", "Odd"  };
        private string flag = "0";
        IniFiles ini = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\ScanAppConfig.INI");

        public ComSet()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (cbPortName.SelectedValue.ToString() == "" ||
               btl.SelectedValue.ToString() == "" ||
               sjw.SelectedValue.ToString() == "" ||
               tzw.SelectedValue.ToString() == "" ||
               parity.SelectedValue.ToString() == "") {
                PopMessageHelper.ShowMessage("参数选择不能为空！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);

            }
             ini.IniWriteValue("串口设置", "COM", cbPortName.SelectedValue.ToString());  
             ini.IniWriteValue("串口设置", "BaudRate", btl.SelectedValue.ToString());
            ini.IniWriteValue("串口设置", "DataBits", sjw.SelectedValue.ToString());
            ini.IniWriteValue("串口设置", "StopRate", tzw.SelectedValue.ToString());
            ini.IniWriteValue("串口设置", "ParityRate", parity.SelectedValue.ToString());  
             
            //Common.ConfigHelper.SetIni("COM", cbPortName.SelectedValue.ToString());
            //Common.ConfigHelper.SetIni("BaudRate", btl.SelectedValue.ToString());
            //Common.ConfigHelper.SetIni("DataBits", sjw.SelectedValue.ToString());
            //Common.ConfigHelper.SetIni("StopRate", tzw.SelectedValue.ToString());
            //Common.ConfigHelper.SetIni("ParityRate", parity.SelectedValue.ToString());
            this.Close();
        }
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            flag = "1";
            this.Close();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //获取串口
             string[] s = SerialPort.GetPortNames();
            //  cbPortName.Items.AddRange(SerialPort.GetPortNames());
            cbPortName.ItemsSource = s;
            if (cbPortName.Items.Count > 0)
            {
                cbPortName.SelectedIndex = 0;
            }
            btl.ItemsSource = BaudRateList;
            sjw.ItemsSource = DataBitsList;
            tzw.ItemsSource = StopBitsList;
            parity.ItemsSource = ParityList;

            btl.SelectedIndex = 6;
            sjw.SelectedIndex = 2;
            tzw.SelectedIndex = 0;
            parity.SelectedIndex = 0;

            if (ini.ExistINIFile())
            {
                try
                {
                    string Com = ini.IniReadValue("串口设置", "COM"); //Common.ConfigHelper.GetIni("COM", "");
                    string BaudRate = ini.IniReadValue("串口设置", "BaudRate"); //Common.ConfigHelper.GetIni("BaudRate", "");
                    string DataBits = ini.IniReadValue("串口设置", "DataBits");//Common.ConfigHelper.GetIni("DataBits", "");
                    string StopRate = ini.IniReadValue("串口设置", "StopRate");//Common.ConfigHelper.GetIni("StopRate", "");
                    string ParityRate = ini.IniReadValue("串口设置", "ParityRate"); //Common.ConfigHelper.GetIni("ParityRate", "");
                    cbPortName.SelectedValue = Com;
                    btl.SelectedValue = int.Parse( BaudRate);
                    sjw.SelectedValue = int.Parse(DataBits);
                    tzw.SelectedValue = double.Parse( StopRate);
                    parity.SelectedValue = ParityRate;
                }
                catch { }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        { StrikeEvent(); }    //触发事件改变MainWindow的值   
        private void StrikeEvent()
        {
            if (updateMainwindowLabel != null)
            {
                updateMainwindowLabel(flag);
            }
        }
 
       
    }
}
