﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using ScannerApp.Common;

namespace ScannerApp
{
    class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {

            
            // 方式1
            Application app = new Application();
            AutoUpdate.Updater.CheckUpdateStatus();


            IniFiles ini = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\ScanAppConfig.INI");
            string IsInitSet = "N";
            // 方式3
            if (ini.ExistINIFile())
            {
                try
                {
                    IsInitSet = ini.IniReadValue("是否初始化", "IsInitSet");

                }
                catch { }
            }

            if (IsInitSet == "N")
            {
                app.StartupUri = new Uri("MainWindow.xaml", UriKind.Relative);
            }
            else { app.StartupUri = new Uri("navigtion.xaml", UriKind.Relative); }
            app.Run();
        }
    }
}
