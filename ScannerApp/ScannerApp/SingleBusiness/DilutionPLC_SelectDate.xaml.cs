﻿using CountDown;
using MaterialDesignThemes.Wpf;
using ScannerApp.Common;
using ScannerApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using static ScannerApp.Common.PopMessageHelper;

namespace ScannerApp.SingleBusiness
{
    /// <summary>
    /// DilutionPLC_SelectDate.xaml 的交互逻辑
    /// </summary>
    public partial class DilutionPLC_SelectDate : Window
    {
        private DispatcherTimer timer = new DispatcherTimer();

        private ProcessCount processCount;

        public DilutionPLC_SelectDate()
        {
            InitializeComponent();
        }

        private void btn_cancel(object sender, RoutedEventArgs e)
        {
            try
            {
                timer.Stop();
                start.IsEnabled = true; start.Background = Brushes.Green;
                stop.IsEnabled = false; stop.Background = Brushes.Gray;
            }
            catch { }
            
           // this.Hide();
            
        }
        /// <summary>
        /// 到时间，启动M2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_start(object sender, RoutedEventArgs e)
        {
            if (pdate.Text == "" || ptime.Text == "")
            {
                PopMessageHelper.ShowMessage("时间不能为空!\r\n窗口3秒后关闭...", "WinPro系统设置提示", 3000, MsgStyle.RedCritical_OK);

                return;
            }
            DateTime dt = DateTime.Parse( pdate.Text.Replace("/","-") +" "+ ptime.Text+":00");
            if (dt <= DateTime.Now)
            {
                PopMessageHelper.ShowMessage("选择时间不能小于当前时间!\r\n窗口3秒后关闭...", "WinPro系统设置提示", 3000, MsgStyle.RedCritical_OK);

                return;
            }
            TimeSpan ts = dt - DateTime.Now;
            HourArea.Text = ((ts.Days*24)+ts.Hours).ToString();
            MinuteArea.Text = ts.Minutes.ToString();
            SecondArea.Text = ts.Seconds.ToString();
            //设置定时器
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(10000000);   //时间间隔为一秒
            timer.Tick += new EventHandler(timer_Tick);

            //转换成秒数
            Int32 hour = Convert.ToInt32(HourArea.Text);
            Int32 minute = Convert.ToInt32(MinuteArea.Text);
            Int32 second = Convert.ToInt32(SecondArea.Text);

            //处理倒计时的类
            processCount = new ProcessCount(hour * 3600 + minute * 60 + second);
            CountDown += new CountDownHandler(processCount.ProcessCountDown);

            //开启定时器
            timer.Start();
            start.IsEnabled = false;start.Background = Brushes.Gray;
            stop.IsEnabled = true; start.Background = Brushes.Green;

        }

        /// <summary>
        /// Timer触发的事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            if (OnCountDown())
            {
                HourArea.Text = processCount.GetHour();
                MinuteArea.Text = processCount.GetMinute();
                SecondArea.Text = processCount.GetSecond();
            }
            else {
                publicclass.IsPreStart = true;
                this.Close();
                timer.Stop();
            }
              
        }

        /// <summary>
        /// 处理事件
        /// </summary>
        public event CountDownHandler CountDown;
        public bool OnCountDown()
        {
            if (CountDown != null)
                return CountDown();

            return false;
        }
  

    /// <summary>
    /// 处理倒计时的委托
    /// </summary>
    /// <returns></returns>
    public delegate bool CountDownHandler();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            pdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            ptime.Text = DateTime.Now.ToString("HH:ss");
        }
    }
}
