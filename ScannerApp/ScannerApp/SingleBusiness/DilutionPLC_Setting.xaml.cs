﻿using ScannerApp.Common;
using ScannerApp.Common.PLC;
using ScannerApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static ScannerApp.Common.PopMessageHelper;

namespace ScannerApp.SingleBusiness
{
    /// <summary>
    /// DilutionPLC_Setting.xaml 的交互逻辑
    /// </summary>
    public partial class DilutionPLC_Setting : Window
    {
        IniFiles ini = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\ScanAppConfig.INI");

        public DilutionPLC_Setting()
        {
            InitializeComponent(); 
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                double result = 0;
                d422.Text =  DMTDeltaPLC.ReadDReg(422, out result) == 0 ? result.ToString() : "0";// 
                d424.Text = DMTDeltaPLC.ReadDReg(424, out result) == 0 ? result.ToString() : "0"; 
                d426.Text = DMTDeltaPLC.ReadDReg(426, out result) == 0 ? result.ToString() : "0"; 
                d432.Text = DMTDeltaPLC.ReadDReg(432, out result) == 0 ? result.ToString() : "0";
            }
            catch (Exception ex) { }
        }
            //出液称去皮
            private void btn_cy(object sender, TouchEventArgs e) {
            // loading();
            bool o = false;
            int m239 = DMTDeltaPLC.ReadMReg(113, out o);
            if (m239 == 1)//操作成功
            {
                int m96 = DMTDeltaPLC.SetMReg(113, !o);
                if (m96 == 1)//操作成功
                {
                    tips.SetResourceReference(TextBlock.TextProperty, "ook");
                    PopMessageHelper.ShowMessage(tips.Text, "Tips", 3000, MsgStyle.RedCritical_OK);


                }
            }
            // loadingEnd();
        }
        //配置桶去皮
        private void btn_pz(object sender, TouchEventArgs e)
        { 
            bool o = false;
            int m239 = DMTDeltaPLC.ReadMReg(112, out o);
            if (m239 == 1)//操作成功
            {
                int m96 = DMTDeltaPLC.SetMReg(112, !o);
                if (m96 == 1)//操作成功
                {
                    tips.SetResourceReference(TextBlock.TextProperty, "ook");
                    PopMessageHelper.ShowMessage(tips.Text, "Tips", 3000, MsgStyle.RedCritical_OK);

                    //ScannerApp.Common.AutoClosedMsgBox.Show("操作成功", "提示", 3000, MsgBoxStyle.BlueInfo_OK);
                  //  tips.SetResourceReference(TextBlock.TextProperty, "ook");


                }
            }
        }
        //配置桶去皮
        private void btn_cz(object sender, TouchEventArgs e)
        {
            bool o = false;
            int m239 = DMTDeltaPLC.ReadMReg(5, out o);
            if (m239 == 1)//操作成功
            {
                int m96 = DMTDeltaPLC.SetMReg(5, !o);
                if (m96 == 1)//操作成功
                {
                    tips.SetResourceReference(TextBlock.TextProperty, "ook");
                    PopMessageHelper.ShowMessage(tips.Text, "Tips", 3000, MsgStyle.RedCritical_OK);

                }
            }
            try
            {
                double result = 0;
                d422.Text = DMTDeltaPLC.ReadDReg(422, out result) == 0 ? result.ToString() : "0";// 
                d424.Text = DMTDeltaPLC.ReadDReg(424, out result) == 0 ? result.ToString() : "0"; 
                d426.Text = DMTDeltaPLC.ReadDReg(426, out result) == 0 ? result.ToString() : "0"; 
                d432.Text = DMTDeltaPLC.ReadDReg(432, out result) == 0 ? result.ToString() : "0";
            }
            catch (Exception ex) { }
        }
 
        public void rlimitnumber(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9]+");
            e.Handled = re.IsMatch(e.Text);

        }


        private void btn_tj(object sender, RoutedEventArgs e)
        {
            if (publicclass.PopNum != "")
            {
                aaa.IsOpen = false;
                int addr = 0;
                if (Type == "D422") {
                    d422.Text = publicclass.PopNum;
                    addr = 422;
                   // ini.IniWriteValue("SetDilution", "D422", publicclass.PopNum);
                }
               
                if (Type == "D424")
                {
                    d424.Text = publicclass.PopNum;
                    addr = 424;
                   // ini.IniWriteValue("SetDilution", "D424", publicclass.PopNum);
                }
                if (Type == "D432")
                {
                    d432.Text = publicclass.PopNum;
                    addr = 432;
                    // ini.IniWriteValue("SetDilution", "D426", publicclass.PopNum);
                }
                if (Type == "D426")
                {
                    d426.Text = publicclass.PopNum;
                    addr = 426;
                   // ini.IniWriteValue("SetDilution", "D426", publicclass.PopNum);
                }

                if (Type == "D26")
                {
                    d424.Text = publicclass.PopNum;
                    addr = 26;
                    // ini.IniWriteValue("SetDilution", "D424", publicclass.PopNum);
                }
                int i = 0;
                try
                {
                    i = int.Parse(publicclass.PopNum);
                }
                catch
                {
                    ScannerApp.Common.AutoClosedMsgBox.Show("数据转换错误", "提示", 3000, MsgBoxStyle.BlueInfo_OK);

                   // PopMessageHelper.ShowMessage("数据转换错误！" + "！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);
                    return;
                }
                if (i > 100 && addr == 26)
                {
                    ScannerApp.Common.AutoClosedMsgBox.Show("最高输入限制100!", "提示", 3000, MsgBoxStyle.BlueInfo_OK);

                    return;
                }
                   else if (i > 50 && addr == 426)
                {
                    ScannerApp.Common.AutoClosedMsgBox.Show("最高输入限制50!", "提示", 3000, MsgBoxStyle.BlueInfo_OK);

                     return;
                }
                else if (i > 1000)
                {

                    ScannerApp.Common.AutoClosedMsgBox.Show("最高输入限制1000!", "提示", 3000, MsgBoxStyle.BlueInfo_OK);
                  //  PopMessageHelper.ShowMessage("最高输入限制1000！" + "！\r\n窗口3秒后关闭...", "提示", 3000, MsgStyle.RedCritical_OK);
                        return;
                   

                }
                int m239 = DMTDeltaPLC.SetDReg(addr, i);
                if (m239 == 1)//操作成功
                {
                    ScannerApp.Common.AutoClosedMsgBox.Show("成功!", "提示", 3000, MsgBoxStyle.BlueInfo_OK);
 
                }

                // ShowModalDialog(false); 

            }
          
        }
        private void btn_numclose(object sender, RoutedEventArgs e)
        {
            publicclass.PopNum = "0";
            aaa.IsOpen = false;
        }
        string Type = "";
        private void _sdwd22(object sender, MouseButtonEventArgs e)
        {
            Type = "D422";
            aaa.IsOpen = true;
        }
        private void _sdwd24(object sender, MouseButtonEventArgs e)
        {
            Type = "D424";
            aaa.IsOpen = true;
        }
        private void _sdwd426(object sender, MouseButtonEventArgs e)
        {
            Type = "D426";
            aaa.IsOpen = true;
        }
        private void _sdwd26(object sender, MouseButtonEventArgs e)
        {
            Type = "D26";
            aaa.IsOpen = true;
        }
        private void _sdwd432(object sender, MouseButtonEventArgs e)
        {
            Type = "D432";
            aaa.IsOpen = true;
        }


    }
}
