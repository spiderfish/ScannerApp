﻿using ScannerApp.Common;
using ScannerApp.Common.PLC;
using ScannerApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static ScannerApp.Common.PopMessageHelper;

namespace ScannerApp.SingleBusiness
{
    /// <summary>
    /// SecndHome.xaml 的交互逻辑
    /// </summary>
    public partial class SecndHome : UserControl
    {
        IniFiles ini = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\ScanAppConfig.INI");
        string PopFlag = "";
        public SecndHome()
        {
            InitializeComponent();
        }
        private void btn_sz(object sender, RoutedEventArgs e)
        {
            SingleSetting ss = new SingleSetting();
            ss.Show();
        }
        private void btn_closepop(object sender, RoutedEventArgs e)
        {
        
            string close = btnclosepop.IsChecked == true ? "true" : "false";
            ini.IniWriteValue("公司信息", "CloePopWindow", close);
        }
        private void btn_printType(object sender, RoutedEventArgs e)
        {
            string close = btnprintType.IsChecked==true ? "true" : "false";
          
            ini.IniWriteValue("公司信息", "CloePrintType", close);
        }
        
        //语言
        private void btn_language(object sender, RoutedEventArgs e)
        {
            loading();
            selectlanguage.Visibility = Visibility.Visible;
            yxcs.Visibility = Visibility.Collapsed;
            yxts.Visibility = Visibility.Collapsed;
            loadingEnd();
        }

        //运行参数
        private void btn_yxcs(object sender, RoutedEventArgs e)
        {
            PopFlag = "yxcs";
            ShowModalDialog(true);
           
        }
        //运行调试
        private void btn_yxts(object sender, RoutedEventArgs e)
        {
            PopFlag = "yxts";
            ShowModalDialog(true);
           
        }
        private void btn_save(object sender, RoutedEventArgs e)
        {
            loading();
            if (gsmc.Text == "" )
            {
                PopMessageHelper.ShowMessage("公司信息不全！！", "提示", 3000, MsgStyle.RedCritical_OK);
                loadingEnd();
                return;
            }
            if (pickdate.Text == "" || ts.Text == "")
            {
                loadingEnd();
                PopMessageHelper.ShowMessage("Date or days  is empty!", "Tip", 3000, MsgStyle.RedCritical_OK);
            }
            ini.IniWriteValue("公司信息", "ComapyName", gsmc.Text);
            ini.IniWriteValue("公司信息", "ComapnyAddress", gsdz.Text);
            ini.IniWriteValue("公司信息", "CompanyData", pickdate.Text); 
            ini.IniWriteValue("公司信息", "CompanyDay", ts.Text);
            ini.IniWriteValue("公司信息", "logo", logo.Text);
            ini.IniWriteValue("公司信息", "machinename", machinename.Text);
            ini.IniWriteValue("公司信息", "machinestyle", machinestyle.Text);
            ini.IniWriteValue("公司信息", "bz1", bz1.Text);
            ini.IniWriteValue("公司信息", "bz2", bz2.Text);
            loadingEnd();
            PopMessageHelper.ShowMessage("Save Successfully!", "Tip", 3000, MsgStyle.OK);

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            loading();
            gsmc.Text = publicclass.SystemName;
            
                LoadData();
            string close = "false";
            try { close = ini.IniReadValue("公司信息", "CloePopWindow"); }
            catch { }

            btnclosepop.IsChecked = (close == "true" ? true : false);

            string close2 = "false";
            try { close2 = ini.IniReadValue("公司信息", "CloePrintType"); }
            catch { }
            btnprintType.IsChecked = (close2 == "true" ? true : false);

            loadingEnd();
        }

        void LoadData()
        {
            if (ini.ExistINIFile())
            {
                try
                {
                    gsmc.Text= ini.IniReadValue("公司信息", "ComapyName"  );
                    gsdz.Text= ini.IniReadValue("公司信息", "ComapnyAddress");
                    pickdate.Text= ini.IniReadValue("公司信息", "CompanyData"  );
                    ts.Text= ini.IniReadValue("公司信息", "CompanyDay");
                    logo.Text = ini.IniReadValue("公司信息", "logo");
                    machinename.Text = ini.IniReadValue("公司信息", "machinename");
                    machinestyle.Text = ini.IniReadValue("公司信息", "machinestyle");
                    bz1.Text = ini.IniReadValue("公司信息", "bz1");
                    bz2.Text = ini.IniReadValue("公司信息", "bz2");
                }
                catch { }
            }

        }

        private void ShowModalDialog(bool bShow)
        {
          
            this.ModalDialog.IsOpen = bShow;
            // this.MainPanel.IsEnabled = !bShow;
        }
        private void Dlg_BtnOK_Click(object sender, RoutedEventArgs e)
        {
            string pwd = ini.IniReadValue("密码设置", "PLC"); ;
            if (TxtBoxInput.Password == pwd)
            {
                TxtBoxInput.Password = "";
                ShowModalDialog(false);
                if (PopFlag == "yxcs")
                {
                    selectlanguage.Visibility = Visibility.Collapsed;
                    yxcs.Visibility = Visibility.Visible;
                    yxts.Visibility = Visibility.Collapsed;
                    PopFlag = "";
                }
                else if (PopFlag == "yxts")
                {
                    selectlanguage.Visibility = Visibility.Collapsed;
                    yxcs.Visibility = Visibility.Collapsed;
                    yxts.Visibility = Visibility.Visible;
                    PopFlag = "";
                }
            }
            else
            {
                PopMessageHelper.ShowMessage("PASSWORD ERROR！", "WinPro系统设置提示", 3000, MsgStyle.RedCritical_OK);
            }

        }
        private void Dlg_BtnClose_Click(object sender, RoutedEventArgs e)
        {
            ShowModalDialog(false);

        }

        private void loading()
        {
             this._loading.Visibility = Visibility.Visible;
        }
        private void loadingEnd()
        {
            this._loading.Visibility = Visibility.Collapsed;
        }

        private void TextBlock_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {


                TextBlock btn = e.OriginalSource as TextBlock;
                if (btn.Name == "Back")
                {
                    TxtBoxInput.Password = TxtBoxInput.Password.Substring(0, TxtBoxInput.Password.Length - 1);
                }
                else
                {
                    TxtBoxInput.Password += btn.Text;
                }
                // publicclass.PopNum = TxtBoxInput.Text.Length == 0 ? "0" : TxtBoxInput.Text;
            }
            catch
            {
            }
        }
        int stringtoint(string a)
        {
            int i = 0;
            try { i = int.Parse(a); } catch { }
            return i;
        }
        private void ts_mld(object sender, MouseButtonEventArgs e)
        {
            ts.Text  = "0";
            aaa.IsOpen = true;
        }

        private void btn_tj(object sender, RoutedEventArgs e)
        {

            try
            {
                
                string value =  (publicclass.PopNum);
                ts.Text = value;
                    aaa.IsOpen = false; 
            }
            catch (Exception ex)
            {  }
           
        }
        private void btn_numclose(object sender, RoutedEventArgs e)
        {
            publicclass.PopNum = "0";
            aaa.IsOpen = false;
        }

    }
}
