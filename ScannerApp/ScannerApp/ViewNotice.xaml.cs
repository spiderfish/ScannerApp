﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScannerApp
{
    /// <summary>
    /// ViewNotice.xaml 的交互逻辑
    /// </summary>
    public partial class ViewNotice : Window
    {
        public ViewNotice()
        {
            InitializeComponent();
            Init();
        }

        void Init()
        {
            string preurl = Common.ConfigHelper.GetEndpointClientAddress("ScanAppSoap");
            //http://winpro.gerp.top/Images/upload/Basicboar/ZHU.jpg
            Uri uri = new Uri(preurl);
          string  imageurl = uri.Scheme + "://" + uri.Host ;

            wb.Source = new Uri(imageurl + "/WebService/notice.aspx");
           // content.AppendText("aaa");
        }
    }
}
