﻿using ScannerApp.Common; 
using ScannerApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static ScannerApp.Common.PopMessageHelper;

namespace ScannerApp
{
    /// <summary>
    /// setting.xaml 的交互逻辑
    /// </summary>
    public partial class setting : Window
    {
        ScanApp.ScanAppSoapClient sa = new ScanApp.ScanAppSoapClient();
        string DeviceNo = "";
        public setting()
        {
            InitializeComponent();
        }

      

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string mac = Common.MacAddress.GetMacAddress();
            string DevName = DeviceNo;
            if(mac=="") PopMessageHelper.ShowMessage("MAC地址获取失败！检测网卡是否正常？\r\n窗口3秒后关闭...", "WinPro系统设置提示", 3000, MsgStyle.RedCritical_OK);
            if (DevName == "") PopMessageHelper.ShowMessage("请选择一个有效机器名称！\r\n窗口3秒后关闭...", "WinPro系统设置提示", 3000, MsgStyle.RedCritical_OK);

            string para = "{\"DeviceName\":\""+DevName+ "\",\"MacAddress\":\"" + mac + "\"}";
           string msg= sa.PC_DeviceName("ADD",para);
            if (msg == "")
            {
                PopMessageHelper.ShowMessage("绑定成功!!\r\n窗口1秒后关闭...", "WinPro系统设置提示", 1000, MsgStyle.BlueInfo_OK);
                this.Close();
            }
            else
                PopMessageHelper.ShowMessage("绑定失败" + msg + "\r\n窗口3秒后关闭...", "WinPro系统设置提示", 3000, MsgStyle.RedCritical_OK);
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView dr = dataGrid.SelectedItem as DataRowView; if (dr != null)
            {
                tb_No.Text = dr["product_name"].ToString();
                DeviceNo = dr["id"].ToString();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
          DataTable lsdt=  sa.GetList_Basic_Device();
            if(lsdt.Rows.Count<=0) PopMessageHelper.ShowMessage("请联系相关人员，维护机器列表！\r\n窗口3秒后关闭...", "WinPro系统设置提示", 3000, MsgStyle.YellowAlert_OK);

            dataGrid.ItemsSource = lsdt.DefaultView;
        }
    }
}
