﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Common
{
  public  class ActionResult
    {
        public enum ResultType
        {
            Total = 0,
            /// <summary>
            /// 消息结果类型
            /// </summary>
            info = 0,

            /// <summary>
            /// 成功结果类型
            /// </summary>
            success = 1,

            /// <summary>
            /// 警告结果类型
            /// </summary>
            warning = 2,

            /// <summary>
            /// 异常结果类型
            /// </summary>
            error = 3
        }
    }
}
