﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Common
{
   public   class PrintCache
    {
        private static string printString = "";

        public static string PrintString { get => printString; set => printString = value; }
    }
}
