﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace ScannerApp.Common
{
    public class PopMessageHelper
    {
        static Popup popUp = new Popup();
        static System.Timers.Timer timer;
        #region 提示消?息¡é

        /// <summary>
        /// 弹出提示消息标题为提示，按钮为确定
        /// </summary>
        /// <param name="msg"></param>
        public static void ShowMessage(string msg, string title, int time, MsgStyle style)
        {
            //ShowFriendMessage(msg, "提示", MessageBoxButton.OK);

            ScannerApp.Component.PopDialog pborder = new ScannerApp.Component.PopDialog();
            pborder.txtMessage.Text = " " + msg + " ";
            pborder.txttitle.Text = " " + title;
            pborder.UpdateLayout();

            popUp = new Popup();
            popUp.HorizontalAlignment = HorizontalAlignment.Center;
            popUp.VerticalAlignment = VerticalAlignment.Center;
            popUp.Child = pborder;

            if (style == MsgStyle.RedCritical_OK)
            {
                pborder.txtMessage.Foreground = System.Windows.Media.Brushes.Red;
            }
            if (style == MsgStyle.OK)
            {
                pborder.txtMessage.Foreground = System.Windows.Media.Brushes.Green;
            }
            if (style == MsgStyle.YellowAlert_OK)
            {
                pborder.txtMessage.Foreground = System.Windows.Media.Brushes.Yellow;
            }
            if (style == MsgStyle.BlueInfo_OK)
            {
                pborder.txtMessage.Foreground = System.Windows.Media.Brushes.Blue;
            }
            //pborder.myStoryboard.Completed += delegate
            //{
            //    popUp.IsOpen = false;
            //};
            //pborder.myStoryboard.Begin();


            popUp.LayoutUpdated += delegate
            {
                //popUp.Margin = new Thickness(
                //          (App.Current.Host.Content.ActualWidth - pborder.ActualWidth) / 2,
                //          (App.Current.Host.Content.ActualHeight - pborder.ActualHeight) / 2,
                //          0,
                //          0);
                popUp.HorizontalOffset = 450;
                popUp.VerticalOffset = 150;
                popUp.StaysOpen = true;
                System.Threading.Timer timer = new System.Threading.Timer(
                        (state) =>
                        {
                            popUp.Dispatcher.BeginInvoke((Action)delegate ()
                            {
                                popUp.IsOpen = false;
                            });


                        }, null, time, Timeout.Infinite);
            };
            popUp.IsOpen = true;
            timer = new System.Timers.Timer();
            timer.Enabled = true;
            timer.Interval = time; //执行间隔时间,单位为毫秒; 这里实际间隔为1秒钟
            timer.Start();
            timer.Elapsed += new System.Timers.ElapsedEventHandler(heartbeat);


        }
        public static void HideShowMessage()
        {
            popUp.IsOpen = false;
        }
        private static void heartbeat(object source, ElapsedEventArgs e)
        {

            try
            {
                popUp.IsOpen = false;
                timer.Stop();
            }
            catch (Exception ex)
            {


            }

        }
        public static void ShowMessage(string msg, string title, MsgStyle style)
        {
            //ShowFriendMessage(msg, "提示", MessageBoxButton.OK);

            ScannerApp.Component.PopDialog pborder = new ScannerApp.Component.PopDialog();
            pborder.txtMessage.Text = " " + msg + " ";
            pborder.txttitle.Text = " " + title;
            pborder.UpdateLayout();

            popUp = new Popup();
            popUp.HorizontalAlignment = HorizontalAlignment.Center;
            popUp.VerticalAlignment = VerticalAlignment.Center;
            popUp.Child = pborder;

            if (style == MsgStyle.RedCritical_OK)
            {
                pborder.txtMessage.Foreground = System.Windows.Media.Brushes.Red;
            }
            if (style == MsgStyle.OK)
            {
                pborder.txtMessage.Foreground = System.Windows.Media.Brushes.Green;
            }
            if (style == MsgStyle.YellowAlert_OK)
            {
                pborder.txtMessage.Foreground = System.Windows.Media.Brushes.Yellow;
            }
            if (style == MsgStyle.BlueInfo_OK)
            {
                pborder.txtMessage.Foreground = System.Windows.Media.Brushes.Blue;
            }
            //pborder.myStoryboard.Completed += delegate
            //{
            //    popUp.IsOpen = false;
            //};
            //pborder.myStoryboard.Begin();


            popUp.LayoutUpdated += delegate
            {
                //popUp.Margin = new Thickness(
                //          (App.Current.Host.Content.ActualWidth - pborder.ActualWidth) / 2,
                //          (App.Current.Host.Content.ActualHeight - pborder.ActualHeight) / 2,
                //          0,
                //          0);
                popUp.HorizontalOffset = 450;
                popUp.VerticalOffset = 150;
                popUp.StaysOpen = true;

            };
            popUp.IsOpen = true;

        }


        /// <summary>
        /// 弹出提示消息按钮为确定
        /// </summary>
        /// <param name="msg"></param>
        public static void ShowMessage(string msg, string title)
        {
            ShowMessage(msg, title, MessageBoxButton.OK);
        }

        /// <summary>
        /// 弹出提示消息
        /// </summary>
        /// <param name="msg"></param>
        public static void ShowMessage(string msg, string title, MessageBoxButton buttons)
        {
            MessageBox.Show(msg, title, buttons);
        }

        #endregion

        public enum MsgStyle
        {
            OK = 0, OKCancel = 1, AbortRetryIgnore = 2, YesNoCancel = 3, YesNo = 4,
            RetryCancel = 5, CancelRetryContinue = 6,

            //红叉 + ...
            RedCritical_OK = 16, RedCritical_OKCancel = 17, RedCritical_AbortRetryIgnore = 18,
            RedCritical_YesNoCancel = 19, RedCritical_YesNo = 20,
            RedCritical_RetryCancel = 21, RedCritical_CancelRetryContinue = 22,

            //蓝问号 + ...
            BlueQuestion_OK = 32, BlueQuestion_OKCancel = 33, BlueQuestion_AbortRetryIgnore = 34,
            BlueQuestion_YesNoCancel = 35, BlueQuestion_YesNo = 36,
            BlueQuestion_RetryCancel = 37, BlueQuestion_CancelRetryContinue = 38,

            //黄叹号 + ...
            YellowAlert_OK = 48, YellowAlert_OKCancel = 49, YellowAlert_AbortRetryIgnore = 50,
            YellowAlert_YesNoCancel = 51, YellowAlert_YesNo = 52,
            YellowAlert_RetryCancel = 53, YellowAlert_CancelRetryContinue = 54,

            //蓝叹号 + ...
            BlueInfo_OK = 64, BlueInfo_OKCancel = 65, BlueInfo_AbortRetryIgnore = 66,
            BlueInfo_YesNoCancel = 67, BlueInfo_YesNo = 68,
            BlueInfo_RetryCancel = 69, BlueInfo_CancelRetryContinue = 70,
        }


    }
}
