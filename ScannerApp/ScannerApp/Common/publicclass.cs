﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 

namespace ScannerApp.Models
{
    public class publicclass
    {


        private static string _userID;
        private static string _userName;
        private static string _remarks;
        private static string _id;
        private static string _SystemName;
        private static string _systemLogo;
        private static string _macAddress;
        private static string _DeviceName;
        private static bool _isPreStart;
        private static string _isPrintMutil;
        private static string _isPrintTag;

        private static string _PopNum;

     

        public static string UserID { get => _userID; set => _userID = value; }
        public static string UserName { get => _userName; set => _userName = value; }
        public static string Remarks { get => _remarks; set => _remarks = value; }
        public static string Id { get => _id; set => _id = value; }
        public static string SystemName { get => _SystemName; set => _SystemName = value; }
        public static string SystemLogo { get => _systemLogo; set => _systemLogo = value; }
        public static string MacAddress { get => _macAddress; set => _macAddress = value; }
        public static string PopNum { get => _PopNum; set => _PopNum = value; }
        public static string DeviceName { get => _DeviceName; set => _DeviceName = value; }
        public static bool IsPreStart { get => _isPreStart; set => _isPreStart = value; }
        public static string IsPrintTag { get => _isPrintTag; set => _isPrintTag = value; }
        public static string IsPrintMutil { get => _isPrintMutil; set => _isPrintMutil = value; }
    }
}
