﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ServiceModel.Configuration;

namespace ScannerApp.Common
{
  public   class ConfigHelper
    {
        private static Configuration config = null;
        /// <summary>
        /// 从文件中加载配置
        /// </summary>
        /// <param name="key">配置名</param>
        /// <param name="def">默认值</param>
        /// <returns>返回配置，如果不存在则返回默认值</returns>
        public static string GetIni(string key, string def)
        {
            if (config == null)
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            if (config.AppSettings.Settings[key] == null)
                return def;
            else
                return config.AppSettings.Settings[key].Value;
        }
        /// <summary>
        /// 设置文件配置
        /// </summary>
        /// <param name="key">配置名</param>
        /// <param name="value">配置值</param>
        public static void SetIni(string key, string value)
        {
            if (config == null)
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            if (config.AppSettings.Settings[key] == null)
            {
                config.AppSettings.Settings.Add(key, value);
            }
            else
            {
                config.AppSettings.Settings[key].Value = value;
            }
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        /// <summary>
        /// 读取EndpointAddress
        /// </summary>
        /// <param name="endpointName"></param>
        /// <returns></returns>
        public static string GetEndpointClientAddress(string endpointName)
        {
            ClientSection clientSection = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
            foreach (ChannelEndpointElement item in clientSection.Endpoints)
            {
                if (item.Name == endpointName)
                    return item.Address.ToString();
            }
            return string.Empty;
        }


        /// <summary>
        /// 设置EndpointAddress
        /// </summary>
        /// <param name="endpointName"></param>
        /// <param name="address"></param>
        public static bool SetEndpointClientAddress(string endpointName, string address)
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                ClientSection clientSection = config.GetSection("system.serviceModel/client") as ClientSection;
                foreach (ChannelEndpointElement item in clientSection.Endpoints)
                {
                    if (item.Name != endpointName)
                        continue;
                    item.Address = new Uri(address);
                    break;
                }
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("system.serviceModel");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

     
    }
}
