﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Common
{
    public  class PrintEntity
    {
        string bq = "";
        string p_port = "";
        string title = "";
        string barCode = "";
        string address = "";
        string breed = "";
        string lotNumber = "";
        string useBefore = "";
        string prodDate = "";
        string iD = "";
        string fullID = "";
        string iDs="";
        string ml = "";
        string dylsh = "";
        string shortname = "";
        bool issingle=false;
        string sDensity = "";
        string sVitality = "";
        string sVaild = "";
        string sTotal = "";
        string zdy1 = "";
        string zdy2 = "";
        string fontName = "微软雅黑";
        string sLevel = "";
        bool _IsDebug = false;



        public string Bq { get => bq; set => bq = value; }
        public string P_port { get => p_port; set => p_port = value; }
        public string Title { get => title; set => title = value; }
        public string BarCode { get => barCode; set => barCode = value; }
        public string Address { get => address; set => address = value; }
        public string Breed { get => breed; set => breed = value; }
        public string LotNumber { get => lotNumber; set => lotNumber = value; }
        public string UseBefore { get => useBefore; set => useBefore = value; }
        public string ProdDate { get => prodDate; set => prodDate = value; }
        public string ID { get => iD; set => iD = value; }
        public string Ml { get => ml; set => ml = value; }
        public string Dylsh { get => dylsh; set => dylsh = value; }
        public string Shortname { get => shortname; set => shortname = value; }
        public bool Issingle { get => issingle; set => issingle = value; } 
        public string SVitality { get => sVitality; set => sVitality = value; }
        public string SDensity { get => sDensity; set => sDensity = value; }
        public string SVaild { get => sVaild; set => sVaild = value; }
        public string STotal { get => sTotal; set => sTotal = value; }
        public string IDs { get => iDs; set => iDs = value; }
        public string FullID { get => fullID; set => fullID = value; }
        public string Zdy1 { get => zdy1; set => zdy1 = value; }
        public string Zdy2 { get => zdy2; set => zdy2 = value; }
        public string FontName { get => fontName; set => fontName = value; }
        public string SLevel { get => sLevel; set => sLevel = value; }
        public bool IsDebug { get => _IsDebug; set => _IsDebug = value; }
    }
}
