﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScannerApp.Common.Print
{
    class ReadStatus
    {
        public static void SetSerial(string Port, int Baud, int StopBits, int Parity, int DataBits)
        {
            COMPrint.COMPort = Port;
            COMPrint.Baud = Baud;//9600
            COMPrint.StopBits = StopBits;//2
            COMPrint.Parity = Parity;//1
            COMPrint.DataBits = DataBits;//7
        }

        public static string ReadPrintStatus()
        {
            string Result = "-1";
            try
            {
           
                string rx = COMPrint.Read("1B 21 3F"); ;

                if (rx == "Port is not Exist")
                    Result ="-2";
                else if (rx == "Port Already in use by another application")
                    Result = "-3";
                else
                    Result = rx;
            }
            catch(Exception ex)
            {
                Result = ex.Message;
            }
            return Result;
        }
      }
}
