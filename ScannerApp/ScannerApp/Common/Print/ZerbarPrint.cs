﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Zebra.Sdk.Printer.Discovery;
using ScannerApp.Common;
using System.Drawing; 
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using AutoUpdate.Log;
using System.Windows.Media.Imaging; 
using System.IO;
using System.Windows.Forms;

namespace ScannerApp.Common.Print
{
   public class ZerbarPrint
    {
        private static Mutex m_Mutex = new Mutex();
        static string strpath = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\PrintIni.INI";
       static string LogoPath = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\Logo\";

        static IniFiles iniprint = new IniFiles(System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\PrintIniCache.INI");
        static IniFiles ini = new IniFiles(strpath); 
        /// <summary>
        /// 
        /// </summary>
       //static object PrintPara = new {
        static int Hight_LL = 600;//标签高度
        static int Width_PW =800;//标签长度
        static int Font_Size =8;//字体大小
        static int Font_X =80;//字体X轴移动多少
        static int Font_Y= 15;//字体Y轴移动多少(需要累计）
        static int BarCode_Size =4;//二维码放大倍数加多少 
        static int Addres_X = 20;//address 需要下移 
        static int ID_X = 10;//ID 可以下移动10
        static string ZPL_FontStyle = "0";//英文字体
        static string ZPL_FontChStyle = "微软雅黑";
       static string Orgin = "N";//字体方向 N=正常 R=90度旋转 I=180度旋转 B=270度旋转
                             //};



        static bool IsExistIni()
        {

            if (ini.ExistINIFile())
            {
                return true;
            }
            else return false;
        }

        //public static string ZerTSC(string bq, string p_port, string title, string barCode, string address
        //    , string Breed = "", string LotNumber = "", string UseBefore = "", string ProdDate = "", string ID = ""
        //    , string ml = "", string IDS = "", string dylsh = "",
        //    string shortname = "", bool issingle = false
        //    , string sDensity = "", string sVitality = "", string sVaild = "", string sTotal = "")//简称，是否单机
        //{
        //    string flag = ""; string error = "";
        //    if (m_Mutex.WaitOne())
        //    {
        //        try
        //        {
        //            if (!IsExistIni()) flag = "不存在标签配置文件！PrintIni.INI";
        //            StringBuilder sb = new StringBuilder();

        //            string Setting = ini.IniReadValue(PE.Bq, "Setting");
        //            sb.AppendLine(ZPL_start(StrToInt(Setting.Split(',')[0]), StrToInt(Setting.Split(',')[1]))); ;

        //            string QRCODE = ini.IniReadValue(PE.Bq, "QRCODE");
        //            sb.AppendLine(printQRCode(StrToInt(QRCODE.Split(',')[0]), StrToInt(QRCODE.Split(',')[1]), barCode) );

        //            string Title = ini.IniReadValue(PE.Bq, "Title");
        //            sb.AppendLine(ZPL_Text(StrToInt(Title.Split(',')[0]), StrToInt(Title.Split(',')[1]), StrToInt(Title.Split(',')[2]), title));

        //            string adr = ini.IniReadValue(PE.Bq, "ADDRESS");
        //            {
        //                string[] s = address.Split(';');
        //                int y = 20;
        //                for (int i = 0; i < s.Length; i++)
        //                {
        //                    sb.AppendLine(ZPL_Text(StrToInt(adr.Split(',')[0]), StrToInt(adr.Split(',')[1]), StrToInt(adr.Split(',')[2]), s[i]));
        //                          y = y + 20;
        //                }
        //            }
        //            string C1 = ini.IniReadValue(PE.Bq, "Breed");
        //            if (C1.Length > 0)
        //                sb.AppendLine(ZPL_Text(StrToInt(C1.Split(',')[0]), StrToInt(C1.Split(',')[1]), StrToInt(C1.Split(',')[2]), Breed));

        //            string C2 = ini.IniReadValue(PE.Bq, "LotNumber");
        //            if (C2.Length > 0)
        //                sb.AppendLine(ZPL_Text(StrToInt(C2.Split(',')[0]), StrToInt(C2.Split(',')[1]), StrToInt(C2.Split(',')[2]), LotNumber));

        //            string C3 = ini.IniReadValue(PE.Bq, "UseBefore");
        //            error += ">>UseBefore:" + C3 + "|" + UseBefore;
        //            if (C3.Length > 0 && UseBefore != "")
        //            {
        //                string[] s = UseBefore.Split(':');
        //                UseBefore = DateTime.Parse(s[1]).ToString(C3.Split(',')[5]);
        //                sb.AppendLine(ZPL_Text(StrToInt(C3.Split(',')[0]), StrToInt(C3.Split(',')[1]), StrToInt(C3.Split(',')[2]), s[0] + ":" + UseBefore));

        //                //TSCLIB.windowsfont(int.Parse(C3.Split(',')[0]), int.Parse(C3.Split(',')[1]), int.Parse(C3.Split(',')[2]), 0, int.Parse(C3.Split(',')[4]), 0, C3.Split(',')[3], s[0] + ":" + UseBefore);
        //            }
        //            string C4 = ini.IniReadValue(PE.Bq, "ProdDate");
        //            if (C4.Length > 0 && ProdDate != "")
        //            {
        //                string[] s = ProdDate.Split(':');
        //                ProdDate = DateTime.Parse(s[1]).ToString(C4.Split(',')[5]);
        //                sb.AppendLine(ZPL_Text(StrToInt(C4.Split(',')[0]), StrToInt(C4.Split(',')[1]), StrToInt(C4.Split(',')[2]), s[0] + ":" + ProdDate));

        //               // TSCLIB.windowsfont(int.Parse(C4.Split(',')[0]), int.Parse(C4.Split(',')[1]), int.Parse(C4.Split(',')[2]), 0, int.Parse(C4.Split(',')[4]), 0, C4.Split(',')[3], s[0] + ":" + ProdDate);
        //            }

        //            bool IsIDs = true;
        //            string C5 = ini.IniReadValue(PE.Bq, "ID");
        //            string ID_ = ini.IniReadValue(PE.Bq, "ID_");
        //            error += ">>ID_:" + C5 + "|" + ID;
        //            if (C5.Length > 0)//&& bq == "DemoBoarID"
        //            {
        //                IsIDs = false;
        //                string[] s = ID.Split(';');
        //                sb.AppendLine(ZPL_Text(StrToInt(ID_.Split(',')[0]), StrToInt(ID_.Split(',')[1]), StrToInt(ID_.Split(',')[2]), s[0] ));
        //                sb.AppendLine(ZPL_Text(StrToInt(C5.Split(',')[0]), StrToInt(C5.Split(',')[1]), StrToInt(C5.Split(',')[2]), s[1]));


        //            }
        //            string C6 = ini.IniReadValue(PE.Bq, "ml");
        //            if (C6.Length > 0)
        //                sb.AppendLine(ZPL_Text(StrToInt(C6.Split(',')[0]), StrToInt(C6.Split(',')[1]), StrToInt(C6.Split(',')[2]), ml));

        //            // TSCLIB.windowsfont(int.Parse(C6.Split(',')[0]), int.Parse(C6.Split(',')[1]), int.Parse(C6.Split(',')[2]), 0, int.Parse(C6.Split(',')[4]), 0, C6.Split(',')[3], ml);
        //            string C7 = ini.IniReadValue(PE.Bq, "IDs");
        //            string IDs_ = ini.IniReadValue(PE.Bq, "IDs_");
        //            if (C7.Length > 0 && IsIDs)//&& bq == "DemoBoarIDs"
        //            {
        //                string[] s = IDS.Split(';');
        //                int y = 0;
        //                for (int i = 0; i < s.Length; i++)
        //                {
        //                    if (i == 0)
        //                        sb.AppendLine(ZPL_Text(StrToInt(IDs_.Split(',')[0]), StrToInt(IDs_.Split(',')[1]), StrToInt(IDs_.Split(',')[2]), s[0]));

        //                    //TSCLIB.windowsfont(int.Parse(IDs_.Split(',')[0]), int.Parse(IDs_.Split(',')[1]), int.Parse(IDs_.Split(',')[2]), 0, int.Parse(IDs_.Split(',')[4]), 0, IDs_.Split(',')[3], s[0]);
        //                    else
        //                        sb.AppendLine(ZPL_Text(StrToInt(C7.Split(',')[0]), StrToInt(C7.Split(',')[1]), StrToInt(C7.Split(',')[2]), s[i]));
        //                    //TSCLIB.windowsfont(int.Parse(C7.Split(',')[0]), int.Parse(C7.Split(',')[1]) + y, int.Parse(C7.Split(',')[2]), 0, int.Parse(C7.Split(',')[4]), 0, C7.Split(',')[3], s[i]);
        //                    y = y + int.Parse(C7.Split(',')[5]);
        //                }
        //            }

        //            string sn = ini.IniReadValue(PE.Bq, "sn");
        //            if (sn.Length > 0)
        //                sb.AppendLine(ZPL_Text(StrToInt(sn.Split(',')[0]), StrToInt(sn.Split(',')[1]), StrToInt(sn.Split(',')[2]), dylsh));

        //            // TSCLIB.windowsfont(int.Parse(sn.Split(',')[0]), int.Parse(sn.Split(',')[1]), int.Parse(sn.Split(',')[2]), 0, int.Parse(sn.Split(',')[4]), 0, sn.Split(',')[3], dylsh);

        //            if (!issingle)//如果联网，则生效
        //            {
        //                string st = ini.IniReadValue(PE.Bq, "short");
        //                if (st.Length > 0)
        //                    sb.AppendLine(ZPL_Text(StrToInt(st.Split(',')[0]), StrToInt(st.Split(',')[1]), StrToInt(st.Split(',')[2]), shortname));

        //               // TSCLIB.windowsfont(int.Parse(st.Split(',')[0]), int.Parse(st.Split(',')[1]), int.Parse(st.Split(',')[2]), 0, int.Parse(st.Split(',')[4]), 0, st.Split(',')[3], shortname);
        //            }

        //            //新增4个字段
        //            string bqDensity = ini.IniReadValue(PE.Bq, "Density");
        //            string bqVitality = ini.IniReadValue(PE.Bq, "Vitality");
        //            string bqVaild = ini.IniReadValue(PE.Bq, "Vaild");
        //            string bqTotal = ini.IniReadValue(PE.Bq, "Total");
        //            if (bqDensity.Length > 0)
        //              //  TSCLIB.windowsfont(int.Parse(bqDensity.Split(',')[0]), int.Parse(bqDensity.Split(',')[1]), int.Parse(bqDensity.Split(',')[2]), 0, int.Parse(bqDensity.Split(',')[4]), 0, bqDensity.Split(',')[3], sDensity);
        //            sb.AppendLine(ZPL_Text(StrToInt(bqDensity.Split(',')[0]), StrToInt(bqDensity.Split(',')[1]), StrToInt(bqDensity.Split(',')[2]), sDensity));

        //            if (bqVitality.Length > 0)
        //                sb.AppendLine(ZPL_Text(StrToInt(bqVitality.Split(',')[0]), StrToInt(bqVitality.Split(',')[1]), StrToInt(bqVitality.Split(',')[2]), sVitality));

        //          //  TSCLIB.windowsfont(int.Parse(bqVitality.Split(',')[0]), int.Parse(bqVitality.Split(',')[1]), int.Parse(bqVitality.Split(',')[2]), 0, int.Parse(bqVitality.Split(',')[4]), 0, bqVitality.Split(',')[3], sVitality);


        //            if (bqVaild.Length > 0)
        //                sb.AppendLine(ZPL_Text(StrToInt(bqVaild.Split(',')[0]), StrToInt(bqVaild.Split(',')[1]), StrToInt(bqVaild.Split(',')[2]), sVaild));

        //            //TSCLIB.windowsfont(int.Parse(bqVaild.Split(',')[0]), int.Parse(bqVaild.Split(',')[1]), int.Parse(bqVaild.Split(',')[2]), 0, int.Parse(bqVaild.Split(',')[4]), 0, bqVaild.Split(',')[3], sVaild);


        //            if (bqTotal.Length > 0)
        //                sb.AppendLine(ZPL_Text(StrToInt(bqTotal.Split(',')[0]), StrToInt(bqTotal.Split(',')[1]), StrToInt(bqTotal.Split(',')[2]), sTotal));

        //            //TSCLIB.windowsfont(int.Parse(bqTotal.Split(',')[0]), int.Parse(bqTotal.Split(',')[1]), int.Parse(bqTotal.Split(',')[2]), 0, int.Parse(bqTotal.Split(',')[4]), 0, bqTotal.Split(',')[3], sTotal);
        //            sb.AppendLine(ZPL_End());
        //            //发送命令
        //            Common.ZebarPrint.GetZebarPrintSendCode(sb.ToString());
        //        }
        //        catch (Exception ex)
        //        {
        //            flag = ex.Message;
        //           // Logger.Instance.WriteLog(publicclass.UserName + "[打包]" + error + "??" + ex.Message, LogType.Error, "Packing");

        //        }
        //        finally
        //        {
        //            m_Mutex.ReleaseMutex();
        //        }
        //    }

        //    return flag;
        //}
        /*
        //图片打印
        //和佳博比较，修改的地方
        //   全体非汉字，Y周增加10；汉字大小 600, 40；全体汉字字体大小-20
        //1  ADDRESS Y轴增加+35
        //2  二维码大小，增加+2（放大系数）
        */
        public static string ZerTSC2(PrintEntity PE)//简称，是否单机 z
        {
            string flag = ""; string error = "";
            StringBuilder sb = new StringBuilder();

            if (m_Mutex.WaitOne())
            {
                try
                {
                    if (!IsExistIni()) flag = "不存在标签配置文件！PrintIni.INI";
                 
                    string Setting = ini.IniReadValue(PE.Bq, "Setting");
                    string fontName = "";
                    try
                    {
                        fontName= ini.IniReadValue(PE.Bq, "PrintFont");
                    }
                    catch { }
                    sb.AppendLine(ZPL_startCode(StrToInt(Setting.Split(',')[0]), StrToInt(Setting.Split(',')[1]), fontName));
                    //字体方向
                    try
                    {
                        Orgin = Setting.Split(',')[2];
                    }
                    catch { Orgin = "N"; }

                    string QRCODE = ini.IniReadValue(PE.Bq, "QRCODE");
                    if(QRCODE.Length>1)
                    sb.AppendLine(ZPL_QRCode(StrToInt(QRCODE.Split(',')[0]), StrToInt(QRCODE.Split(',')[1]), StrToInt(QRCODE.Split(',')[2]), PE.BarCode));

                    string Title = ini.IniReadValue(PE.Bq, "Title");
                    if (Title.Length > 1)
                        sb.AppendLine(ZPL_Text(StrToInt(Title.Split(',')[0]), StrToInt(Title.Split(',')[1]), StrToInt(Title.Split(',')[2]), PE.Title));

                    string adr = ini.IniReadValue(PE.Bq, "ADDRESS");
                    if(adr.Length>1)
                    {
                        string[] s = PE.Address.Split(';');
                        int y = 0;
                        for (int i = 0; i < s.Length; i++)
                        {
                            sb.AppendLine(ZPL_Text(StrToInt(adr.Split(',')[0]), StrToInt(adr.Split(',')[1])  +y, StrToInt(adr.Split(',')[2]), s[i]));
                            y = y + 20;
                        }
                    }
                    string logo = ini.IniReadValue(PE.Bq, "Logo");
                    if(logo.Length>1)
                    sb.AppendLine(ZPL_Logo(StrToInt(logo.Split(',')[0]), StrToInt(logo.Split(',')[1]), StrToInt(logo.Split(',')[2]), StrToInt(logo.Split(',')[3]))); ;

                    string zdy1 = ini.IniReadValue(PE.Bq, "Zdy1");
                    if (zdy1.Length > 0)
                        sb.AppendLine(ZPL_Text(StrToInt(zdy1.Split(',')[0]), StrToInt(zdy1.Split(',')[1]), StrToInt(zdy1.Split(',')[2]), PE.Zdy1));  
                    string zdy2 = ini.IniReadValue(PE.Bq, "Zdy2");
                    if (zdy2.Length > 0)
                        sb.AppendLine(ZPL_Text(StrToInt(zdy2.Split(',')[0]), StrToInt(zdy2.Split(',')[1]), StrToInt(zdy2.Split(',')[2]), PE.Zdy2));  


                    string C1 = ini.IniReadValue(PE.Bq, "Breed");
                    if (C1.Length > 0)
                        sb.AppendLine(ZPL_Text(StrToInt(C1.Split(',')[0]), StrToInt(C1.Split(',')[1]), StrToInt(C1.Split(',')[2]), PE.Breed));

                    string C2 = ini.IniReadValue(PE.Bq, "LotNumber");
                    if (C2.Length > 0)
                        sb.AppendLine(ZPL_Text(StrToInt(C2.Split(',')[0]), StrToInt(C2.Split(',')[1]), StrToInt(C2.Split(',')[2]), PE.LotNumber));

                    string C3 = ini.IniReadValue(PE.Bq, "UseBefore");
                    error += ">>UseBefore:" + C3 + "|" + PE.UseBefore;
                    if (C3.Length > 0 && PE.UseBefore != "")
                    {
                        string[] s = PE.UseBefore.Split(':');
                        PE.UseBefore = DateTime.Parse(s[1]).ToString(C3.Split(',')[5]);
                        sb.AppendLine(ZPL_Text(StrToInt(C3.Split(',')[0]), StrToInt(C3.Split(',')[1]), StrToInt(C3.Split(',')[2]), s[0] + ":" + PE.UseBefore));

                        //TSCLIB.windowsfont(int.Parse(C3.Split(',')[0]), int.Parse(C3.Split(',')[1]), int.Parse(C3.Split(',')[2]), 0, int.Parse(C3.Split(',')[4]), 0, C3.Split(',')[3], s[0] + ":" + UseBefore);
                    }
                    string C4 = ini.IniReadValue(PE.Bq, "ProdDate");
                    if (C4.Length > 0 && PE.ProdDate != "")
                    {
                        string[] s = PE.ProdDate.Split(':');
                        PE.ProdDate = DateTime.Parse(s[1]).ToString(C4.Split(',')[5]);
                        sb.AppendLine(ZPL_Text(StrToInt(C4.Split(',')[0]), StrToInt(C4.Split(',')[1]), StrToInt(C4.Split(',')[2]), s[0] + ":" + PE.ProdDate));

                        // TSCLIB.windowsfont(int.Parse(C4.Split(',')[0]), int.Parse(C4.Split(',')[1]), int.Parse(C4.Split(',')[2]), 0, int.Parse(C4.Split(',')[4]), 0, C4.Split(',')[3], s[0] + ":" + ProdDate);
                    }

                    bool IsIDs = true;
                    string C5 = ini.IniReadValue(PE.Bq, "ID");
                    string ID_ = ini.IniReadValue(PE.Bq, "ID_");
                    error += ">>ID_:" + C5 + "|" + PE.ID;
                    if (C5.Length > 0)//&& bq == "DemoBoarID"
                    {
                        IsIDs = false;
                        string[] s = PE.ID.Split(';');
                        sb.AppendLine(ZPL_Text(StrToInt(ID_.Split(',')[0]) + ID_X, StrToInt(ID_.Split(',')[1]), StrToInt(ID_.Split(',')[2]), s[0]));
                        sb.AppendLine(ZPL_Text(StrToInt(C5.Split(',')[0])+ ID_X, StrToInt(C5.Split(',')[1]), StrToInt(C5.Split(',')[2]), s[1])); 
                    }
                    string C6 = ini.IniReadValue(PE.Bq, "ml");
                    if (C6.Length > 0)
                        sb.AppendLine(ZPL_Text(StrToInt(C6.Split(',')[0]), StrToInt(C6.Split(',')[1]), StrToInt(C6.Split(',')[2]), PE.Ml));

                    // TSCLIB.windowsfont(int.Parse(C6.Split(',')[0]), int.Parse(C6.Split(',')[1]), int.Parse(C6.Split(',')[2]), 0, int.Parse(C6.Split(',')[4]), 0, C6.Split(',')[3], ml);
                    string C7 = ini.IniReadValue(PE.Bq, "IDs");
                    string IDs_ = ini.IniReadValue(PE.Bq, "IDs_");
                    if (C7.Length > 0 && IsIDs)//&& bq == "DemoBoarIDs"
                    {
                        string[] s = PE.IDs.Split(';');
                        int y = 0;
                        for (int i = 0; i < s.Length; i++)
                        {
                            if (i == 0)
                                sb.AppendLine(ZPL_Text(StrToInt(IDs_.Split(',')[0]), StrToInt(IDs_.Split(',')[1]), StrToInt(IDs_.Split(',')[2]), s[0]));

                            //TSCLIB.windowsfont(int.Parse(IDs_.Split(',')[0]), int.Parse(IDs_.Split(',')[1]), int.Parse(IDs_.Split(',')[2]), 0, int.Parse(IDs_.Split(',')[4]), 0, IDs_.Split(',')[3], s[0]);
                            else
                                sb.AppendLine(ZPL_Text(StrToInt(C7.Split(',')[0]), StrToInt(C7.Split(',')[1]), StrToInt(C7.Split(',')[2]), s[i]));
                            //TSCLIB.windowsfont(int.Parse(C7.Split(',')[0]), int.Parse(C7.Split(',')[1]) + y, int.Parse(C7.Split(',')[2]), 0, int.Parse(C7.Split(',')[4]), 0, C7.Split(',')[3], s[i]);
                            y = y + int.Parse(C7.Split(',')[5]);
                        }
                    }

                  
                    // TSCLIB.windowsfont(int.Parse(sn.Split(',')[0]), int.Parse(sn.Split(',')[1]), int.Parse(sn.Split(',')[2]), 0, int.Parse(sn.Split(',')[4]), 0, sn.Split(',')[3], dylsh);

                  //  if (!PE.Issingle)//如果联网，则生效
                    {
                        string st = ini.IniReadValue(PE.Bq, "short");//短号
                        if (st.Length > 0)
                            sb.AppendLine(ZPL_Text(StrToInt(st.Split(',')[0]), StrToInt(st.Split(',')[1]), StrToInt(st.Split(',')[2]), PE.Shortname));

                        // TSCLIB.windowsfont(int.Parse(st.Split(',')[0]), int.Parse(st.Split(',')[1]), int.Parse(st.Split(',')[2]), 0, int.Parse(st.Split(',')[4]), 0, st.Split(',')[3], shortname);
                    }

                    //新增4个字段
                    string bqDensity = ini.IniReadValue(PE.Bq, "Density");
                    string bqVitality = ini.IniReadValue(PE.Bq, "Vitality");
                    string bqVaild = ini.IniReadValue(PE.Bq, "Vaild");
                    string bqTotal = ini.IniReadValue(PE.Bq, "Total");
                    if (bqDensity.Length > 0)
                        //  TSCLIB.windowsfont(int.Parse(bqDensity.Split(',')[0]), int.Parse(bqDensity.Split(',')[1]), int.Parse(bqDensity.Split(',')[2]), 0, int.Parse(bqDensity.Split(',')[4]), 0, bqDensity.Split(',')[3], sDensity);
                        sb.AppendLine(ZPL_Text(StrToInt(bqDensity.Split(',')[0]), StrToInt(bqDensity.Split(',')[1]), StrToInt(bqDensity.Split(',')[2]), PE.SDensity));

                    if (bqVitality.Length > 0)
                        sb.AppendLine(ZPL_Text(StrToInt(bqVitality.Split(',')[0]), StrToInt(bqVitality.Split(',')[1]), StrToInt(bqVitality.Split(',')[2]), PE.SVitality));

                    //  TSCLIB.windowsfont(int.Parse(bqVitality.Split(',')[0]), int.Parse(bqVitality.Split(',')[1]), int.Parse(bqVitality.Split(',')[2]), 0, int.Parse(bqVitality.Split(',')[4]), 0, bqVitality.Split(',')[3], sVitality);


                    if (bqVaild.Length > 0)
                        sb.AppendLine(ZPL_Text(StrToInt(bqVaild.Split(',')[0]), StrToInt(bqVaild.Split(',')[1]), StrToInt(bqVaild.Split(',')[2]), PE.SVaild));

                    //TSCLIB.windowsfont(int.Parse(bqVaild.Split(',')[0]), int.Parse(bqVaild.Split(',')[1]), int.Parse(bqVaild.Split(',')[2]), 0, int.Parse(bqVaild.Split(',')[4]), 0, bqVaild.Split(',')[3], sVaild);


                    if (bqTotal.Length > 0)
                        sb.AppendLine(ZPL_Text(StrToInt(bqTotal.Split(',')[0]), StrToInt(bqTotal.Split(',')[1]), StrToInt(bqTotal.Split(',')[2]), PE.STotal));
                    string bqlevel = ini.IniReadValue(PE.Bq, "level");
                    if (bqlevel.Length > 0)
                        sb.AppendLine(ZPL_Text(StrToInt(bqlevel.Split(',')[0]), StrToInt(bqlevel.Split(',')[1]), StrToInt(bqlevel.Split(',')[2]), PE.SLevel));

                    string sn = ini.IniReadValue(PE.Bq, "sn");
                    if (sn.Length > 0)
                        sb.AppendLine(ZPL_Text(StrToInt(sn.Split(',')[0]), StrToInt(sn.Split(',')[1]), StrToInt(sn.Split(',')[2]), PE.Dylsh));

                    //   iniprint.IniWriteValue("ZerBar", "Pre", sb.ToString());

                    //byte[] bytes = Encoding.Default.GetBytes(sb.ToString());
                    //string str = Convert.ToBase64String(bytes);
                    //iniprint.IniWriteValue("ZerBar", "Pre", sb.ToString()); 
                    sb.AppendLine(ZPL_End());

                    //发送命令
                    string printsting = sb.ToString();// iniprint.IniReadValue("ZerBar", "Pre") ;
                    //Encoding gb = Encoding.GetEncoding("GB18030");
                    //byte[] gbytes = gb.GetBytes(printsting);
                   
                    //string aa = Encoding.GetEncoding("GB18030").GetString(gbytes);
                    //Logger.Instance.WriteLog(printsting, LogType.Information, "PrintTest");
                    flag = Common.ZebarPrint.GetZebarPrintSendCodeGB18030(printsting);

                }
                catch (Exception ex)
                {
                    flag = ex.Message;
                    // Logger.Instance.WriteLog(publicclass.UserName + "[打包]" + error + "??" + ex.Message, LogType.Error, "Packing");

                }
                finally
                {
                    m_Mutex.ReleaseMutex();
                }
            }

            return flag;
        }


        public static string ZebraBarcode(PrintEntity PE)
        {
            StringBuilder sb = new StringBuilder();
            string QRCODE = ini.IniReadValue(PE.Bq, "QRCODE");
            sb.AppendLine(ZPL_QRCode(StrToInt(QRCODE.Split(',')[0]), StrToInt(QRCODE.Split(',')[1]), StrToInt(QRCODE.Split(',')[2]),  PE.BarCode));
            return sb.ToString();
        }

 
        public static void ZerTSC_LocalImg(PrintEntity PE)
        {
            if (!IsExistIni()) return  ;
            #region Graphic类绘制图片打印
            //开始绘制图片
            int initialWidth = 700, initialHeight = 400;

            string Setting = ini.IniReadValue(PE.Bq, "Setting");
            if (Setting.Length > 0) {
                initialWidth = StrToInt(Setting.Split(',')[0])*10;
                initialHeight = StrToInt(Setting.Split(',')[1]) * 10;
                //字体方向
                try
                {
                    Orgin = Setting.Split(',')[2];
                }
                catch { Orgin = "N"; }
            }
            //设置长度宽度
            Bitmap theBitmap = new Bitmap(initialWidth, initialHeight);
            Graphics theGraphics = Graphics.FromImage(theBitmap);
            //填充的颜色
            Brush bush = new SolidBrush(System.Drawing.Color.Black);
            //呈现质量
            theGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            //背景色
            theGraphics.Clear(System.Drawing.Color.White);
            System.Drawing.Font myFontUnder;
            myFontUnder = new System.Drawing.Font(PE.FontName, 15, FontStyle.Regular);
            float angle = 0;
            // 绘制围绕点旋转的文本  
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            if (Orgin == "R")
            {
                angle = 90;
                //theGraphics.RotateTransform(90.0f);
                format.Alignment = StringAlignment.Center;

            }
            else if (Orgin == "I")
            {
                angle = 180;
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                //theGraphics.RotateTransform(180.0f);
            }
            else if (Orgin == "B")
            {
                angle = 270;
               format.Alignment = StringAlignment.Center;
                //theGraphics.RotateTransform(270.0f);
            }
            //string urlImage = System.AppDomain.CurrentDomain.BaseDirectory;
            System.Drawing.Font myFont;
            System.Drawing.Font myFontT; 
            System.Drawing.Font myFontBigUnder;
            myFontT = new System.Drawing.Font(PE.FontName, 9, FontStyle.Regular);
            myFont = new System.Drawing.Font(PE.FontName, 14, FontStyle.Regular);
         
            myFontBigUnder = new System.Drawing.Font("Century Schoolbook", 8, FontStyle.Regular);
            StringFormat geshi = new StringFormat();
            geshi.Alignment = StringAlignment.Center; //居中(LOGO)
             System.Drawing.Image img = new Bitmap(LogoPath + @"\PrintLogo.png");
            //lOGOimg
             theGraphics.DrawImage(img, 5, 0, (float)(120), (float)(25));
            GraphicsText graphicsText = new GraphicsText();
            graphicsText.Graphics = theGraphics;
            //theGraphics.DrawString("touching tomorrow,today!", myFontT, bush, 15, 55);
            //Conimg
            // System.Drawing.Image img2 = new Bitmap(urlImage + "\Image\consignmentLabel.jpg");
            //  theGraphics.DrawImage(img2, 196, 0, (float)(287), (float)(75));
            //QRCodeimg
            //Image img3 = new Bitmap(urlImage + "\Image\labelQRCode.jpg");
            //theGraphics.DrawImage(img3, 490, 11, (float)(67), (float)(67));

            string Title = ini.IniReadValue(PE.Bq, "Title");
            if (Title.Length > 0)
            {
                //float hg = theGraphics.MeasureString(PE.Title, myFontUnder).Height;//为了使第二、三。。。n行也显示出来 
                //if (Orgin == "R"|| Orgin == "B")
                //    theGraphics.DrawString(PE.Title, myFontUnder, bush, new PointF(StrToInt(Title.Split(',')[0]), -hg));
                //else
               // theGraphics.DrawString(PE.Title, myFontUnder, bush, StrToInt(Title.Split(',')[0]), StrToInt(Title.Split(',')[1]));//+10
                graphicsText.DrawString(PE.Title, myFontUnder, bush, new PointF(StrToInt(Title.Split(',')[0]), StrToInt(Title.Split(',')[1])), format,angle);//+10
            }

            string adr = ini.IniReadValue(PE.Bq, "ADDRESS");
            if (adr.Length > 0)
                graphicsText.DrawString(PE.Address, new Font(PE.FontName, StrToInt(adr.Split(',')[2])), bush, new PointF(StrToInt(adr.Split(',')[0]), StrToInt(adr.Split(',')[1])), format,angle);//+10
            string zdy1 = ini.IniReadValue(PE.Bq, "Zdy1");
            if (zdy1.Length > 0)
                graphicsText.DrawString(PE.Zdy1, new Font(PE.FontName, StrToInt(zdy1.Split(',')[2])), bush, new PointF(StrToInt(zdy1.Split(',')[0]), StrToInt(zdy1.Split(',')[1])), format,angle);//+5
            string zdy2 = ini.IniReadValue(PE.Bq, "Zdy2");
            if (zdy2.Length > 0)
           graphicsText.DrawString(PE.Zdy2, new Font(PE.FontName, StrToInt(zdy2.Split(',')[2])), bush, new PointF(StrToInt(zdy2.Split(',')[0]), StrToInt(zdy2.Split(',')[1])), format,angle);
            string fullName = ini.IniReadValue(PE.Bq, "fullName");
            if (fullName.Length > 0)
                graphicsText.DrawString(PE.Shortname, new Font(PE.FontName, StrToInt(fullName.Split(',')[2])), bush, new PointF(StrToInt(fullName.Split(',')[0]), StrToInt(fullName.Split(',')[1])), format,angle);//+5
       
            string Breed = ini.IniReadValue(PE.Bq, "Breed");
            if (Breed.Length > 0)
                graphicsText.DrawString(PE.Breed, new Font(PE.FontName, StrToInt(Breed.Split(',')[2])), bush, new PointF(StrToInt(Breed.Split(',')[0]), StrToInt(Breed.Split(',')[1])), format,angle);
            string LotNumber = ini.IniReadValue(PE.Bq, "LotNumber");
            if (LotNumber.Length > 0)
                graphicsText.DrawString(PE.LotNumber, new Font(PE.FontName, StrToInt(LotNumber.Split(',')[2])), bush, new PointF(StrToInt(LotNumber.Split(',')[0]), StrToInt(LotNumber.Split(',')[1])), format,angle);
          //  graphicsText.DrawLine(new Pen(Color.Black), new Point(350, hei + txtUnder * 2), new Point(500, hei + txtUnder * 2));
            string C3 = ini.IniReadValue(PE.Bq, "UseBefore");
          
            if (C3.Length > 0 && PE.UseBefore != "")
            {
                string[] s = PE.UseBefore.Split(':');
                PE.UseBefore = DateTime.Parse(s[1]).ToString(C3.Split(',')[5]);
               // sb.AppendLine(ZPL_Text(StrToInt(C3.Split(',')[0]), StrToInt(C3.Split(',')[1]), format,angle, StrToInt(C3.Split(',')[2]), s[0] + ":" + PE.UseBefore));
                graphicsText.DrawString(s[0] + ":" + PE.UseBefore, new Font(PE.FontName, StrToInt(C3.Split(',')[2])), bush, new PointF(StrToInt(C3.Split(',')[0]), StrToInt(C3.Split(',')[1])), format,angle);
          }
            string C4 = ini.IniReadValue(PE.Bq, "ProdDate");
            if (C4.Length > 0 && PE.ProdDate != "")
            {
                string[] s = PE.ProdDate.Split(':');
                PE.ProdDate = DateTime.Parse(s[1]).ToString(C4.Split(',')[5]);
                graphicsText.DrawString(s[0] + ":" + PE.ProdDate, new Font(PE.FontName, StrToInt(C4.Split(',')[2])), bush, new PointF(StrToInt(C4.Split(',')[0]), StrToInt(C4.Split(',')[1])), format,angle);

                // TSCLIB.windowsfont(int.Parse(C4.Split(',')[0]), int.Parse(C4.Split(',')[1]), format,angle, int.Parse(C4.Split(',')[2]), 0, int.Parse(C4.Split(',')[4]), 0, C4.Split(',')[3], s[0] + ":" + ProdDate);
            }
            bool IsIDs = true;
            string C5 = ini.IniReadValue(PE.Bq, "ID");
            string ID_ = ini.IniReadValue(PE.Bq, "ID_"); 
            if (C5.Length > 0)//&& bq == "DemoBoarID"
            {
                IsIDs = false;
                string[] s = PE.ID.Split(';');
                graphicsText.DrawString(s[0], new Font(PE.FontName, StrToInt(ID_.Split(',')[2])), bush, new PointF(StrToInt(ID_.Split(',')[0]), StrToInt(ID_.Split(',')[1])), format,angle);
                graphicsText.DrawString(s[1], new Font(PE.FontName, StrToInt(C5.Split(',')[2])), bush, new PointF(StrToInt(C5.Split(',')[0]), StrToInt(C5.Split(',')[1])), format,angle);
                 
       
            }
            string C6 = ini.IniReadValue(PE.Bq, "ml");
            if (C6.Length > 0) 
                graphicsText.DrawString(PE.Ml, new Font(PE.FontName, StrToInt(C6.Split(',')[2])), bush, new PointF(StrToInt(C6.Split(',')[0]), StrToInt(C6.Split(',')[1])), format,angle);
            string C7 = ini.IniReadValue(PE.Bq, "IDs");
            string IDs_ = ini.IniReadValue(PE.Bq, "IDs_");
            if (C7.Length > 0 && IsIDs)//&& bq == "DemoBoarIDs"
            {
                string[] s = PE.IDs.Split(';');
                int y = 0;
                for (int i = 0; i < s.Length; i++)
                {
                    if (i == 0) 
                    graphicsText.DrawString(s[0], new Font(PE.FontName, StrToInt(IDs_.Split(',')[2])), bush, new PointF(StrToInt(IDs_.Split(',')[0]), StrToInt(IDs_.Split(',')[1])), format,angle);
                    else
                        graphicsText.DrawString(s[i], new Font(PE.FontName, StrToInt(C7.Split(',')[2])), bush, new PointF(StrToInt(C7.Split(',')[0]), StrToInt(C7.Split(',')[1])), format,angle);
                      y = y + int.Parse(C7.Split(',')[5]);
                }
            }

            if (!PE.Issingle)//如果联网，则生效
            {
                string st = ini.IniReadValue(PE.Bq, "short");
                if (st.Length > 0)
                    graphicsText.DrawString(PE.Shortname, new Font(PE.FontName, StrToInt(st.Split(',')[2])), bush, new PointF(StrToInt(st.Split(',')[0]), StrToInt(st.Split(',')[1])), format,angle);
            }
            //新增4个字段
            string bqDensity = ini.IniReadValue(PE.Bq, "Density");
            string bqVitality = ini.IniReadValue(PE.Bq, "Vitality");
            string bqVaild = ini.IniReadValue(PE.Bq, "Vaild");
            string bqTotal = ini.IniReadValue(PE.Bq, "Total");
            if (bqDensity.Length > 0)
                graphicsText.DrawString(PE.SDensity, new Font(PE.FontName, StrToInt(bqDensity.Split(',')[2])), bush, new PointF(StrToInt(bqDensity.Split(',')[0]), StrToInt(bqDensity.Split(',')[1])), format,angle); 
   
            if (bqVitality.Length > 0)
                  graphicsText.DrawString(PE.SVitality, new Font(PE.FontName, StrToInt(bqVitality.Split(',')[2])), bush, new PointF(StrToInt(bqVitality.Split(',')[0]), StrToInt(bqVitality.Split(',')[1])), format,angle);
 
            if (bqVaild.Length > 0)
                graphicsText.DrawString(PE.SVaild, new Font(PE.FontName, StrToInt(bqVaild.Split(',')[2])), bush, new PointF(StrToInt(bqVaild.Split(',')[0]), StrToInt(bqVaild.Split(',')[1])), format,angle);
         
            if (bqTotal.Length > 0)
                graphicsText.DrawString(PE.STotal, new Font(PE.FontName,StrToInt(bqTotal.Split(',')[2])), bush, new PointF(StrToInt(bqTotal.Split(',')[0]), StrToInt(bqTotal.Split(',')[1])), format,angle);
            if (PE.IsDebug)
            {
                //结束图片绘制
                var imgTPK = LogoPath + @"\PrintImg\PrePrint.jpg";
                if (!Directory.Exists(LogoPath + @"\PrintImg\"))
                    Directory.CreateDirectory(LogoPath + @"\PrintImg\");
                //保存图片
                theBitmap.Save(imgTPK);
            }
            #endregion
        }



        public static Bitmap ZerTSC3_Bitmap(PrintEntity PE)
        {
            if (!IsExistIni()) return null;
            #region Graphic类绘制图片打印
            //开始绘制图片
            int initialWidth = 700, initialHeight = 400;
            string Setting = ini.IniReadValue(PE.Bq, "Setting");
            if (Setting.Length > 0)
            {
                initialWidth = StrToInt(Setting.Split(',')[0]) * 15;
                initialHeight = StrToInt(Setting.Split(',')[1]) * 15;
                try
                {
                    Orgin = Setting.Split(',')[2];
                }
                catch { }
            }
            //设置长度宽度
            Bitmap theBitmap = new Bitmap(initialWidth, initialHeight);
            Graphics theGraphics = Graphics.FromImage(theBitmap);
            //填充的颜色
            Brush bush = new SolidBrush(System.Drawing.Color.Black);
            //呈现质量
            theGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //背景色
            theGraphics.Clear(System.Drawing.Color.White);
            GraphicsText graphicsText = new GraphicsText();
            graphicsText.Graphics = theGraphics;
            //R=90度旋转 I=180度旋转 B=270度旋转
            System.Drawing.Font myFontUnder;
            myFontUnder = new System.Drawing.Font(PE.FontName, 15, FontStyle.Regular);
            float angle = 0;
            // 绘制围绕点旋转的文本  
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            if (Orgin == "R")
            {
                angle = 90;
                //theGraphics.RotateTransform(90.0f);
                format.Alignment = StringAlignment.Center;

            }
            else if (Orgin == "I")
            {
                angle = 180;
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                //theGraphics.RotateTransform(180.0f);
            }
            else if (Orgin == "B")
            {
                angle = 270;
                format.Alignment = StringAlignment.Center;
                //theGraphics.RotateTransform(270.0f);
            }
            //string urlImage = System.AppDomain.CurrentDomain.BaseDirectory;
            System.Drawing.Font myFont;
            System.Drawing.Font myFontT; 
            System.Drawing.Font myFontBigUnder;
            myFontT = new System.Drawing.Font(PE.FontName, 9, FontStyle.Regular);
            myFont = new System.Drawing.Font(PE.FontName, 14, FontStyle.Regular);
            myFontUnder = new System.Drawing.Font(PE.FontName, 15, FontStyle.Regular);
            myFontBigUnder = new System.Drawing.Font("Century Schoolbook", 8, FontStyle.Regular);
            StringFormat geshi = new StringFormat();
            geshi.Alignment = StringAlignment.Center; //居中(LOGO)
            System.Drawing.Image img = new Bitmap(LogoPath + @"\PrintLogo.png");
            //lOGOimg
            string logo = ini.IniReadValue(PE.Bq, "Logo");
            if (logo.Length > 0)
                theGraphics.DrawImage(img, StrToInt(logo.Split(',')[0]),  StrToInt(logo.Split(',')[1]),  StrToInt(logo.Split(',')[2]), StrToInt(logo.Split(',')[3]));
            //theGraphics.DrawString("touching tomorrow,today!", myFontT, bush, 15, 55);
            //Conimg
            // System.Drawing.Image img2 = new Bitmap(urlImage + "\Image\consignmentLabel.jpg");
            //  theGraphics.DrawImage(img2, 196, 0, (float)(287), (float)(75));
            //QRCodeimg
            //Image img3 = new Bitmap(urlImage + "\Image\labelQRCode.jpg");
            //theGraphics.DrawImage(img3, 490, 11, (float)(67), (float)(67));

            string Title = ini.IniReadValue(PE.Bq, "Title");
            if (Title.Length > 0)
            {
                myFontUnder = new System.Drawing.Font(Title.Split(',')[3], StrToInt(Title.Split(',')[2]), Title.Split(',')[4]=="1"?FontStyle.Bold:FontStyle.Regular);
                graphicsText.DrawString(PE.Title, myFontUnder, bush, new PointF(StrToInt(Title.Split(',')[0]), StrToInt(Title.Split(',')[1])), format, angle);//+10
            }

            string adr = ini.IniReadValue(PE.Bq, "ADDRESS");
            if (adr.Length > 0)
            {
                myFontUnder = new System.Drawing.Font(adr.Split(',')[3], StrToInt(adr.Split(',')[2]), adr.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);
                graphicsText.DrawString(PE.Address, myFontUnder, bush, new PointF(StrToInt(adr.Split(',')[0]), StrToInt(adr.Split(',')[1])), format, angle);//+10

            }
            string zdy1 = ini.IniReadValue(PE.Bq, "Zdy1");
            if (zdy1.Length > 0)
            {
                myFontUnder = new System.Drawing.Font(zdy1.Split(',')[3], StrToInt(zdy1.Split(',')[2]), zdy1.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);

                graphicsText.DrawString(PE.Zdy1, myFontUnder, bush, new PointF(StrToInt(zdy1.Split(',')[0]), StrToInt(zdy1.Split(',')[1])), format, angle);//+5


            }
            string zdy2 = ini.IniReadValue(PE.Bq, "Zdy2");
            if (zdy2.Length > 0)
            {
                myFontUnder = new System.Drawing.Font(zdy2.Split(',')[3], StrToInt(zdy2.Split(',')[2]), zdy2.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);

                graphicsText.DrawString(PE.Zdy2, myFontUnder, bush, new PointF(StrToInt(zdy2.Split(',')[0]), StrToInt(zdy2.Split(',')[1])), format, angle);

            }
            string fullName = ini.IniReadValue(PE.Bq, "fullName");
            if (fullName.Length > 0)
            {
                myFontUnder = new System.Drawing.Font(fullName.Split(',')[3], StrToInt(fullName.Split(',')[2]), fullName.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);

                graphicsText.DrawString(PE.Shortname, myFontUnder, bush, new PointF(StrToInt(fullName.Split(',')[0]), StrToInt(fullName.Split(',')[1])), format, angle);//+5

            } 
            string Breed = ini.IniReadValue(PE.Bq, "Breed");
            if (Breed.Length > 0)
            {
                myFontUnder = new System.Drawing.Font(Breed.Split(',')[3], StrToInt(Breed.Split(',')[2]), Breed.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular); 
                graphicsText.DrawString(PE.Breed, myFontUnder, bush, new PointF(StrToInt(Breed.Split(',')[0]), StrToInt(Breed.Split(',')[1])), format, angle);

            }
            string LotNumber = ini.IniReadValue(PE.Bq, "LotNumber");
            if (LotNumber.Length > 0)
            {
                myFontUnder = new System.Drawing.Font(LotNumber.Split(',')[3], StrToInt(LotNumber.Split(',')[2]), LotNumber.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);

                graphicsText.DrawString(PE.LotNumber, myFontUnder, bush, new PointF(StrToInt(LotNumber.Split(',')[0]), StrToInt(LotNumber.Split(',')[1])), format, angle);

            }
            //  graphicsText.DrawLine(new Pen(Color.Black), new Point(350, hei + txtUnder * 2), new Point(500, hei + txtUnder * 2));
            string C3 = ini.IniReadValue(PE.Bq, "UseBefore");

            if (C3.Length > 0 && PE.UseBefore != "")
            {
                string[] s = PE.UseBefore.Split(':');
                myFontUnder = new System.Drawing.Font(C3.Split(',')[3], StrToInt(C3.Split(',')[2]), C3.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);

                PE.UseBefore = DateTime.Parse(s[1]).ToString(C3.Split(',')[5]);
                // sb.AppendLine(ZPL_Text(StrToInt(C3.Split(',')[0]), StrToInt(C3.Split(',')[1]), format,angle, StrToInt(C3.Split(',')[2]), s[0] + ":" + PE.UseBefore));
                graphicsText.DrawString(s[0] + ":" + PE.UseBefore, myFontUnder, bush, new PointF(StrToInt(C3.Split(',')[0]), StrToInt(C3.Split(',')[1])), format, angle);
            }
            string C4 = ini.IniReadValue(PE.Bq, "ProdDate");
            if (C4.Length > 0 && PE.ProdDate != "")
            {
                string[] s = PE.ProdDate.Split(':');
                myFontUnder = new System.Drawing.Font(C4.Split(',')[3], StrToInt(C4.Split(',')[2]), C4.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);

                PE.ProdDate = DateTime.Parse(s[1]).ToString(C4.Split(',')[5]);
                graphicsText.DrawString(s[0] + ":" + PE.ProdDate, myFontUnder, bush, new PointF(StrToInt(C4.Split(',')[0]), StrToInt(C4.Split(',')[1])), format, angle);

                // TSCLIB.windowsfont(int.Parse(C4.Split(',')[0]), int.Parse(C4.Split(',')[1]), format,angle, int.Parse(C4.Split(',')[2]), 0, int.Parse(C4.Split(',')[4]), 0, C4.Split(',')[3], s[0] + ":" + ProdDate);
            }
            bool IsIDs = true;
            string C5 = ini.IniReadValue(PE.Bq, "ID");
            string ID_ = ini.IniReadValue(PE.Bq, "ID_");
            if (C5.Length > 0)//&& bq == "DemoBoarID"
            {
                IsIDs = false;
                string[] s = PE.ID.Split(';');
                myFontUnder = new System.Drawing.Font(ID_.Split(',')[3], StrToInt(ID_.Split(',')[2]), ID_.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular); 
                graphicsText.DrawString(s[0], myFontUnder, bush, new PointF(StrToInt(ID_.Split(',')[0]), StrToInt(ID_.Split(',')[1])), format, angle);
                myFontUnder = new System.Drawing.Font(C5.Split(',')[3], StrToInt(C5.Split(',')[2]), C5.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular); 
                graphicsText.DrawString(s[1], myFontUnder, bush, new PointF(StrToInt(C5.Split(',')[0]), StrToInt(C5.Split(',')[1])), format, angle);


            }
            string C6 = ini.IniReadValue(PE.Bq, "ml");
            if (C6.Length > 0)
            {
                myFontUnder = new System.Drawing.Font(C6.Split(',')[3], StrToInt(C6.Split(',')[2]), C6.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);

                graphicsText.DrawString(PE.Ml, myFontUnder, bush, new PointF(StrToInt(C6.Split(',')[0]), StrToInt(C6.Split(',')[1])), format, angle);

            }
            string C7 = ini.IniReadValue(PE.Bq, "IDs");
            string IDs_ = ini.IniReadValue(PE.Bq, "IDs_");
            if (C7.Length > 0 && IsIDs)//&& bq == "DemoBoarIDs"
            {
                string[] s = PE.IDs.Split(';');
                int y = 0;
                for (int i = 0; i < s.Length; i++)
                {
                    if (i == 0)
                    {
                        myFontUnder = new System.Drawing.Font(IDs_.Split(',')[3], StrToInt(IDs_.Split(',')[2]), IDs_.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);

                        graphicsText.DrawString(s[0], myFontUnder, bush, new PointF(StrToInt(IDs_.Split(',')[0]), StrToInt(IDs_.Split(',')[1])), format, angle);

                    }
                    else {
                        myFontUnder = new System.Drawing.Font(C7.Split(',')[3], StrToInt(C7.Split(',')[2]), C7.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);

                        graphicsText.DrawString(s[i], myFontUnder, bush, new PointF(StrToInt(C7.Split(',')[0]), StrToInt(C7.Split(',')[1])), format, angle);

                    }
                    y = y + int.Parse(C7.Split(',')[5]);
                }
            }

            if (!PE.Issingle)//如果联网，则生效
            {
                string st = ini.IniReadValue(PE.Bq, "short");
                if (st.Length > 0) {
                    myFontUnder = new System.Drawing.Font(st.Split(',')[3], StrToInt(st.Split(',')[2]), st.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);

                    graphicsText.DrawString(PE.Shortname, myFontUnder, bush, new PointF(StrToInt(st.Split(',')[0]), StrToInt(st.Split(',')[1])), format, angle);

                }
            }
            //新增4个字段
            string bqDensity = ini.IniReadValue(PE.Bq, "Density");
            string bqVitality = ini.IniReadValue(PE.Bq, "Vitality");
            string bqVaild = ini.IniReadValue(PE.Bq, "Vaild");
            string bqTotal = ini.IniReadValue(PE.Bq, "Total");
            if (bqDensity.Length > 0)
            {
                myFontUnder = new System.Drawing.Font(bqDensity.Split(',')[3], StrToInt(bqDensity.Split(',')[2]), bqDensity.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);

                graphicsText.DrawString(PE.SDensity, myFontUnder, bush, new PointF(StrToInt(bqDensity.Split(',')[0]), StrToInt(bqDensity.Split(',')[1])), format, angle);

            }

            if (bqVitality.Length > 0) {
                myFontUnder = new System.Drawing.Font(bqVitality.Split(',')[3], StrToInt(bqVitality.Split(',')[2]), bqVitality.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);
                graphicsText.DrawString(PE.SVitality, myFontUnder, bush, new PointF(StrToInt(bqVitality.Split(',')[0]), StrToInt(bqVitality.Split(',')[1])), format, angle);

            }

            if (bqVaild.Length > 0) {
                myFontUnder = new System.Drawing.Font(bqVaild.Split(',')[3], StrToInt(bqVaild.Split(',')[2]), bqVaild.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);
                graphicsText.DrawString(PE.SVaild, myFontUnder, bush, new PointF(StrToInt(bqVaild.Split(',')[0]), StrToInt(bqVaild.Split(',')[1])), format, angle);

            }

            if (bqTotal.Length > 0) {
                myFontUnder = new System.Drawing.Font(bqTotal.Split(',')[3], StrToInt(bqTotal.Split(',')[2]), bqTotal.Split(',')[4] == "1" ? FontStyle.Bold : FontStyle.Regular);
                graphicsText.DrawString(PE.STotal, myFontUnder, bush, new PointF(StrToInt(bqTotal.Split(',')[0]), StrToInt(bqTotal.Split(',')[1])), format, angle);

            }

            //  sb.AppendLine(ZPL_Text_(StrToInt(sn.Split(',')[0]), StrToInt(sn.Split(',')[1]), PE.Dylsh));
            if (PE.IsDebug)
            {
                //结束图片绘制
                var imgTPK = LogoPath + @"\PrintImg\PrePrint.jpg";
                if (!Directory.Exists(LogoPath + @"\PrintImg\"))
                    Directory.CreateDirectory(LogoPath + @"\PrintImg\");
                //保存图片
                theBitmap.Save(imgTPK);
            }
                return theBitmap;
            #endregion
        }


        public static Bitmap ZerTSC3_Bitmap_Normal(PrintEntity PE)
        {
            if (!IsExistIni()) return null;
            #region Graphic类绘制图片打印
            //开始绘制图片
            int initialWidth = 700, initialHeight = 400;
            string Setting = ini.IniReadValue(PE.Bq, "Setting");
            if (Setting.Length > 0)
            {
                initialWidth = StrToInt(Setting.Split(',')[0]) * 15;
                initialHeight = StrToInt(Setting.Split(',')[1]) * 15;
                try
                {
                    Orgin = Setting.Split(',')[2];
                }
                catch { }
            }
            //设置长度宽度
            Bitmap theBitmap = new Bitmap(initialWidth, initialHeight);
            Graphics theGraphics = Graphics.FromImage(theBitmap);
            //填充的颜色
            Brush bush = new SolidBrush(System.Drawing.Color.Black);
            //呈现质量
            theGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //背景色
            theGraphics.Clear(System.Drawing.Color.White);
            //R=90度旋转 I=180度旋转 B=270度旋转
            if (Orgin == "R")
            {

                theGraphics.RotateTransform(90.0f);
            }
            else if (Orgin == "I")
            {

                theGraphics.RotateTransform(180.0f);
            }
            else if (Orgin == "B")
            {

                theGraphics.RotateTransform(270.0f);
            }
            //string urlImage = System.AppDomain.CurrentDomain.BaseDirectory;
            System.Drawing.Font myFont;
            System.Drawing.Font myFontT;
            System.Drawing.Font myFontUnder;
            System.Drawing.Font myFontBigUnder;
            myFontT = new System.Drawing.Font(PE.FontName, 9, FontStyle.Regular);
            myFont = new System.Drawing.Font(PE.FontName, 14, FontStyle.Regular);
            myFontUnder = new System.Drawing.Font(PE.FontName, 15, FontStyle.Regular);
            myFontBigUnder = new System.Drawing.Font("Century Schoolbook", 8, FontStyle.Regular);
            StringFormat geshi = new StringFormat();
            geshi.Alignment = StringAlignment.Center; //居中(LOGO)
            System.Drawing.Image img = new Bitmap(LogoPath + @"\PrintLogo.png");
            //lOGOimg
            string logo = ini.IniReadValue(PE.Bq, "Logo");
            if (logo.Length > 0)
                theGraphics.DrawImage(img, StrToInt(logo.Split(',')[0]), StrToInt(logo.Split(',')[1]), StrToInt(logo.Split(',')[2]), StrToInt(logo.Split(',')[3]));
            //theGraphics.DrawString("touching tomorrow,today!", myFontT, bush, 15, 55);
            //Conimg
            // System.Drawing.Image img2 = new Bitmap(urlImage + "\Image\consignmentLabel.jpg");
            //  theGraphics.DrawImage(img2, 196, 0, (float)(287), (float)(75));
            //QRCodeimg
            //Image img3 = new Bitmap(urlImage + "\Image\labelQRCode.jpg");
            //theGraphics.DrawImage(img3, 490, 11, (float)(67), (float)(67));

            string Title = ini.IniReadValue(PE.Bq, "Title");
            if (Title.Length > 0)
                theGraphics.DrawString(PE.Title, new Font(PE.FontName, StrToInt(Title.Split(',')[2])), bush, StrToInt(Title.Split(',')[0]), StrToInt(Title.Split(',')[1]));
            string adr = ini.IniReadValue(PE.Bq, "ADDRESS");
            if (adr.Length > 0)
                theGraphics.DrawString(PE.Address, new Font(PE.FontName, StrToInt(adr.Split(',')[2])), bush, StrToInt(adr.Split(',')[0]), StrToInt(adr.Split(',')[1]));//+10
            string zdy1 = ini.IniReadValue(PE.Bq, "Zdy1");
            if (zdy1.Length > 0)
                theGraphics.DrawString(PE.Zdy1, new Font(PE.FontName, StrToInt(zdy1.Split(',')[2])), bush, StrToInt(zdy1.Split(',')[0]), StrToInt(zdy1.Split(',')[1]));//+5
            string zdy2 = ini.IniReadValue(PE.Bq, "Zdy2");
            if (zdy2.Length > 0)
                theGraphics.DrawString(PE.Zdy2, new Font(PE.FontName, StrToInt(zdy2.Split(',')[2])), bush, StrToInt(zdy2.Split(',')[0]), StrToInt(zdy2.Split(',')[1]));
            string fullName = ini.IniReadValue(PE.Bq, "fullName");
            if (fullName.Length > 0)
                theGraphics.DrawString(PE.Shortname, new Font(PE.FontName, StrToInt(fullName.Split(',')[2])), bush, StrToInt(fullName.Split(',')[0]), StrToInt(fullName.Split(',')[1]));//+5

            string Breed = ini.IniReadValue(PE.Bq, "Breed");
            if (Breed.Length > 0)
                theGraphics.DrawString(PE.Breed, new Font(PE.FontName, StrToInt(Breed.Split(',')[2])), bush, StrToInt(Breed.Split(',')[0]), StrToInt(Breed.Split(',')[1]));
            string LotNumber = ini.IniReadValue(PE.Bq, "LotNumber");
            if (LotNumber.Length > 0)
                theGraphics.DrawString(PE.LotNumber, new Font(PE.FontName, StrToInt(LotNumber.Split(',')[2])), bush, StrToInt(LotNumber.Split(',')[0]), StrToInt(LotNumber.Split(',')[1]));
            //  theGraphics.DrawLine(new Pen(Color.Black), new Point(350, hei + txtUnder * 2), new Point(500, hei + txtUnder * 2));
            string C3 = ini.IniReadValue(PE.Bq, "UseBefore");

            if (C3.Length > 0 && PE.UseBefore != "")
            {
                string[] s = PE.UseBefore.Split(':');
                PE.UseBefore = DateTime.Parse(s[1]).ToString(C3.Split(',')[5]);
                // sb.AppendLine(ZPL_Text(StrToInt(C3.Split(',')[0]), StrToInt(C3.Split(',')[1]), StrToInt(C3.Split(',')[2]), s[0] + ":" + PE.UseBefore));
                theGraphics.DrawString(s[0] + ":" + PE.UseBefore, new Font(PE.FontName, StrToInt(C3.Split(',')[2])), bush, StrToInt(C3.Split(',')[0]), StrToInt(C3.Split(',')[1]));
            }
            string C4 = ini.IniReadValue(PE.Bq, "ProdDate");
            if (C4.Length > 0 && PE.ProdDate != "")
            {
                string[] s = PE.ProdDate.Split(':');
                PE.ProdDate = DateTime.Parse(s[1]).ToString(C4.Split(',')[5]);
                theGraphics.DrawString(s[0] + ":" + PE.ProdDate, new Font(PE.FontName, StrToInt(C4.Split(',')[2])), bush, StrToInt(C4.Split(',')[0]), StrToInt(C4.Split(',')[1]));

                // TSCLIB.windowsfont(int.Parse(C4.Split(',')[0]), int.Parse(C4.Split(',')[1]), int.Parse(C4.Split(',')[2]), 0, int.Parse(C4.Split(',')[4]), 0, C4.Split(',')[3], s[0] + ":" + ProdDate);
            }
            bool IsIDs = true;
            string C5 = ini.IniReadValue(PE.Bq, "ID");
            string ID_ = ini.IniReadValue(PE.Bq, "ID_");
            if (C5.Length > 0)//&& bq == "DemoBoarID"
            {
                IsIDs = false;
                string[] s = PE.ID.Split(';');
                theGraphics.DrawString(s[0], new Font(PE.FontName, StrToInt(ID_.Split(',')[2])), bush, StrToInt(ID_.Split(',')[0]), StrToInt(ID_.Split(',')[1]));
                theGraphics.DrawString(s[1], new Font(PE.FontName, StrToInt(C5.Split(',')[2])), bush, StrToInt(C5.Split(',')[0]), StrToInt(C5.Split(',')[1]));


            }
            string C6 = ini.IniReadValue(PE.Bq, "ml");
            if (C6.Length > 0)
                theGraphics.DrawString(PE.Ml, new Font(PE.FontName, StrToInt(C6.Split(',')[2])), bush, StrToInt(C6.Split(',')[0]), StrToInt(C6.Split(',')[1]));
            string C7 = ini.IniReadValue(PE.Bq, "IDs");
            string IDs_ = ini.IniReadValue(PE.Bq, "IDs_");
            if (C7.Length > 0 && IsIDs)//&& bq == "DemoBoarIDs"
            {
                string[] s = PE.IDs.Split(';');
                int y = 0;
                for (int i = 0; i < s.Length; i++)
                {
                    if (i == 0)
                        theGraphics.DrawString(s[0], new Font(PE.FontName, StrToInt(IDs_.Split(',')[2])), bush, StrToInt(IDs_.Split(',')[0]), StrToInt(IDs_.Split(',')[1]));
                    else
                        theGraphics.DrawString(s[i], new Font(PE.FontName, StrToInt(C7.Split(',')[2])), bush, StrToInt(C7.Split(',')[0]), StrToInt(C7.Split(',')[1]));
                    y = y + int.Parse(C7.Split(',')[5]);
                }
            }

            if (!PE.Issingle)//如果联网，则生效
            {
                string st = ini.IniReadValue(PE.Bq, "short");
                if (st.Length > 0)
                    theGraphics.DrawString(PE.Shortname, new Font(PE.FontName, StrToInt(st.Split(',')[2])), bush, StrToInt(st.Split(',')[0]), StrToInt(st.Split(',')[1]));
            }
            //新增4个字段
            string bqDensity = ini.IniReadValue(PE.Bq, "Density");
            string bqVitality = ini.IniReadValue(PE.Bq, "Vitality");
            string bqVaild = ini.IniReadValue(PE.Bq, "Vaild");
            string bqTotal = ini.IniReadValue(PE.Bq, "Total");
            if (bqDensity.Length > 0)
                theGraphics.DrawString(PE.SDensity, new Font(PE.FontName, StrToInt(bqDensity.Split(',')[2])), bush, StrToInt(bqDensity.Split(',')[0]), StrToInt(bqDensity.Split(',')[1]));

            if (bqVitality.Length > 0)
                theGraphics.DrawString(PE.SVitality, new Font(PE.FontName, StrToInt(bqVitality.Split(',')[2])), bush, StrToInt(bqVitality.Split(',')[0]), StrToInt(bqVitality.Split(',')[1]));

            if (bqVaild.Length > 0)
                theGraphics.DrawString(PE.SVaild, new Font(PE.FontName, StrToInt(bqVaild.Split(',')[2])), bush, StrToInt(bqVaild.Split(',')[0]), StrToInt(bqVaild.Split(',')[1]));

            if (bqTotal.Length > 0)
                theGraphics.DrawString(PE.STotal, new Font(PE.FontName, StrToInt(bqTotal.Split(',')[2])), bush, StrToInt(bqTotal.Split(',')[0]), StrToInt(bqTotal.Split(',')[1]));

            string sn = ini.IniReadValue(PE.Bq, "sn");
            if (sn.Length > 0)
                theGraphics.DrawString(PE.Dylsh, new Font(PE.FontName, StrToInt(sn.Split(',')[2])), bush, StrToInt(sn.Split(',')[0]), StrToInt(sn.Split(',')[1]));

            //  sb.AppendLine(ZPL_Text_(StrToInt(sn.Split(',')[0]), StrToInt(sn.Split(',')[1]), PE.Dylsh));

            //结束图片绘制
            var imgTPK = LogoPath + @"\PrintImg\PrePrint.jpg";
            if (!Directory.Exists(LogoPath + @"\PrintImg\"))
                Directory.CreateDirectory(LogoPath + @"\PrintImg\");
            //保存图片
              theBitmap.Save(imgTPK);
            return theBitmap;
            #endregion
        }
        #region PNG图像转为16进制
        //string zplToSend = ConvertImageHasPosition(imgTPK, 0, 0);//生成打印机指令(识别单色) 
        //string printImage = "^XA^^FO10,80^LH10,80^LL2500^PW2500^XGR:ZLOGO.GRF,2,2^FS^XZ";//打印图片命令 //^FO0,0 设置打印内容坐标
        #endregion

        #region   ZPL帮助类//图片转为GRF格式 实现并输出
        //不带位置
        public string ConvertImageNoPosition(string s_FilePath)
        {

            int b = 0;

            long n = 0;

            long clr;

            StringBuilder sb = new StringBuilder();

            sb.Append("~DGR:ZLOGO.GRF,");

            Bitmap bm = new Bitmap(s_FilePath);

            int w = ((bm.Size.Width / 8 + ((bm.Size.Width % 8 == 0) ? 0 : 1)) * bm.Size.Height);

            int h = (bm.Size.Width / 8 + ((bm.Size.Width % 8 == 0) ? 0 : 1));



            sb.Append(w.ToString().PadLeft(5, '0') + "," + h.ToString().PadLeft(3, '0') + ",\n");

            using (Bitmap bmp = new Bitmap(bm.Size.Width, bm.Size.Height))
            {

                for (int y = 0; y < bm.Size.Height; y++)
                {

                    for (int x = 0; x < bm.Size.Width; x++)
                    {

                        b = b * 2;

                        clr = bm.GetPixel(x, y).ToArgb();

                        string s = clr.ToString("X");



                        if (s.Substring(s.Length - 6, 6).CompareTo("BBBBBB") < 0)
                        {

                            bmp.SetPixel(x, y, bm.GetPixel(x, y));

                            b++;

                        }

                        n++;

                        if (x == (bm.Size.Width - 1))
                        {

                            if (n < 8)
                            {

                                b = b * (2 ^ (8 - (int)n));



                                sb.Append(b.ToString("X").PadLeft(2, '0'));

                                b = 0;

                                n = 0;

                            }

                        }

                        if (n >= 8)
                        {

                            sb.Append(b.ToString("X").PadLeft(2, '0'));

                            b = 0;

                            n = 0;

                        }

                    }

                }

                sb.Append("^FO100,540^XGR:ZLOGO.GRF,2,2^FS");

            }

            return sb.ToString();

        }

        //带位置 (从左上角开始)到条码字段x，y座标
        public static string ConvertImageHasPosition(string s_FilePath, int xtop, int ytop)
        {

            int b = 0;

            long n = 0;

            long clr;

            StringBuilder sb = new StringBuilder();

            sb.Append("~DGR:ZLOGO.GRF,");

            Bitmap bm = new Bitmap(s_FilePath);

            int w = ((bm.Size.Width / 8 + ((bm.Size.Width % 8 == 0) ? 0 : 1)) * bm.Size.Height);

            int h = (bm.Size.Width / 8 + ((bm.Size.Width % 8 == 0) ? 0 : 1));



            sb.Append(w.ToString().PadLeft(5, '0') + "," + h.ToString().PadLeft(3, '0') + ",\n");

            using (Bitmap bmp = new Bitmap(bm.Size.Width, bm.Size.Height))
            {

                for (int y = 0; y < bm.Size.Height; y++)
                {

                    for (int x = 0; x < bm.Size.Width; x++)
                    {

                        b = b * 2;

                        clr = bm.GetPixel(x, y).ToArgb();

                        string s = clr.ToString("X");



                        if (s.Substring(s.Length - 6, 6).CompareTo("BBBBBB") < 0)
                        {

                            bmp.SetPixel(x, y, bm.GetPixel(x, y));

                            b++;

                        }

                        n++;

                        if (x == (bm.Size.Width - 1))
                        {

                            if (n < 8)
                            {

                                b = b * (2 ^ (8 - (int)n));



                                sb.Append(b.ToString("X").PadLeft(2, '0'));

                                b = 0;

                                n = 0;

                            }

                        }

                        if (n >= 8)
                        {

                            sb.Append(b.ToString("X").PadLeft(2, '0'));

                            b = 0;

                            n = 0;

                        }

                    }

                }

                sb.Append(string.Format("^FO{0},{1}^XGR:ZLOGO.GRF,2,2^FS", xtop, ytop));

            }
            return sb.ToString();
        }


        public static string ConvertImageHasPosition(Bitmap bm, int xtop, int ytop)
        {

            int b = 0;

            long n = 0;

            long clr;

            StringBuilder sb = new StringBuilder();

            sb.Append("~DGR:ZLOGO.GRF,");

            //Bitmap bm = new Bitmap(s_FilePath);

            int w = ((bm.Size.Width / 8 + ((bm.Size.Width % 8 == 0) ? 0 : 1)) * bm.Size.Height);

            int h = (bm.Size.Width / 8 + ((bm.Size.Width % 8 == 0) ? 0 : 1));



            sb.Append(w.ToString().PadLeft(5, '0') + "," + h.ToString().PadLeft(3, '0') + ",\n");

            using (Bitmap bmp = new Bitmap(bm.Size.Width, bm.Size.Height))
            {

                for (int y = 0; y < bm.Size.Height; y++)
                {

                    for (int x = 0; x < bm.Size.Width; x++)
                    {

                        b = b * 2;

                        clr = bm.GetPixel(x, y).ToArgb();

                        string s = clr.ToString("X");



                        if (s.Substring(s.Length - 6, 6).CompareTo("BBBBBB") < 0)
                        {

                            bmp.SetPixel(x, y, bm.GetPixel(x, y));

                            b++;

                        }

                        n++;

                        if (x == (bm.Size.Width - 1))
                        {

                            if (n < 8)
                            {

                                b = b * (2 ^ (8 - (int)n));



                                sb.Append(b.ToString("X").PadLeft(2, '0'));

                                b = 0;

                                n = 0;

                            }

                        }

                        if (n >= 8)
                        {

                            sb.Append(b.ToString("X").PadLeft(2, '0'));

                            b = 0;

                            n = 0;

                        }

                    }

                }

                sb.Append(string.Format("^FO{0},{1}^XGR:ZLOGO.GRF,2,2^FS", xtop, ytop));

            }
            return sb.ToString();
        }

        #endregion
        /// <summary>
        /// 正在执行打印
        /// </summary>
        /// <param name="PE"></param>
        /// <returns></returns>
        public static string ZerTSC_Print(PrintEntity PE)//简称，是否单机 z
        {
            string flag = ""; string error = "";
            StringBuilder sb = new StringBuilder();

           // if (m_Mutex.WaitOne())
            {
                try
                {
                    string Setting = ini.IniReadValue(PE.Bq, "Setting");
                    sb.AppendLine(ZPL_start(StrToInt(Setting.Split(',')[0]), StrToInt(Setting.Split(',')[1])));
                    try {
                        Orgin = Setting.Split(',')[2];
                    } catch { Orgin = "N"; }

                    string QRCODE = ini.IniReadValue(PE.Bq, "QRCODE");
                    if(QRCODE.Length>1)
                    sb.AppendLine(ZPL_QRCode(StrToInt(QRCODE.Split(',')[0]), StrToInt(QRCODE.Split(',')[1]), StrToInt(QRCODE.Split(',')[2]), PE.BarCode));

                    string TITLE = ini.IniReadValue(PE.Bq, "Title");
                    if(TITLE.Length>1)
                    PE.FontName = TITLE.Split(',')[3];
                    Bitmap img= ZerTSC3_Bitmap(PE);
                    // string imgpath = LogoPath + @"\PrintImg\PrePrint.jpg";
                     string zplToSend = ConvertImageHasPosition(img, 0, 0);//生成打印机指令(识别单色) 

                    string data = ZebraImgZPL.BitmapToHex(img, out int to, out int row);
                    string zpl = string.Format("~DGR:Temp0.GRF,{0},{1},{2}", to, row, data);
                    string zplcode = "^FO1,1^XGR:Temp0.GRF,1,1^FS";

                    //string imgCode = ConvertImageToCode(img);
                    //var t = ((img.Size.Width / 8 + ((img.Size.Width % 8 == 0) ? 0 : 1)) * img.Size.Height).ToString();
                    //var w = (img.Size.Width / 8 + ((img.Size.Width % 8 == 0) ? 0 : 1)).ToString();
                    //string zpl = string.Format("~DGR:imgName.GRF,{0},{1},{2}", t, w, imgCode);
                    //string zplcode = "^FO1,1^XGR:imgName.GRF,1,1^FS";
                    //sb.AppendLine(zplToSend  );
                    sb.AppendLine(zplToSend);
                   
                    sb.AppendLine(ZPL_End());
                
                    //发送命令
                    string printsting =   sb.ToString();// iniprint.IniReadValue("ZerBar", "Pre") ;
               

                    flag = Common.ZebarPrint.GetZebarPrintSendCode(printsting);
                  
                }
                catch (Exception ex) { }
                finally {
                   // m_Mutex.ReleaseMutex();
                }
            }
            return flag;
        }
        protected static Bitmap CreateImageAuto(string data, Font f)
        {
            if (string.IsNullOrEmpty(data))
                return null;
            var txt = new TextBox();
            txt.Text = data;
            txt.Font = f;
            //txt.PreferredSize.Height只能取到一行的高度(连边距) 
            //所以需要乘以行数, 但是必须先减掉边距, 乘了以后,再把边距加上. 
            //5是目测的边距 
            var image = new Bitmap(txt.PreferredSize.Width, (txt.PreferredSize.Height - 5) * txt.Lines.Length + 5);
            var g = Graphics.FromImage(image);
            var b = new System.Drawing.Drawing2D.LinearGradientBrush(new Rectangle(0, 0, image.Width, image.Height), Color.Black, Color.Black, 1.2f, true);
            g.Clear(System.Drawing.Color.White);
            g.DrawString(data, f, b, 1, 1);
            return image;
        }

        public static String valueOf(Object obj)
        {
            return (obj == null) ? "null" : obj.ToString();
        }

        private static int StrToInt(string s) {
            int i = 0;
            try {

                i = int.Parse(s);
            } catch { }

            return i;

        }
         
        //    ^XA：标签格式^XA开始
        //^JM：设置每毫米点数(取决打印头)
        //^LL：设置标签长度
        //^PW：设置标签宽度
        //^MD：设置标签深度
        //^PR：设置打印速度
        //^PO：设置打印方向
        //^LR：设置标签反转('Y'/'N')
        //^LH：设置标签起点
         private static string ZPL_start(int width,int length)
        {
            ////开始命令符，设置打印纸张大小、打印的浓度，打印的速度，条码出于纸张的坐标
            var sb = new StringBuilder();
            sb.AppendLine("^XA^JMA^LL" + Hight_LL + "^PW" + Width_PW + "^FS");
            //sb.AppendLine("^CW1,E:MSUNG24.FNT");
            //sb.AppendLine("^SEE:GB18030.DAT^CI26"); 
            return sb.ToString();
        }
        private static string ZPL_startCode(int width, int length,string PrintFont)
        {
            ////开始命令符，设置打印纸张大小、打印的浓度，打印的速度，条码出于纸张的坐标
            var sb = new StringBuilder();
            sb.AppendLine("^XA^JMA^LL" + Hight_LL + "^PW" + Width_PW + "^FS");
            if (PrintFont != "")
            {
                sb.AppendLine("^CW1,"+PrintFont+"");
                sb.AppendLine("^SEE:GB18030.DAT^CI26");
            }
            if (Orgin != "N") {
                sb.AppendLine("^PO" + Orgin + "");
            }
            return sb.ToString();
        }
        private static string ZPL_Text_(int x, int y, string data)
        {
            //设置字体格式
            //strLable = "^CWJ,E:MSUNG.FNT^FS";//  //字体（微软雅黑）
            string strLable = "^FO" + x + "," + y + "^A1N,35,35^FD" + data + "^FS";
            return strLable;
        }

        private static string ZPL_Text(int x,int y,int fontsize,string data)
        {
            //设置字体格式
            //strLable = "^CWJ,E:MSUNG.FNT^FS";//  //字体（微软雅黑）
            // string strLable = " ^FO"+x+","+y+ "^AFN,26,13^CI28^FD" + data+"^FS";
            string fontstyle = ZPL_FontChStyle;
            double yy =Math.Round( (y / 32)*1.0)* Font_Y;   
            //FontStyle style = new FontStyle();
           // style = FontStyle.Italic;
           // Font f = new Font(fontstyle, fontsize - Font_Size);  
            string strLable = uploadImgZpl(x,y,data, fontsize);//全部字体大小-20
            return strLable;
        }

        //设置一维码Code128的格式
        //^FO52,90^BY2,3,50^BCN,50,N,N,N,A^FD$SN^FS
        //    ^FO：设置字段位置
        //    ^BY：条码字段默认参数设置
        //    ^BC：Code128，()
        //    ^FD：Field Data数据字段，限制100字符
        //    ^FS：字段分隔，表示字段定义结束
        //    ^A：缩放/点阵字体
        private static string ZPL_BarCode128(int pointX,int pointY,string data)
        {
            String strLable = "^FO"+ pointX + "," + pointY + "^BY2,3,50^BCN,50,N,N,N,A^FD"+data+"^FS";//$data
            return strLable;

        }
         
        private static string ZPL_QRCode(int x,int y,int size,string data)
        {
            String strLable = "^FO"+x+","+y+ "^BQ"+ Orgin + ",2," + (size+BarCode_Size) + "^FDLA," + data+"^FS";
            return strLable;
        }
    //      ^PQ：设置打印数量
    //^XZ：标签格式以^XZ结束
        private static string ZPL_End()
        {
            String strLable = "^PQ1^XZ";
            return strLable;
        } 
        protected  static string ConvertImageToCode(Bitmap img)
        {
            var sb = new StringBuilder();
            long clr = 0, n = 0;
            int b = 0;
            for (int i = 0; i < img.Size.Height; i++)
            {
                for (int j = 0; j < img.Size.Width; j++)
                {
                    b = b * 2;
                    clr = img.GetPixel(j, i).ToArgb();
                    string s = clr.ToString("X");

                    if (s.Substring(s.Length - 6, 6).CompareTo("BBBBBB") < 0)
                    {
                        b++;
                    }
                    n++;
                    if (j == (img.Size.Width - 1))
                    {
                        if (n < 8)
                        {
                            b = b * (2 ^ (8 - (int)n));

                            sb.Append(b.ToString("X").PadLeft(2, '0'));
                            b = 0;
                            n = 0;
                        }
                    }
                    if (n >= 8)
                    {
                        sb.Append(b.ToString("X").PadLeft(2, '0'));
                        b = 0;
                        n = 0;
                    }
                }
                sb.Append(System.Environment.NewLine);
            }
            return sb.ToString();

        }

        private static string ZPL_Logo(int x,int y,int w,int h)
        {
            string ProImgPath = LogoPath + @"\PrintLogo.png";//要保存的图片的地址，包含文件名
          //  var image = new BitmapImage(new Uri(ProImgPath));
            System.Drawing.Image originalImage = System.Drawing.Image.FromFile(ProImgPath); 
            var imgae2 = ZebraImgZPL.GetThumbnail(originalImage, w, h);//所有图片压缩成200*100
            Bitmap img = new Bitmap(imgae2);
           // Bitmap img =ZebraImgZPL.BitmapImage2Bitmap(imgae2);
            //string imgCode = ConvertImageToCode(img);
            //var t = ((img.Size.Width / 8 + ((img.Size.Width % 8 == 0) ? 0 : 1)) * img.Size.Height).ToString();
            //var w = (img.Size.Width / 8 + ((img.Size.Width % 8 == 0) ? 0 : 1)).ToString();
            //string zpl = string.Format("~DGR:Templogo.GRF,{0},{1},{2}",      imgCode);
            //string zplcode = "^FO" + x + "," + y + "^XGR:Templogo" + x + y + ".GRF,1,1^FS";
            string data = ZebraImgZPL.BitmapToHex(img, out int to, out int row);
            string zpl = string.Format("~DGR:Temp0.GRF,{0},{1},{2}", to, row, data);
            string zplcode = "^FO" + x + "," + y + "^XGR:Temp0.GRF,1,1^FS";
            return zpl + zplcode;
        }


        private static string uploadImgZpl(int x,int y,string data,int fonsize)
        {
            //if (Regex.IsMatch(data, @"[\u4e00-\u9fa5]"))
            //{
            //    return "^FO" + x + "," + y + "^A1N," + fonsize + "," + fonsize + "^FD" + data + "^FS"; //A1N格式^Af,o,h,w 其中f,由CW来确定定义范围A-Z，0-9
            //    //  Bitmap img = CreateImageAuto(data,f);// CreateImage(data, 500, 45, f);
            //    //string imgCode = ZebraImgZPL.BitmapToHex(img, out int to, out int row);
            //    //string zpl = string.Format("~DGR:Temp" + x + y + ".GRF,{0},{1},{2}", to, row, imgCode);
            //    //string zplcode = "^FO" + x + "," + y + "^XGR:Temp" + x + y + ".GRF,1,1^FS"; 
            //    //return zpl + zplcode;
            //}
            //else {
            //    return  "^FO" + x + "," + y + "^A0N,"+ fonsize + ","+ fonsize + "^FD" + data + "^FS";
            //}
            //return ZPL_Text_(x, y + 10, data);
                return "^FO" + x + "," + y + "^A1"+ Orgin + "," + fonsize + "," + fonsize + "^FD" + data + "^FS";

        }
        public static Bitmap CreateImage(string data, int Width, int Height, Font f, int x = 1, int y = 1)
        {
            if (string.IsNullOrEmpty(data))
                return null;
            var image = new Bitmap(Width, Height);
            var g = Graphics.FromImage(image);
            var b = new System.Drawing.Drawing2D.LinearGradientBrush(new Rectangle(0, 0, image.Width, image.Height), Color.Black, Color.Black, 1.2f, true);
            g.Clear(System.Drawing.Color.White);
            g.DrawString(data, f, b, x, y);
            return image;
        }

        public static string ZPLPrintertest(string p_title = "", string p_specs = "", string p_date = "", string p_code = "")
        {
            try
            {
                #region ZPL II 打印指令

                string title = p_title, specs = p_specs, date = p_date, code = p_code;

                //title = title + "古典黄芥沫调味酱";
                //specs = specs + "100ML/瓶";
                //date = date + "2015-10-31";
                //code = "8957891234567895789588535";

                StringBuilder zplsb = new StringBuilder();
                zplsb.Append("^XA");
                zplsb.Append("^LH30,30"); 
                zplsb.Append("^JMA");
                zplsb.Append("^MD30");
                zplsb.Append("^FO45,50");
                Bitmap img = CreateImage(title, 600, 50, new Font("Arial", 35), 1, 2);
                var t = ((img.Size.Width / 8 + ((img.Size.Width % 8 == 0) ? 0 : 1)) * img.Size.Height).ToString();
                var w = (img.Size.Width / 8 + ((img.Size.Width % 8 == 0) ? 0 : 1)).ToString();
                string imgCode = ConvertImageToCode(img);
                string zpl = string.Format("~DGR:imgName.GRF,{0},{1},{2}", t, w, imgCode);
                zplsb.Append(zpl);
                zplsb.Append("^XGR:imgName.GRF,1,1^FS");

                zplsb.Append("^FO45,120");
                img = CreateImage(specs, 600, 50, new Font("Arial", 25), 1, 10);
                t = ((img.Size.Width / 8 + ((img.Size.Width % 8 == 0) ? 0 : 1)) * img.Size.Height).ToString();
                w = (img.Size.Width / 8 + ((img.Size.Width % 8 == 0) ? 0 : 1)).ToString();
                imgCode = ConvertImageToCode(img);
                zpl = string.Format("~DGR:imgName.GRF,{0},{1},{2}", t, w, imgCode);
                zplsb.Append(zpl);
                zplsb.Append("^XGR:imgName.GRF,1,1^FS");

                zplsb.Append("^FO45,180");
                img = CreateImage(date, 600, 50, new Font("Arial", 25), 1, 10);
                t = ((img.Size.Width / 8 + ((img.Size.Width % 8 == 0) ? 0 : 1)) * img.Size.Height).ToString();
                w = (img.Size.Width / 8 + ((img.Size.Width % 8 == 0) ? 0 : 1)).ToString();
                imgCode = ConvertImageToCode(img);
                zpl = string.Format("~DGR:imgName.GRF,{0},{1},{2}", t, w, imgCode);
                zplsb.Append(zpl);
                zplsb.Append("^XGR:imgName.GRF,1,1^FS");

                zplsb.Append("^FO45,240");
                zplsb.Append("^BY4,2");//条码宽度
                zplsb.Append("^BCN,100,Y,N,N,A");
                zplsb.Append("^FD" + code + "^FS");
                zplsb.Append("^XZ");

                #endregion
                //发送命令
                Common.ZebarPrint.GetZebarPrintSendCode(zplsb.ToString());
                return zplsb.ToString();/*Encoding.Default.GetBytes(zplsb.ToString());*/
            }
            catch (Exception)
            {
                throw;
            }
        }

       static void Write_Docment(string msg, string docment)
        { 
                Logger.Instance.WriteLog_(msg, LogType.Information, docment);
        }

    }
}
