﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Common.Print
{
    class ComStatusSet
    {
        public static void SetSerial(string Port, int Baud, int StopBits, int Parity, int DataBits)
        {
            ComPrintStatus.COMPort = Port;
            ComPrintStatus.Baud = Baud;//9600
            ComPrintStatus.StopBits = StopBits;//2
            ComPrintStatus.Parity = Parity;//1
            ComPrintStatus.DataBits = DataBits;//7
        }
        /// <summary>
        /// 佳博获取状态
        /// </summary>
        /// <returns></returns>
        public static string GetStaus()
        {
          return  ComPrintStatus.ReadPort("1B 21 3F");

        }
        /// <summary>
        /// 斑马获取状态
        /// </summary>
        /// <returns></returns>
        public static string GetStaus_Zebra()
        {
            return ComPrintStatus.ReadPort_Str("^XA~HS^XZ");

        }
    }
}
