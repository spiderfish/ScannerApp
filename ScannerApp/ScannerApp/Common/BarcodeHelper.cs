﻿using ScannerApp.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;
using ZXing.QrCode.Internal;

namespace ScannerApp.Common
{
    /// <summary>
    /// 描述:条形码和二维码帮助类
    /// 时间:2019-04-18
    /// </summary>
    public class BarcodeHelper
    {
        /// <summary>
        /// 生成二维码
        /// </summary>
        /// <param name="text">内容</param>
        /// <param name="width">宽度</param>
        /// <param name="height">高度</param>
        /// <returns></returns>
        public static Bitmap Generate1(string text, int width, int height)
        {
            BarcodeWriter writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            QrCodeEncodingOptions options = new QrCodeEncodingOptions()
            {
                DisableECI = true,//设置内容编码
                CharacterSet = "UTF-8",  //设置二维码的宽度和高度
                Width = width,
                PureBarcode = true,
                Height = height,
                Margin = 1//设置二维码的边距,单位不是固定像素
            };

            writer.Options = options;
            Bitmap map = writer.Write(text);
            return map;
        }

        /// <summary>
        /// 生成一维条形码
        /// </summary>
        /// <param name="text">内容</param>
        /// <param name="width">宽度</param>
        /// <param name="height">高度</param>
        /// <returns></returns>
        public static Bitmap Generate2(string text, int width, int height)
        {
            BarcodeWriter writer = new BarcodeWriter();
            //使用ITF 格式，不能被现在常用的支付宝、微信扫出来
            //如果想生成可识别的可以使用 CODE_128 格式
            //writer.Format = BarcodeFormat.ITF;
            writer.Format = BarcodeFormat.CODE_128
                ;
            EncodingOptions options = new EncodingOptions()
            {
                Width = width,
                Height = height,
                Margin = 2,
            };
            writer.Options = options;
            Bitmap map = writer.Write(text);
            return map;
        }

        public static Bitmap GenerateCZ(string text, int width, int height)
        {
            BarcodeWriter writer = new BarcodeWriter();
            //使用ITF 格式，不能被现在常用的支付宝、微信扫出来
            //如果想生成可识别的可以使用 CODE_128 格式
            //writer.Format = BarcodeFormat.ITF;
            writer.Format = BarcodeFormat.CODE_128;

            EncodingOptions options = new EncodingOptions()
            {
                Width = width,
                Height = height,
                Margin = 5,
                PureBarcode = true //不显示数字
            };
            writer.Options = options;
            Bitmap map = writer.Write(text);
            return map;
        }

        /// <summary>
        /// 生成带Logo的二维码
        /// </summary>
        /// <param name="text">内容</param>
        /// <param name="width">宽度</param>
        /// <param name="height">高度</param>
        public static Bitmap Generate3(string text, int width, int height)
        {
            //Logo 图片
            string logoPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\img\logo.jpg";
            Bitmap logo = new Bitmap(logoPath);
            //构造二维码写码器
            MultiFormatWriter writer = new MultiFormatWriter();
            Dictionary<EncodeHintType, object> hint = new Dictionary<EncodeHintType, object>();
            hint.Add(EncodeHintType.CHARACTER_SET, "UTF-8");
            hint.Add(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            //hint.Add(EncodeHintType.MARGIN, 2);//旧版本不起作用，需要手动去除白边

            //生成二维码 
            BitMatrix bm = writer.encode(text, BarcodeFormat.QR_CODE, width + 30, height + 30, hint);
            bm = deleteWhite(bm);
            BarcodeWriter barcodeWriter = new BarcodeWriter();
            Bitmap map = barcodeWriter.Write(bm);

            //获取二维码实际尺寸（去掉二维码两边空白后的实际尺寸）
            int[] rectangle = bm.getEnclosingRectangle();

            //计算插入图片的大小和位置
            int middleW = Math.Min((int)(rectangle[2] / 3), logo.Width);
            int middleH = Math.Min((int)(rectangle[3] / 3), logo.Height);
            int middleL = (map.Width - middleW) / 2;
            int middleT = (map.Height - middleH) / 2;

            Bitmap bmpimg = new Bitmap(map.Width, map.Height, PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmpimg))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(map, 0, 0, width, height);
                //白底将二维码插入图片
                g.FillRectangle(Brushes.White, middleL, middleT, middleW, middleH);
                g.DrawImage(logo, middleL, middleT, middleW, middleH);
            }
            return bmpimg;
        }

        /// <summary>
        /// 删除默认对应的空白
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        private static BitMatrix deleteWhite(BitMatrix matrix)
        {
            int[] rec = matrix.getEnclosingRectangle();
            int resWidth = rec[2] + 1;
            int resHeight = rec[3] + 1;

            BitMatrix resMatrix = new BitMatrix(resWidth, resHeight);
            resMatrix.clear();
            for (int i = 0; i < resWidth; i++)
            {
                for (int j = 0; j < resHeight; j++)
                {
                    if (matrix[i + rec[0], j + rec[1]])
                        resMatrix[i, j] = true;
                }
            }
            return resMatrix;
        }

        /// <summary>
        /// 生成图片的方法(打印预览）
        /// </summary>
        /// <param name="image"></param>
        /// <param name="asset">数据</param>
        /// <param name="g"></param>
        public static void GetPrintPicture(Bitmap image, AssetEntity asset, PrintPageEventArgs g)
        {
            int height = 5;
            Font font = new Font("宋体", 10f);
            System.Drawing.Brush brush = System.Drawing.Brushes.Black;
            //System.Drawing.Brush brush = new SolidBrush(Color.Black);
            g.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            int interval = 15;
            int pointX = 5;
            image = CreateQRCode(asset.Serial);
            System.Drawing.Rectangle destRect = new System.Drawing.Rectangle(190, 10, image.Width, image.Height);
            g.Graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
            height += 8;
            RectangleF layoutRectangle = new RectangleF(pointX, height, 260f, 85f);
            g.Graphics.DrawString("资产编号:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.Graphics.DrawString("资产名称:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.Graphics.DrawString("类    别:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.Graphics.DrawString("规格型号:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.Graphics.DrawString("生产厂家:" + asset.Serial, font, brush, layoutRectangle);


            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.Graphics.DrawString("启用时间:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.Graphics.DrawString("资产价格:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.Graphics.DrawString("保管单位:" + asset.Serial, font, brush, layoutRectangle);

            //height += interval;
            layoutRectangle = new RectangleF(pointX + 150, height, 230f, 85f);
            g.Graphics.DrawString("保管人:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.Graphics.DrawString("存放地点:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 240f, 85f);
            g.Graphics.DrawString("备    注:" + asset.Serial, font, brush, layoutRectangle);

        }

        /// <summary>
        /// 程序预览
        /// </summary>
        /// <param name="image"></param>
        /// <param name="asset"></param>
        /// <param name="picWidth"></param>
        /// <param name="picHeight"></param>
        /// <returns></returns>
        public static Image GetPrintPicture(Bitmap image, AssetEntity asset, int picWidth, int picHeight)
        {
            Bitmap printPicture = new Bitmap(picWidth, picHeight);
            int height = 5;
            Font font = new Font("黑体", 10f);
            Graphics g = Graphics.FromImage(printPicture);
            Brush brush = new SolidBrush(Color.Black);

            g.SmoothingMode = SmoothingMode.HighQuality;

            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;//如果不填加反锯齿代码效果如图1

            image= CreateQRCode(asset.Serial);
            int interval = 15;
            int pointX = 5;
            Rectangle destRect = new Rectangle(190, 10, image.Width, image.Height);
            g.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
            height += 8;
            RectangleF layoutRectangle = new RectangleF(pointX, height, 260f, 85f);
            g.DrawString("资产编号:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.DrawString("资产名称:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.DrawString("类    别:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.DrawString("规格型号:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.DrawString("生产厂家:" + asset.Serial, font, brush, layoutRectangle);


            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.DrawString("启用时间:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.DrawString("资产价格:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.DrawString("保管单位:" + asset.Serial, font, brush, layoutRectangle);

            //height += interval;
            layoutRectangle = new RectangleF(pointX + 150, height, 230f, 85f);
            g.DrawString("保管人:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            g.DrawString("存放地点:" + asset.Serial, font, brush, layoutRectangle);

            height += interval;
            layoutRectangle = new RectangleF(pointX, height, 240f, 85f);
            g.DrawString("备    注:" + asset.Serial, font, brush, layoutRectangle);

            return printPicture;
        }
        /// <summary>
        /// 生成一维码(图片)固定长度/宽度
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        public static Bitmap CreateQRCode(string asset)
        {
            EncodingOptions options = new QrCodeEncodingOptions
            {
                DisableECI = true,
                CharacterSet = "UTF-8",
                Width = 120,
                Height = 120
            };
            BarcodeWriter writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.CODE_128;
            writer.Options = options;
            return writer.Write(asset);
        }

        /// <summary>
        /// 打印作业单
        /// </summary>
        /// <param name="image"></param>
        /// <param name="asset">数据</param>
        /// <param name="g"></param>
        public static void GetPrintWorkIDPicture(Bitmap image, PrintWorkDocumentEntity asset, PrintPageEventArgs g)
        {
            int height = 1;
            Font font = new Font("宋体", 8f);
            System.Drawing.Brush brush = System.Drawing.Brushes.Black;
            //System.Drawing.Brush brush = new SolidBrush(Color.Black);
            g.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            int pointX = 5;
            image = Generate2(asset.WorkID,100,30);//生成一维码
            System.Drawing.Rectangle destRect = new System.Drawing.Rectangle(15, 5, image.Width, image.Height);
            g.Graphics.DrawImage(image, destRect, 15, 5, image.Width, image.Height, GraphicsUnit.Pixel);
            height += 30;
            RectangleF layoutRectangle = new RectangleF(pointX, height, 5f, 5f);
            g.Graphics.DrawString("销售编号:" + asset.SaleID, font, brush, layoutRectangle);

            //height += interval;
            //layoutRectangle = new RectangleF(pointX, height, 230f, 85f);
            //g.Graphics.DrawString("资产名称:" + asset.Name, font, brush, layoutRectangle);


        }
        /// <summary>
            /// 绘制打印内容
            /// </summary>
            /// <param name="e">PrintPageEventArgs</param>
            /// <param name="PrintStr">需要打印的文本</param>
            /// <param name="BarcodeStr">条码</param>
        public static void DrawPrint(PrintPageEventArgs e, string PrintStr, string BarcodeStr, int BarcodeWidth, int BarcodeHeight)
        {
            try
            {
                //绘制打印字符串
                e.Graphics.DrawString(PrintStr, new Font(new FontFamily("黑体"), 5), System.Drawing.Brushes.Black, 1, 1);

                if (!string.IsNullOrEmpty(BarcodeStr))
                {
                    int PrintWidth = 150;
                    int PrintHeight = 20;
                    //绘制打印图片
                    e.Graphics.DrawImage(Generate2(BarcodeStr, BarcodeWidth-10, BarcodeHeight-2), 0, 0, PrintWidth, PrintHeight);
                }

            }
            catch (Exception ex)
            {
                
            }
        }
    }
}