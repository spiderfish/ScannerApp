﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Common
{
   public class ISASpSUSSendCommond
    {
        //新建
        public static string newanalysis = "newanalysis";
        //putdata state fresh  fresh|stored 
        public static string putdata_state = "putdata state";
        public static string putdata_collector = "putdata collector";
        public static string putdata_operator = "putdata operator";
        ///Int 一次剂量;
        public static string putdata_dosisvolume = "putdata dosisvolume";
        //Int 一种剂量中产生数百万有用的精子
        public static string putdata_usefulspermperdosis = "putdata usefulspermperdosis";
        // Name/identificator of the animal 
        public static string putdata_subject = "putdata subject";
        //When we are doing control analysis it’s the reference of the fresh analysis
        public static string putdata_freshreference = "putdata freshreference";

        public static string putdata_dilutor = "putdata dilutor";
        //Int Number of mml of ejaculated 射精量
        public static string putdata_ejaculatedvolume = "putdata ejaculatedvolume";
        //Int If we use a quantity of initial dilutor, number of mml 
        public static string putdata_initialdilutorvolume = "putdata putdata_initialdilutorvolume";
        //Int Ratio of dilution used to do the analysis 1+X. X is the number of part of dilutor
        public static string putdata_analysisdilution = "putdata analysisdilution";
        public static string savedata = "savedata";
        public static string captureandanalize = "captureandanalize";
        public static string capture = "capture";
    }
    public class ISASpSUSReciveCommond
    {
        public static string getdata_casereference = "getdata casereference";
        public static string getdata_date = "getdata date";
        public static string getdata_state = "getdata state";
        public static string getdata_collector = "getdata collector";
        public static string getdata_operator = "getdata operator";
        public static string getdata_dosisvolume = "getdata dosisvolume";
        public static string getdata_usefulspermperdosis = "getdata usefulspermperdosis";
        public static string getdata_subject = "getdata subject";
        public static string getdata_freshreference = "getdata freshreference";
        public static string getdata_dilutor= "getdata dilutor";
        public static string getdata_ejaculatedvolume = "getdata ejaculatedvolume";
        public static string getdata_initialdilutorvolume = "getdata initialdilutorvolume";
        public static string getdata_analysisdilution = "getdata analysisdilution";
        public static string getdata_totnumber = "getdata totnumber";
        public static string getdata_totpercen = "getdata totpercen";
        public static string getdata_totmillions = "getdata totmillions";
        public static string getdata_totmml = "getdata totmml";
        public static string getdata_nornumber = "getdata nornumber";
        public static string getdata_norpercen = "getdata norpercen";
        public static string getdata_normillions = "getdata normillions";
        public static string getdata_normml = "getdata normml";
        public static string getdata_motnumber = "getdata motnumber";
        public static string getdata_motpercen = "getdata motpercen";
        public static string getdata_motmillions = "getdata motmillions";
        public static string getdata_motmml = "getdata motmml";
        public static string getdata_mnonumber = "getdata mnonumber";
        public static string getdata_mnopercen = "getdata mnopercen";
        public static string getdata_mnomillions = "getdata mnomillions";
        public static string getdata_mnomml = "getdata mnomml";
        public static string getdata_usenumber = "getdata usenumber";
        public static string getdata_usepercen = "getdata usepercen";
        public static string getdata_usemillions = "getdata usemillions";
        public static string getdata_usemml = "getdata usemml";
        public static string getdata_produceddosis = "getdata produceddosis";
        public static string getdata_diluentvolume = "getdata diluentvolume";
        public static string getdata_dilutionratio = "getdata dilutionratio";
        public static string getdata_totalspermsperdosis = "getdata totalspermsperdosis";
        public static string getdata_totalfinalvolume = "getdata totalfinalvolume";


        public static string getfielddata_totnumber = "getdata totnumber";
        public static string getfielddata_totpercen = "getdata totpercen";
        public static string getfielddata_totmillions = "getdata totmillions";
        public static string getfielddata_totmml = "getdata totmml";
        public static string getfielddata_nornumber = "getdata nornumber";
        public static string getfielddata_norpercen = "getdata norpercen";
        public static string getfielddata_normillions = "getdata normillions";
        public static string getfielddata_normml = "getdata normml";
        public static string getfielddata_motnumber = "getdata motnumber";
        public static string getfielddata_motpercen = "getdata motpercen";
        public static string getfielddata_motmillions = "getdata motmillions";
        public static string getfielddata_motmml = "getdata motmml";
        public static string getfielddata_mnonumber = "getdata mnonumber";
        public static string getfielddata_mnopercen = "getdata mnopercen";
        public static string getfielddata_mnomillions = "getdata mnomillions";
        public static string getfielddata_mnomml = "getdata mnomml";
        public static string getfielddata_usenumber = "getdata usenumber";
        public static string getfielddata_usepercen = "getdata usepercen";
        public static string getfielddata_usemillions = "getdata usemillions";
        public static string getfielddata_usemml = "getdata usemml";

    }

    public class IsASpSusDic
    {
      private  Dictionary<int, string> _SendMSG;
        private Dictionary<int, string> _RecvieMSG;

        public Dictionary<int, string> SendMSG { get => _SendMSG; set => _SendMSG = value; }
        public Dictionary<int, string> RecvieMSG { get => _RecvieMSG; set => _RecvieMSG = value; }
    }
}
