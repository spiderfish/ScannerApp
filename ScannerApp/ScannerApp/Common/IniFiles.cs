﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace ScannerApp.Common
{
    public class IniFiles
    {
        public string inipath;
        Regex regChina = new Regex("^[^\x00-\xFF]");
        Regex regEnglish = new Regex("^[a-zA-Z]");
        //声明API函数

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32", CharSet = CharSet.Unicode, EntryPoint = "GetPrivateProfileString")]
        private static extern uint GetPrivateProfileStringA(

        string lpAppName, // points to section name

                    string lpKeyName, // points to key name

                    string lpDefault, // points to default string

                    byte[] lpReturnedString, // points to destination buffer

                    uint nSize, // size of destination buffer

                    string lpFileName  // points to initialization filename

                );  
        /// <summary> 
        /// 构造方法 
        /// </summary> 
        /// <param name="INIPath">文件路径</param> 
        public IniFiles(string INIPath)
        {
            inipath = INIPath;
        }

        public IniFiles() { }

        /// <summary> 
        /// 写入INI文件 
        /// </summary> 
        /// <param name="Section">项目名称(如 [TypeName] )</param> 
        /// <param name="Key">键</param> 
        /// <param name="Value">值</param> 
        public void IniWriteValue(string Section, string Key, string Value)
        {
            //if (regEnglish.IsMatch(Value) || regChina.IsMatch(Value))
            //{

            //}
            //else
            //{
            //    Value = cryptHelper.Encrypt(Value);
            //}
                WritePrivateProfileString(Section, Key, Value, this.inipath);
        }
        /// <summary> 
        /// 读出INI文件 
        /// </summary> 
        /// <param name="Section">项目名称(如 [TypeName] )</param> 
        /// <param name="Key">键</param> 
        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(500);
            string a = "";
            int i = GetPrivateProfileString(Section, Key, "", temp, 500, this.inipath);

            //if (regEnglish.IsMatch(temp.ToString()) || regChina.IsMatch(temp.ToString()))
            //{
            //    a = temp.ToString();
            //}
            //else
            //{
            //    a = cryptHelper.Decrypt(temp.ToString());
            //}
            a = temp.ToString();
            return a;
        }

        //获取节点
        public List<string> IniReadSection()
        {
            List<string> result = new List<string>();
            byte[] buf = new byte[65536*2];
            uint len = GetPrivateProfileStringA(null, null, null, buf, (uint)buf.Length, this.inipath);

            int j = 0;

            for (int i = 0; i < len*2; i=i+2)

              if (buf[i] == 0) 
                {
                    //string a = "";
                    //foreach (EncodingInfo encodingInfo in Encoding.GetEncodings())
                    //{
                    //     a+=(encodingInfo.Name + "///" + encodingInfo.GetEncoding().EncodingName);
                    //}
                    Encoding utf16 = Encoding.Unicode;
                    result.Add(utf16.GetString(buf, j, (i - j)));

                    j = i + 2;

                }
            return result;
        }

        //删除节
        public   long DeleteSection(string Section)
        {
          return  WritePrivateProfileString(Section, null, null, this.inipath); 
        }
        /// <summary>
        /// 删除键值
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="iniFilePath"></param>
        /// <returns></returns>
        public   long DeleteKey(string Section, string key)
        {
            return WritePrivateProfileString(Section, key, null, this.inipath); 
        } 
        /// <summary> 
        /// 验证文件是否存在 
        /// </summary> 
        /// <returns>布尔值</returns> 
        public bool ExistINIFile()
        {
            return File.Exists(inipath);
        }

     
    }
}