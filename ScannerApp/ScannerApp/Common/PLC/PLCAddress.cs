﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Common
{
   public class PLCAddress
    {
        public static string MStart_ModBus_Address = "002049";
        public static string MStart_0X_Address = "0800";
        //4095
        public static string MEnd_ModBus_Address = "047616 ";
        public static string MEnd_0X_Address = "B9FF";

        public static string DStart_ModBus_Address = "404097";
        public static string DStart_0X_Address = "1000";
        //9999
        public static string DEnd_ModBus_Address = "442768 ";
        public static string DEnd_0X_Address = "A70F";
        //SE主机再增加10000-11999  //442769~444768 A710~AEDF 

    }
}
