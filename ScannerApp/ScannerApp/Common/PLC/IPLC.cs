﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScannerApp.Common.PLC
{  
    interface IPLC
    {
        /// <summary>
        /// 连接PLC
        /// </summary>
        /// <returns></returns>
        bool ConnPLC();
        /// <summary>
        /// 断开PLC
        /// </summary>
        /// <returns></returns>
        bool DisConnPLC();
        /// <summary>
        /// 检测PLC是否连接正常
        /// </summary>
        /// <returns></returns>
        bool CheckPLCStatus();
        /// <summary>
        /// 读取指定连续个数的Bit值
        /// </summary>
        /// <param name="start">开始地址</param>
        /// <param name="num">读取个数</param>
        /// <returns>读取所有值的连接字符串</returns>
    
    /// <summary>
    /// 连续写入多个Bit值
    /// </summary>
    /// <param name="start">开始地址</param>
    /// <param name="writeData">要写入的字符串</param>
    /// <returns>写入是否成功</returns>
    int SetDReg(int Reg, int RegValue);
    /// <summary>
    /// 读取指定个数的Int值
    /// </summary>
    /// <param name="start">开始地址</param>
    /// <param name="num">读取个数</param>
    /// <returns>返回读取个数长度的整数数组</returns>
    int SetDDReg(int Reg, int RegValue);
        /// <summary>
        /// 写入Int值
        /// </summary>
        /// <param name="start">开始地址</param>
        /// <param name="writeInt">要写入的整数数组</param>
        /// <returns>写入是否成功</returns>
    int ReadDReg(int Reg, out double RegValue);
    /// <summary>
    /// 读取指定个数的Float值
    /// </summary>
    /// <param name="start">开始地址</param>
    /// <param name="num">读取个数</param>
    /// <returns>返回读取个数长度的单精度浮点数数组</returns>
    int ReadDDReg(int Reg, out double RegValue);
    /// <summary>
    /// 写入Float值
    /// </summary>
    /// <param name="start">开始地址</param>
    /// <param name="writeInt">要写入的单精度浮点数数组</param>
    /// <returns>写入是否成功</returns>
    int SetMReg(int Reg, bool RegState);
    /// <summary>
    /// 读取一个Int值
    /// </summary>
    /// <param name="start">开始地址</param>
    /// <returns>返回一个整数</returns>
    int ReadMReg(int Reg, out bool RegState);
       
    }
}
