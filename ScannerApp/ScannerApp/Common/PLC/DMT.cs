﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Common.PLC
{
    public class DMT
    { // import the LoadLibrary function from kernel32.dll 

        // 当想要将DMT.dll置于特定路径下时需要引入此函式  

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr LoadLibrary(string dllPath);



        // import the FreeLibrary function from kernel32.dll 

        // 当想要将DMT.dll置于特定路径下时需要引入此函式 

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern bool FreeLibrary(IntPtr hDll);



        // Data Access

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int RequestData(int comm_type, int conn_num, int slave_addr, int func_code, byte[] sendbuf, int sendlen);

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int ResponseData(int comm_type, int conn_num, ref int slave_addr, ref int func_code, byte[] recvbuf);



        // Serial Communication

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int OpenModbusSerial(int conn_num, int baud_rate, int data_len, char parity, int stop_bits, int modbus_mode);

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern void CloseSerial(int conn_num);

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int GetLastSerialErr();

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern void ResetSerialErr();



        // Socket Communication

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int OpenModbusTCPSocket(int conn_num, int ipaddr);

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern void CloseSocket(int conn_num);

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int GetLastSocketErr();

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern void ResetSocketErr();

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int ReadSelect(int conn_num, int millisecs);



        // Wrapped MODBUS Functions are in Unicode version

        // MODBUS Address Calculation

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int DevToAddrW(string series, string device, int qty);



        // Wrapped MODBUS Funcion : 0x01

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int ReadCoilsW(int comm_type, int conn_num, int slave_addr, int dev_addr, int qty, UInt32[] data_r, StringBuilder req, StringBuilder res);



        // Wrapped MODBUS Funcion : 0x02 

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int ReadInputsW(int comm_type, int conn_num, int slave_addr, int dev_addr, int qty, UInt32[] data_r, StringBuilder req, StringBuilder res);



        // Wrapped MODBUSFuncion : 0x03 

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int ReadHoldRegsW(int comm_type, int conn_num, int slave_addr, int dev_addr, int qty, UInt32[] data_r, StringBuilder req, StringBuilder res);

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int ReadHoldRegs32W(int comm_type, int conn_num, int slave_addr, int dev_addr, int qty, UInt32[] data_r, StringBuilder req, StringBuilder res);



        // Wrapped MODBUS Funcion : 0x04 

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int ReadInputRegsW(int comm_type, int conn_num, int slave_addr, int dev_addr, int qty, UInt32[] data_r, StringBuilder req, StringBuilder res);



        // Wrapped MODBUS Funcion : 0x05            

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int WriteSingleCoilW(int comm_type, int conn_num, int slave_addr, int dev_addr, UInt32 data_w, StringBuilder req, StringBuilder res);



        // Wrapped MODBUS Funcion : 0x06 

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int WriteSingleRegW(int comm_type, int conn_num, int slave_addr, int dev_addr, UInt32 data_w, StringBuilder req, StringBuilder res);

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int WriteSingleReg32W(int comm_type, int conn_num, int slave_addr, int dev_addr, UInt32 data_w, StringBuilder req, StringBuilder res);



        // Wrapped MODBUSFuncion : 0x0F

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int WriteMultiCoilsW(int comm_type, int conn_num, int slave_addr, int dev_addr, int qty, UInt32[] data_w, StringBuilder req, StringBuilder res);



        // Wrapped MODBUS Funcion : 0x10

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int WriteMultiRegsW(int comm_type, int conn_num, int slave_addr, int dev_addr, int qty, UInt32[] data_w, StringBuilder req, StringBuilder res);

        [DllImport("DMT.dll", CharSet = CharSet.Auto)]

        public static extern int WriteMultiRegs32W(int comm_type, int conn_num, int slave_addr, int dev_addr, int qty, UInt32[] data_w, StringBuilder req, StringBuilder res);




    }
}
