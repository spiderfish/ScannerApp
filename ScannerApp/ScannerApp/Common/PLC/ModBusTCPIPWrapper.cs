﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Common.PLC
{
    public class ModBusTCPIPWrapper  
    {
        private  static string ipaddress;
        private static int portNum;

        #region TCPIP
        public static bool IsConnected = false;
       static Socket   socketClient = null;

     

        public static string Ipaddress { get => ipaddress; set => ipaddress = value; }
        public static int PortNum { get => portNum; set => portNum = value; }

        public static void Connect(string ipaddress, int portNum)
        {
            IPAddress address = IPAddress.Parse(ipaddress);
            IPEndPoint endPoint = new IPEndPoint(address, portNum);
            socketClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
               
                socketClient.Connect(endPoint);

            }
            catch (SocketException se)
            {
              
                IsConnected = false;
                return;
            }
      
            IsConnected = true;
          
        }

        //处理tcp接收的数据。
       public static string Receive()
        {
            //if (!IsConnected)
            //{
            //    Connect(Ipaddress, PortNum);
            //    Receive();
                
            //}
            EndPoint remoteEndPoint = new IPEndPoint(0, 0); ;
            byte[] buffMsgRec = new byte[1024];//* 1024 * 10

            int length = -1; 

                try
                {


                    length = socketClient.Receive(buffMsgRec);
                    string receiveText = System.Text.Encoding.Default.GetString(buffMsgRec, 0, length);// 将接受到的字节数据转化成字符串；  

                    return receiveText;
                }

                catch (SocketException se)
                {
                    
                    return "";

                }
                catch (Exception e)
                {
                    
                    return "";

                }
 
            

        }
        //断开连接
        public static void UnConnect()
        {
            try
            { 
                socketClient.Close();
            }
            catch (Exception e)
            { 
                return;
            }    
            IsConnected = false;
        }



        //发送数据
        public static void Send(string Msg)
        {
            SendMsg(socketClient, Msg);
          
        }
        //发送数据
        private static void SendMsg(object sockConnectionparn, string SendMsg)
        {
            Socket sockConnection = sockConnectionparn as Socket;

            if (!sockConnection.IsBound)
            {
               
                return;
            }
            try
            {
                string sendData = SendMsg;    //复制发送数据

                //字符串发送

                //serial.Write(sendData);
                byte[] arrData = System.Text.Encoding.Default.GetBytes(sendData);
                int i = sockConnection.Send(arrData);

                if (i > 0)
                    return;
                else
                {
                  
                    return;
                };

            }
            catch (Exception ex)
            {
              
                return;
            }

        }
        #endregion
 
       
    }
}
