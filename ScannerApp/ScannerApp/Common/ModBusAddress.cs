﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerApp.Common
{
  public  class ModBusAddress
    {
        public static int Mod_M000= 002049;//modbus地址
        public static string A_M000 = "0800";//装置通讯地址
        public static int A_M000_int = Convert.ToInt32(A_M000, 16);

        public static int Mod_D000 = 404097;
        public static string A_D000 = "1000";
        public static int A_D000_int = Convert.ToInt32(A_D000, 16);


        //public static string Mod_M255 = "003584 ";
        public static string A_M255 = "08FF";

        //public static string Mod_D255 = "404097 ";
        public static string A_D255 = "10FF";


        //暂停：m70（按下置1，再次按下置0），使能位m135（为0时使能，为1时显示灰阶效果）。

        public static int Mod_M70 = Mod_M000 + 70;
        public static string A_M70 = (A_M000_int + 70).ToString("X4");

        //启动：m101（按下置1，松开置0），使能位m139（为0时使能，为1时显示灰阶效果）。

        public static int Mod_M101 = Mod_M000 + 101;
        public static string A_M101 = (A_M000_int + 101).ToString("X4");

        //停止：m71（按下置1，松开置0），使能位m135（为0时使能，为1时显示灰阶效果）。

        public static int Mod_M71 = Mod_M000 + 71; 
        public static string A_M71 = (A_M000_int + 71).ToString("X4");


        //复位：m96（按下置1，松开置0），使能位m239（为0时隐藏，为1时正常显示）。
        public static int Mod_M96 = Mod_M000 + 96;
        public static string A_M96 = (A_M000_int + 96).ToString("X4");

        //流量校准：点击后弹出子画面，点击确认m91置1， 松开m91置0
        public static int Mod_M91 = Mod_M000 + 91;
        public static string A_M91 = (A_M000_int + 91).ToString("X4");
        // 参数重置：点击后m92置1，松开m91置0
        public static int Mod_M92 = Mod_M000 + 92;
        public static string A_M92 = (A_M000_int + 92).ToString("X4");

        //调试运行：M102  指示灯：X1

        public static int Mod_M102 = Mod_M000 + 102;
        public static string A_M102 = (A_M000_int + 102).ToString("X4");
        //移袋：M103 指示灯：X3
        public static int Mod_M103 = Mod_M000 + 103;
        public static string A_M103 = (A_M000_int + 103).ToString("X4");
        //压袋：M109 指示灯：X21
        public static int Mod_M109 = Mod_M000 + 109;
        public static string A_M109 = (A_M000_int + 109).ToString("X4");
        //切袋：M105 指示灯：X7
        public static int Mod_M105 = Mod_M000 + 105;
        public static string A_M105 = (A_M000_int + 105).ToString("X4");
        //分离标签：M108
        public static int Mod_M108 = Mod_M000 + 108;
        public static string A_M108 = (A_M000_int + 108).ToString("X4");
        //吸取标签：M111 指示灯：X27
        public static int Mod_M111 = Mod_M000 + 111;
        public static string A_M111 = (A_M000_int + 111).ToString("X4");
        //吸标选装：M106 指示灯;X23
        public static int Mod_M106 = Mod_M000 + 106;
        public static string A_M106 = (A_M000_int + 106).ToString("X4");
        //贴标：M107 指示灯：X25
        public static int Mod_M107 = Mod_M000 + 107;
        public static string A_M107 = (A_M000_int + 107).ToString("X4");
        //灌针插入：M104 指示灯：X5
        public static int Mod_M104 = Mod_M000 + 104;
        public static string A_M104 = (A_M000_int + 104).ToString("X4");
        //泵入：M0
        public static int Mod_M0 = Mod_M000  ;
        public static string A_M0 = (A_M000_int ).ToString("X4");


        //打印机启动/停止：M512(按下后置1，再次按下后置0)
        public static int Mod_M512 = Mod_M000 + 512;
        public static string A_M512 = (A_M000_int + 512).ToString("X4");


        //容量：D20为浮点数（只读），D408为浮点数（读/写）
        public static int Mod_D20 = Mod_D000 + 20;
        public static string A_D20 = (A_D000_int + 20).ToString("X4");

        //热合：D420为16为整数（只读，以浮点数形式显示，小数点向左偏移1位，D422为16位整数（读/写，以浮点形式显示，小数点向左偏移1位）
        public static int Mod_D420 =  Mod_D000 + 420;
        public static string A_D420 = (A_D000_int + 420).ToString("X4");

        //数量：D220位16位整数（只读），D218为16位整数（读/写）
        public static int Mod_D220 = Mod_D000 + 220;
        public static string A_D220 = (A_D000_int + 220).ToString("X4");

        //节拍：D500为16位整数（只读，以浮点数形式显示，小数点向左偏移1位，D501位16位整数（只读，以浮点数形式显示，小数点向左偏移1位）
        public static int Mod_D500 = Mod_D000 + 500;
        public static string A_D500 = (A_D000_int + 500).ToString("X4");

        //灌装预设量：D408为浮点数，读/写
        public static int Mod_D408 = Mod_D000 + 408;
        public static string A_D408 = (A_D000_int + 408).ToString("X4");
        //灌装速度： D410为浮点数，读/写
        public static int Mod_D410 = Mod_D000 + 410;
        public static string A_D410 = (A_D000_int + 410).ToString("X4");
        //灌装流量：D436位浮点数，只读
        public static int Mod_D436 = Mod_D000 + 436;
        public static string A_D436 = (A_D000_int + 436).ToString("X4");
        ////热合温度; D420为16为整数（只读，以浮点数形式显示，小数点向左偏移1位，D422为16位整数（读/写，以浮点形式显示，小数点向左偏移1位）
        //public static int Mod_D420 = Mod_D000 + 420;
        //public static string A_D420 = (A_D000_int + 420).ToString("X4");
        //当前实际灌装量：D436位浮点数，读/写
        //public static int Mod_D436 = Mod_D000 + 436;
        //public static string A_D436 = (A_D000_int + 436).ToString("X4");
    }
}
